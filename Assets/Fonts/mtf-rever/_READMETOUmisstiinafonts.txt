Thank you for downloading this Miss Tiina Font!!

TERMS OF USE:
-------------

This font is free for personal use, please visit this link to read the terms and view/purchase Commercial License options...

http://www.misstiina.com/fonts

Please do not share my fonts or claim as your own, send your friends to the download page :)

-------------

Copyright � 2006-2009 Tina Raparanta a.k.a. Miss Tiina
All Rights Reserved.

Owner/Designer/Webmistress of MissTiina.com
Website: www.misstiina.com
Blog: www.misstiina.com/blog
Email: tiina@misstiina.com

Thank you for your support! Enjoy!

September 8, 2009