﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienceControl : MonoBehaviour
{
	public bool started, died, musicPlaying, fading;
	public float cameraOffset = 3, waterHeight = -0.1f, landTransition = 0.1f, fadeInTime = 3, ambienceVolume = 0.5f, fadedAmount, underwaterAmount, landAmount;
	public AudioSource underwater, land, sea;
	MusicControl music;
	Level level;
	Animal player;
	// Use this for initialization
	void Awake ()
	{
		music = GameObject.Find("Data").GetComponent<MusicControl>();
		level = GameObject.Find("Controller").GetComponent<Level>();
		player = GetComponent<Animal>();
	}

	// Update is called once per frame
	void Update ()
	{
		underwaterAmount = Mathf.Clamp( 1 - ((transform.position.y + cameraOffset) / (waterHeight + cameraOffset)), 0, 1);
		landAmount = Mathf.Clamp ((CellConversor.GetCell(transform.position, level.grid.cells).altitude - (level.biome.sealevel- landTransition/2))  /
			((level.biome.sealevel + landTransition/2) - (level.biome.sealevel- landTransition/2)), 0, 1);
		if(!started && level.finishedLoading)
		{
			StartCoroutine(FadeIn());
	        if(level.biome.type[0] == "sea")
	        {
	            land.clip = music.seaAmbience;
	        }
	        else
	        {
	            if (level.biome.type[0] == "tundra" || level.biome.type[0] == "desert")
	            {
	                land.clip = music.aridAmbience;
	            }
	            else if(level.biome.type[0] == "grassland")
	            {
	                land.clip = music.grassAmbience;
	            }
	            else if (level.biome.type[0] == "forest")
	            {
	                land.clip = music.forestAmbience;
	            }
	            else
	            {
	                land.clip = music.aridAmbience;
	            }
	        }
			underwater.clip = music.underwaterAmbience;
			sea.clip = music.seaAmbience;
			underwater.Play();
			sea.Play();
			land.Play();
			started = true;
		}
		underwater.volume = ambienceVolume * underwaterAmount * (1 - fadedAmount);
		sea.volume = ambienceVolume * (1 -underwaterAmount) * (1 - landAmount) * (1 - fadedAmount);
		land.volume = ambienceVolume * landAmount * (1 - fadedAmount);
		if(!died)
		{
			if(player.dead)
			{
				StartCoroutine(FadeOut());
				died = true;
			}
			else
			{
				if(!fading && !musicPlaying && music.mainSource.isPlaying && music.mainSource.volume > 0)
				{
					StartCoroutine(FadeToAmount(0.5f));
					musicPlaying = true;
				}
				else if(!fading && musicPlaying && (!music.mainSource.isPlaying|| music.mainSource.volume <= 0))
				{
					StartCoroutine(FadeToAmount(1f));
					musicPlaying = false;
				}

			}
		}

	}
	IEnumerator FadeIn()
	{
		float time = 0;
		fading = true;
		while (time < fadeInTime && !died)
		{
			time += Time.deltaTime;
			fadedAmount = 1 - time/fadeInTime;
			yield return null;
		}
		fading = false;
	}
	public void StartFadeOut()
	{
		StartCoroutine(FadeOut());
	}

	IEnumerator FadeOut()
	{
		float time = 0;
		fading = true;
		while (time < fadeInTime)
		{
			time += Time.deltaTime;
			fadedAmount = time/fadeInTime;
			yield return null;
		}
		fading = false;
	}

	IEnumerator FadeToAmount(float amount)
	{
		float time = 0;
		float desiredFade = 1 - amount;
		float initialFade = fadedAmount;
		if(fadedAmount < desiredFade)
		{
			float range = desiredFade - fadedAmount;
			while (time < fadeInTime &&!died)
			{
				time += Time.deltaTime;
				fadedAmount = initialFade + range*time/fadeInTime;
				yield return null;
			}
		}
		else if (fadedAmount > desiredFade)
		{
			float range = fadedAmount - desiredFade;
			while (time < fadeInTime &&!died)
			{
				time += Time.deltaTime;
				fadedAmount = initialFade - range*time/fadeInTime;
				yield return null;
			}
		}
	}
}
