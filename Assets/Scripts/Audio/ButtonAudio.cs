﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonAudio : EventTrigger
{
	AudioSource source;
	AudioClip press, hover;

	void Awake()
	{
		press = Resources.Load<AudioClip>("Audio/FX/pressed");
		hover = Resources.Load<AudioClip>("Audio/FX/hover");
		source = GetComponent<AudioSource>();
	}

	public void PlayHover()
	{
		source.volume = 0.5f;
		source.clip = hover;
		source.Play();
	}
	public void PlaySelected()
	{
		source.volume = 0.7f;
		source.clip = press;
		source.Play();
	}

    public override void OnSubmit(BaseEventData data)
    {
        PlaySelected();
    }

    public override void OnPointerEnter(PointerEventData data)
    {
        PlayHover();
    }

    public override void OnMove(AxisEventData data)
    {
        PlayHover();
    }

    public override void OnPointerClick(PointerEventData data)
    {
        PlaySelected();
    }



}
