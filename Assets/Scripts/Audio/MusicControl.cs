﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControl : MonoBehaviour {

	public bool inConflict;
	public float maxVolume, timeWithoutConflict;
	public AudioClip main, evolutionI, evolutionII, evolutionTransition, conflict, mating, extinction, sea, arid, grass, forest,
		seaAmbience, underwaterAmbience, aridAmbience, grassAmbience, forestAmbience, death;
	public AudioSource mainSource, secondarySource, conflictSource, fxSource;

	public void Update()
	{
		timeWithoutConflict += Time.deltaTime;
		if(timeWithoutConflict > 3 && inConflict)
		{
			StartCoroutine(FadeOut(conflictSource, 3));
			inConflict = false;
		}
	}

	public IEnumerator FadeIn (AudioSource source, AudioClip clip, float totalTime, bool fromZero)
	{
		bool ready = false;
		float time = 0;
		while (time < totalTime)
		{
			if (!ready)
			{
				mainSource.clip = clip;
				mainSource.Play();
				if(fromZero)
				{
					mainSource.volume = 0;
				}
				ready = true;
			}
			else if (ready)
			{
				time += Time.deltaTime;
				mainSource.volume = maxVolume * time/totalTime;
			}
			yield return null;
		}
	}

	public IEnumerator FadeIn (AudioSource source, AudioClip clip, float totalTime)
	{
		StartCoroutine(FadeIn(source, clip, totalTime, false));
		yield return null;
	}


	public IEnumerator FadeIn (AudioClip clip, float totalTime)
	{
		StartCoroutine(FadeIn(mainSource, clip, totalTime));
		yield return null;
	}

	public IEnumerator FadeOut (AudioSource source, float totalTime)
	{
		bool ready = false;
		float time = 0;
		while (time < totalTime && source.volume > 0)
		{
			if (!ready)
			{
				ready = true;
			}
			else if (ready)
			{
				time += Time.deltaTime;
				source.volume = maxVolume*(1 - time/totalTime);
				if(secondarySource.volume > 0)
				{
					secondarySource.volume = maxVolume * ( 1 - time/totalTime);
				}
			}
			yield return null;
		}
	}

	public IEnumerator FadeOut (float totalTime)
	{
		StartCoroutine(FadeOut(mainSource, totalTime));
		yield return null;
	}

	public IEnumerator CrossFade (AudioSource sourceIn, AudioSource sourceOut, AudioClip clip, float totalTime, float newMaxVolume)
	{
		bool ready = false;
		float time = 0;
		float volumeRange = Mathf.Abs(newMaxVolume - maxVolume);
		float initialVolume = maxVolume;
		while (time < totalTime)
		{
			if (!ready )
			{
				sourceIn.clip = clip;
				sourceIn.Play();
				ready = true;
			}
			else if (ready)
			{
				time += Time.deltaTime;
				sourceIn.volume =  maxVolume * time/totalTime;
				if(sourceOut.volume > 0)
				{
					maxVolume = initialVolume - volumeRange* (time/totalTime);
					sourceOut.volume =  maxVolume * (1 - time/totalTime);
				}
			}
			yield return null;
		}

	}

	public IEnumerator CrossFade (AudioClip clip, float totalTime, float newMaxVolume)
	{
		StartCoroutine(CrossFade(secondarySource, mainSource, clip, totalTime, newMaxVolume));
		yield return null;
	}

	public void Play (AudioSource source, AudioClip clip, float volume)
	{
		source.clip = clip;
		source.volume =  volume;
		source.Play();
	}

	public void Play (AudioSource source, AudioClip clip)
	{
		Play(source, clip, maxVolume);
	}

	public void Play (AudioClip clip)
	{
		Play(mainSource, clip);
	}
}
