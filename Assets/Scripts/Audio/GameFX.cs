﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFX : MonoBehaviour
{
	bool matingSounded;
	float timeSinceHeartbeat, timeSinceStomach;
	public float maxHeartbeatTime, maxStomachTime;
	public AudioSource health, hunger, mating, movement, eating;
	public AudioClip steps, swim, chewing, filterFeeding, swallowing;
	Animal player;

	void Awake()
	{
		player = GetComponent<Animal>();
	}

	public void Play (AudioSource source, AudioClip clip, float volume)
	{
		source.volume = volume;
		source.clip = clip;
		source.Play();
	}

	public void Play (AudioSource source, AudioClip clip)
	{
		Play(source, clip, 1);
	}

	void Update()
	{
		timeSinceHeartbeat += Time.deltaTime;
		timeSinceStomach += Time.deltaTime;
		if(!player.dead)
		{
			if(player.life < player.species.maxLife/2)
			{
				float heartbeatTime = (2f*player.life/ player.species.maxLife)*maxHeartbeatTime + 1f;
				if(timeSinceHeartbeat > heartbeatTime)
				{
					health.Play();
					timeSinceHeartbeat = 0;
				}
			}
			if(player.hunger < player.species.maxHunger/2)
			{
				float stomachTime = (2f*player.hunger/ player.species.maxHunger)*maxStomachTime + 3f;
				if(timeSinceStomach > stomachTime)
				{
					hunger.Play();
					timeSinceStomach = 0;
				}
			}
			if(!matingSounded && player.level.matingSeason)
			{
				mating.Play();
				matingSounded = true;
			}

		}
	}
}
