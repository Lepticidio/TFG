﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevGridControl : MonoBehaviour
{
	public bool allAquatic, gradient;
	public Grid grid;
	public Transform floor;
	// Use this for initialization
	void Start ()
	{
		if(allAquatic)
		{
			foreach (Cell cell in grid.cells)
			{
				cell.altitude = 0;
				cell.aquatic = true;
				floor.position = new Vector3(0,-10,0);
			}
		}
		else if (gradient)
		{
			for(int i = 0; i< grid.cells.GetLength(0); i++)
			{
				for(int j = 0; j < grid.cells.GetLength(1); j++)
				{
					grid.cells[i, j].altitude = j/grid.cells.GetLength(1);
					if(grid.cells[i,j].altitude < 0.5f)
					{
						grid.cells[i,j].aquatic = true;
					}
				}
			}
			floor.Rotate(new Vector3(45,0,0));
		}
	}
}
