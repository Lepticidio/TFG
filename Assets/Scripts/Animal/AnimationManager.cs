﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour {

	public bool armsCoordinated, legsCoordinated, coordinationChecked;
	public Animator head, body, arm1, arm2, leg1, leg2, tail;

//
	public void Walk()
	{
		SwimFalse();
		if(!coordinationChecked)
		{
			CheckCoordination();
		}
		if(arm1!=null)
		{
			arm1.SetBool("walk",true);
			if(armsCoordinated)
			{
				arm2.SetBool("walk",true);
			}
			else
			{
				arm2.SetBool("walkUncoordinated",true);
			}
		}
		if(leg1!=null)
		{
			leg1.SetBool("walk",true);
			if(legsCoordinated)
			{
				leg2.SetBool("walk",true);
			}
			else
			{
				leg2.SetBool("walkUncoordinated",true);
			}
		}
		if(tail!= null)
		{
			tail.SetBool("walk",true);
		}

	}


	public void Swim(){
		WalkFalse();
		if(!coordinationChecked)
		{
			CheckCoordination();
		}
		if(arm1!=null)
		{
			arm1.SetBool("swim",true);
			if(armsCoordinated)
			{
				arm2.SetBool("swim",true);
			}
			else
			{
				arm2.SetBool("swimUncoordinated",true);
			}
		}
		if(leg1!=null)
		{
			leg1.SetBool("swim",true);
			if(legsCoordinated){
				leg2.SetBool("swim",true);
			}
			else
			{
				leg2.SetBool("swimUncoordinated",true);
			}
		}
		if(tail!= null)
		{
			tail.SetBool("swim",true);
		}

	}
	public void Idle()
	{
		WalkFalse();
		SwimFalse();
	}

	public void Eat()
	{

		head.SetBool("eat",true);

	}
	public void FinishEat()
	{

		head.SetBool("eat",false);

	}
	public void HeadAttack()
	{
		head.SetTrigger("attack");

	}
	public void BodyAttack()
	{

		body.SetTrigger("attack");

	}
	public void ArmAttack()
	{

		arm1.SetTrigger("attack");
		arm2.SetTrigger("attack");

	}
	public void LegAttack()
	{

		leg1.SetTrigger("attack");
		leg2.SetTrigger("attack");

	}
	public void TailAttack()
	{
		Debug.Log("TailAttack");
		tail.SetTrigger("attack");

	}

	public void CheckCoordination()
	{
		if(arm1!=null)
		{
			armsCoordinated = arm1.transform.parent.GetComponent<PartObject>().coordinated;
		}
		if(leg1!=null)
		{
			legsCoordinated = leg1.transform.parent.GetComponent<PartObject>().coordinated;

		}
		coordinationChecked = true;
	}


	void WalkFalse()
	{
		if(arm1!=null)
		{
			arm1.SetBool("walk",false);
			arm2.SetBool("walk",false);
			arm1.SetBool("walkUncoordinated",false);
			arm2.SetBool("walkUncoordinated",false);
		}
		if(leg1!=null)
		{
			leg1.SetBool("walk",false);
			leg2.SetBool("walk",false);
			leg1.SetBool("walkUncoordinated",false);
			leg2.SetBool("walkUncoordinated",false);
		}
		if(tail!= null)
		{
			tail.SetBool("walk",false);
		}

	}
	void SwimFalse()
	{
		if(arm1!=null)
		{
			arm1.SetBool("swim",false);
			arm2.SetBool("swim",false);
			arm1.SetBool("swimUncoordinated",false);
			arm2.SetBool("swimUncoordinated",false);
		}
		if(leg1!=null)
		{
			leg1.SetBool("swim",false);
			leg2.SetBool("swim",false);
			leg1.SetBool("swimUncoordinated",false);
			leg2.SetBool("swimUncoordinated",false);
		}
		if(tail!= null)
		{
			tail.SetBool("swim",false);
		}
	}

	public void StartBreathing()
	{
		body.speed =1;
	}

	public void StopBreathing()
	{
		body.speed =0;
	}

	public void SetMovementSpeed(float speed)
	{
		if(tail!=null)
		{
			tail.speed = speed;
		}
		if(arm1!=null)
		{
			arm1.speed = speed;
		}
		if(arm2!=null)
		{
			arm2.speed = speed;
		}
		if(leg1!=null)
		{
			leg1.speed = speed;
		}
		if(leg2!=null)
		{
			leg2.speed = speed;
		}
	}
}
