﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mate : MonoBehaviour
{

	public float distanceIcon;
	public SpriteRenderer icon;
	public GameObject mateIcon;
	public Part part;
	public GameObject player;
	public Actions actionsPlayer;
	public string bestImprovement;
	public Color color;

	void Start()
	{
		mateIcon = Instantiate(new GameObject(), transform);
		icon = mateIcon.AddComponent<SpriteRenderer>();
		mateIcon.transform.position = mateIcon.transform.position + new Vector3 (0, distanceIcon, 0);
		player = GameObject.Find("Player");
		icon.sprite = Resources.Load<Sprite>("Sprites/Interface/Stats/" + bestImprovement + "Icon");
		icon.color = Color.clear;
		icon.sortingOrder = 1;
	}

	void Update ()
	{
		if(player != null)
		{
			if(actionsPlayer == null)
			{
				actionsPlayer = player.GetComponentInChildren<Actions>();
			}
			else
			{
				float distance = actionsPlayer.senses.mateDistance;
				float minDistance = actionsPlayer.minMateDistance * 3;
				if(minDistance != 0 && distance < minDistance)
				{
					icon.color = new Color (1, 1, 1, 1 - distance/minDistance);
				}
				else
				{
					icon.color = Color.clear;					
				}
			}
		}

	}


}
