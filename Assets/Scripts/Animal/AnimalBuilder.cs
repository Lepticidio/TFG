using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;
using System.Linq;

public class AnimalBuilder : MonoBehaviour {

    public bool trackerTest, animationTest, randomColor, changeColor, ignoreSpecies, hidden, updated, pureWhite, preview;
    public int headInt, bodyInt, tailInt, legsInt;
    public float height, size = 2, mouthMargin, colorTransition, maxAlpha, minAlpha;
    public Transform mouth, centerTail, centerArms, back, front, up, down;
    public Species species;
    public PartObject  bodyPO, headPO, tailPO, arm1, arm2, leg1, leg2;
    public Color color, secondaryColor, eyeColor;
    public List <SpriteMeshInstance> meshes = new List <SpriteMeshInstance>();
    public AnimationManager animationManager;
    public GameObject render;
    public BoxCollider collider;
    public PartData data;
    public Material whiteMaterial, fadeMaterial, cutoutMaterial;





    void FixedUpdate()
    {
        if (changeColor && (color!= species.color||secondaryColor!= species.secondaryColor||eyeColor!=species.eyeColor))
        {
            UpdateColor();
        }
    }

	void OnBecameVisible()
	{
        if(!hidden)
        {
		   render.SetActive(true);
        }
	}
	void OnBecameInvisible()
	{
        hidden = false;
		render.SetActive(false);
	}

    void CreateGameObjects()
    {
        Head head = species.head;
        Body body = species.body;
        Tail tail = species.tail;
        Leg legs = species.legs;

        if (randomColor)
        {
            RandomColor();
        }
        else
        {
          //  color = new Color (0.8f,0.8f, 0.8f, 1f);
          //  secondaryColor = color;
          //  eyeColor = color;
        }
        if(body.gameObject!=null)
        {
            bodyPO = Instantiate(body.gameObject, transform).GetComponent<PartObject>();
            render = bodyPO.transform.Find("Render").gameObject;
            animationManager.body = render.GetComponentInChildren<Animator>();
            if (animationTest)
            {
                animationManager.body.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("AnimatorControllers/Test/body"+body.animal);
            }
            else
            {
                animationManager.body.runtimeAnimatorController = Resources.Load<AnimatorOverrideController>("AnimatorControllers/Override/body"+body.animal);
            }
        }
        if(head.gameObject!=null)
        {
            headPO = Instantiate(head.gameObject, bodyPO.transform).GetComponent<PartObject>();

            animationManager.head = headPO.GetComponentInChildren<Animator>();
            if (animationTest)
            {
                animationManager.head.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("AnimatorControllers/Test/head"+head.animal);
            }
            else
            {
                animationManager.head.runtimeAnimatorController = Resources.Load<AnimatorOverrideController>("AnimatorControllers/Override/head"+head.animal);
            }
            mouth = headPO.transform.Find("mouth");
        }

        if(tail.gameObject!=null)
        {
            tailPO = Instantiate(tail.gameObject, bodyPO.transform).GetComponent<PartObject>();
            animationManager.tail = tailPO.GetComponentInChildren<Animator>();

            if (animationTest)
            {
                animationManager.tail.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("AnimatorControllers/Test/tail"+tail.animal);
            }
            else
            {
                animationManager.tail.runtimeAnimatorController = Resources.Load<AnimatorOverrideController>("AnimatorControllers/Override/tail"+tail.animal);
            }
            centerTail = tailPO.transform.Find("center");
        }

        if(legs.secondaryGameObject!=null)
        {
            arm1 = Instantiate(legs.secondaryGameObject, bodyPO.transform).GetComponent<PartObject>();
            arm2 = Instantiate(legs.secondaryGameObject, bodyPO.transform).GetComponent<PartObject>();

            animationManager.arm1 = arm1.GetComponentInChildren<Animator>();
            animationManager.arm2 = arm2.GetComponentInChildren<Animator>();
            if (animationTest)
            {
                animationManager.arm1.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("AnimatorControllers/Test/arm"+legs.animal);
                animationManager.arm2.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("AnimatorControllers/Test/arm"+legs.animal);
            }
            else
            {
                animationManager.arm1.runtimeAnimatorController = Resources.Load<AnimatorOverrideController>("AnimatorControllers/Override/arm"+legs.animal);
                animationManager.arm2.runtimeAnimatorController = Resources.Load<AnimatorOverrideController>("AnimatorControllers/Override/arm"+legs.animal);
            }
            centerArms = arm1.transform.Find("center");
        }

        if(legs.gameObject!=null)
        {
            leg1 = Instantiate(legs.gameObject, bodyPO.transform).GetComponent<PartObject>();
            leg2 = Instantiate(legs.gameObject, bodyPO.transform).GetComponent<PartObject>();

            animationManager.leg1 = leg1.GetComponentInChildren<Animator>();
            animationManager.leg2 = leg2.GetComponentInChildren<Animator>();
            if (animationTest)
            {
                animationManager.leg1.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("AnimatorControllers/Test/leg"+legs.animal);
                animationManager.leg2.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("AnimatorControllers/Test/leg"+legs.animal);
            }
            else
            {
                animationManager.leg1.runtimeAnimatorController = Resources.Load<AnimatorOverrideController>("AnimatorControllers/Override/leg"+legs.animal);
                animationManager.leg2.runtimeAnimatorController = Resources.Load<AnimatorOverrideController>("AnimatorControllers/Override/leg"+legs.animal);
            }
        }
        meshes = transform.GetComponentsInChildren<SpriteMeshInstance>().ToList();
    }

    public void UpdateAnimal()
    {
        if(species == null)
        {
            Debug.Log("species null");
            species = new Species(0, 0, 0, 0, Color.white, Color.grey, Color.yellow);
        }
        if ( headPO!= null)
        {
            DestroyParts();
        }
        if(ignoreSpecies)
        {
            OverrideSpecies();
        }
        CreateGameObjects();
        UpdatePartPositions();
        UpdateColor();
        CalculateMeasures(headPO.transform.Find("front"),tailPO.transform.Find("back"),
            bodyPO.transform.Find("up"),FindDown());
        updated = true;
    }
    void OverrideSpecies()
    {
        species.headInt = headInt;
        species.bodyInt = bodyInt;
        species.tailInt = tailInt;
        species.legsInt = legsInt;
        species.UpdateStats();
    }

    public void DestroyAnimalEdit()
    {
        DestroyImmediate (bodyPO.gameObject);
    }

    public void DestroyParts()
    {
        DestroyImmediate (bodyPO.gameObject);
    }

    void UpdatePartPositions()
    {
        if(headPO!=null){
            headPO.transform.SetParent(bodyPO.headTracker);
            headPO.transform.localPosition = new Vector3(0,0,0);
            headPO.transform.localScale = new Vector3(1,1,1);
            headPO.bone.transform.SetParent(bodyPO.front.transform);
            if(!trackerTest){
                DestroyImmediate(headPO.gameObject.GetComponent<SpriteRenderer>());

            }
        }
        if(tailPO!=null)
        {
            tailPO.transform.SetParent(bodyPO.tailTracker);
            tailPO.transform.localPosition = new Vector3(0,0,0);
            tailPO.transform.localScale = new Vector3(1,1,1);
            tailPO.bone.transform.SetParent(bodyPO.rear.transform);
            if(!trackerTest){
                DestroyImmediate(tailPO.gameObject.GetComponent<SpriteRenderer>());
            }
        }
        if(arm1!=null)
        {
            arm1.transform.SetParent(bodyPO.arm1Tracker);
            arm1.transform.localPosition = new Vector3(0,0,0);
            arm1.transform.localScale = new Vector3(1,1,1);
            if(!trackerTest)
            {
                DestroyImmediate(arm1.gameObject.GetComponent<SpriteRenderer>());
            }
            arm2.transform.SetParent(bodyPO.arm2Tracker);
            arm2.transform.localPosition = new Vector3(0,0,0);
            arm2.transform.localScale = new Vector3(1,1,1);
            if(!trackerTest)
            {
                DestroyImmediate(arm2.gameObject.GetComponent<SpriteRenderer>());
            }
        }
        if(leg1!=null)
        {
            leg1.transform.SetParent(bodyPO.leg1Tracker);
            leg1.transform.localPosition = new Vector3(0,0,0);
            leg1.transform.localScale = new Vector3(1,1,1);
            if(!trackerTest)
            {
                DestroyImmediate(leg1.gameObject.GetComponent<SpriteRenderer>());
            }
            leg2.transform.SetParent(bodyPO.leg2Tracker);
            leg2.transform.localPosition = new Vector3(0,0,0);
            leg2.transform.localScale = new Vector3(1,1,1);
            if(!trackerTest){
                DestroyImmediate(leg2.gameObject.GetComponent<SpriteRenderer>());
            }
        }
        if(!trackerTest)
        {
            DestroyImmediate(bodyPO.headTracker.gameObject.GetComponent<SpriteRenderer>());
            DestroyImmediate(bodyPO.tailTracker.gameObject.GetComponent<SpriteRenderer>());
            DestroyImmediate(bodyPO.arm1Tracker.gameObject.GetComponent<SpriteRenderer>());
            DestroyImmediate(bodyPO.arm2Tracker.gameObject.GetComponent<SpriteRenderer>());
            DestroyImmediate(bodyPO.leg1Tracker.gameObject.GetComponent<SpriteRenderer>());
            DestroyImmediate(bodyPO.leg2Tracker.gameObject.GetComponent<SpriteRenderer>());
        }
    }

    public void UpdateColor()
    {
        if(pureWhite)
        {
            for (int i= 0; i< meshes.Count; i++)
            {
                meshes[i].sharedMaterial  = whiteMaterial;
            }
        }
        else if(preview)
        {
            Color previewColor = new Color (species.color.r, species.color.g, species.color.b, 0);
            Color previewSecondaryColor = new Color (species.secondaryColor.r, species.secondaryColor.g, species.secondaryColor.b, 0);
            Color previewEyeColor = new Color (species.eyeColor.r, species.eyeColor.g, species.eyeColor.b, 0);
            for (int i= 0; i< meshes.Count; i++)
            {
                meshes[i].sharedMaterial  = fadeMaterial;
                if(meshes[i].gameObject.tag=="Color"){
                    meshes[i].color = previewColor;

                }
                if(meshes[i].gameObject.tag=="SecondaryColor")
                {
                    meshes[i].color = previewSecondaryColor;

                }
                if(meshes[i].gameObject.tag=="EyeColor")
                {
                    meshes[i].color = previewEyeColor;
                }
            }
        }
        else
        {
            for (int i= 0; i< meshes.Count; i++)
            {
                if(meshes[i].gameObject.tag=="Color"){
                    meshes[i].color = species.color;

                }
                if(meshes[i].gameObject.tag=="SecondaryColor")
                {
                    meshes[i].color = species.secondaryColor;

                }
                if(meshes[i].gameObject.tag=="EyeColor")
                {
                    meshes[i].color = species.eyeColor;
                }
            }

        }
    }
    void CalculateMeasures(Transform frontTrans, Transform backTrans, Transform upTrans, Transform downTrans)
    {
        if(frontTrans == null|| backTrans == null || upTrans == null || downTrans == null){
            Debug.Log("a Trans null");
        }
        if(frontTrans == null)
        {
            Debug.Log("Front null "+ bodyPO.gameObject.name);
        }
        if(backTrans == null)
        {
            Debug.Log("back null "+ bodyPO.gameObject.name);
        }
        if(upTrans == null)
        {
            Debug.Log("up null "+ bodyPO.gameObject.name);
        }
        if(downTrans == null)
        {
            Debug.Log("down null "+ bodyPO.gameObject.name);
        }
        else
        {
            transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
            front = frontTrans;
            back = backTrans;
            up = upTrans;
            down = downTrans;
            float distanceToFront = Mathf.Abs(front.position.x - transform.position.x);
            float distanceToUp = Mathf.Abs(up.position.y - transform.position.y);
            float length = Mathf.Abs(front.position.x - back.position.x);
            mouthMargin = Mathf.Abs(mouth.position.x - transform.position.x);
            height = Mathf.Abs(up.position.y - down.position.y);
            collider.size = new Vector3 (length*10, height*10, 3);
            render.transform.localPosition = new Vector3 ((length/2-distanceToFront)*10, (height/2-distanceToUp)*10, 0);
            transform.localScale *= size/length;
        }
    }
    public void GetReferences()
    {
        data = GameObject.Find("Data").GetComponent<PartData>();
        animationManager = GetComponent<AnimationManager>();

    }

    public void RandomAnimal()
    {
        ignoreSpecies = true;
        if(bodyPO != null)
        {
            DestroyAnimalEdit();
        }
        if (data == null)
        {
            GetReferences();
        }
		headInt= Random.Range(0, data.heads.Count);
		bodyInt= Random.Range(0, data.bodies.Count);
		tailInt= Random.Range(0, data.tails.Count);
		legsInt= Random.Range(0, data.legs.Count);
		UpdateAnimal();
        RandomColor();

    }
    public void RandomColor()
    {
        species.color = Misc.PseudoRandomColor();
        species.secondaryColor = Misc.PseudoRandomColor();
        species.eyeColor = Misc.PseudoRandomColor();


        UpdateColor();

    }
    public void ChangeColor(Color newColor)
    {
        species.color = newColor;
        species.secondaryColor = newColor;
        species.eyeColor = newColor;
        UpdateColor();

    }

    public void StartFadeIn()
    {
        StartCoroutine("FadeIn");
    }

    public void StartFadeOut()
    {
        StartCoroutine("FadeOut");
    }

    IEnumerator FadeIn()
    {
        float colorTime = 0;

        while(colorTime < colorTransition)
        {
            for (int i= 0; i< meshes.Count; i++)
            {
                Color meshColor = meshes[i].color;
                meshes[i].color = new Color (meshColor.r, meshColor.g, meshColor.b, minAlpha + maxAlpha*colorTime/colorTransition);
            }
            colorTime += Time.deltaTime;
            yield return null;
        }
        if(preview)
        {
            StartFadeOut();
        }
    }
    IEnumerator FadeOut()
    {
        float colorTime = 0;

        while(colorTime < colorTransition)
        {
            for (int i= 0; i< meshes.Count; i++)
            {
                Color meshColor = meshes[i].color;
                meshes[i].color = new Color (meshColor.r, meshColor.g, meshColor.b, minAlpha + maxAlpha*1 - colorTime/colorTransition);
            }
            colorTime += Time.deltaTime;
            yield return null;
        }
        if(preview)
        {
            StartFadeIn();
        }
    }
    public void ChangeMaterial(Material material)
    {
        for (int i= 0; i< meshes.Count; i++)
        {
            meshes[i].sharedMaterial = material;
        }

    }
    public void ChangeMaterial()
    {
        ChangeMaterial(cutoutMaterial);
    }
    public Transform FindDown()
    {
        Transform bodyDown = bodyPO.transform.Find("down");
        Transform armDown = null;
        Transform legDown = null;
        if(arm1 != null)
        {
            armDown = arm1.transform.Find("down");
        }
        if(leg1 != null)
        {
            legDown = leg1.transform.Find("down");
        }
        if(armDown != null)
        {
            if(bodyDown.position.y < armDown.position.y)
            {
                if(legDown == null || armDown.position.y < legDown.position.y)
                {
                    return bodyDown;
                }
                else
                {
                    return legDown;
                }
            }
            else
            {
                if(legDown == null || armDown.position.y < legDown.position.y)
                {
                    return armDown;
                }
                else
                {
                    return legDown;
                }
            }
        }
        else
        {
            return bodyDown;
        }
    }
}
