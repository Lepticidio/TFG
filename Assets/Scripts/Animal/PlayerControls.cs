﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    public int currentSkill;
    Actions actions;

    void Awake()
    {
        actions = gameObject.GetComponent<Actions>();
    }

    void FixedUpdate ()
    {

        if(Input.GetKeyDown("l"))
        {
    		Misc.LoadScene ("Level");
        }

        if(Input.GetKeyDown("p"))
        {
    		GetComponent<Animal>().RandomAnimal();
            GameObject.Find("Controller").GetComponent<HUD>().UpdateHud();
        }

        if(Input.GetKeyDown("k"))
        {
    		actions.ReproduceCheat();
        }

        if(Input.GetKeyDown("o"))
        {
    		GetComponent<AnimalBuilder>().RandomColor();
        }
        if(!actions.animal.dead)
        {
            if(Input.GetAxis("Horizontal") > 0)
            {
                actions.Right();
            }
            else if(Input.GetAxis("Horizontal") < 0)
            {
                actions.Left();
            }
            if(Input.GetAxis("Vertical") > 0)
            {
                actions.Straight();
            }

            else if(Input.GetAxis("Vertical") < 0)
            {
                actions.Back();
            }
            if(Input.GetAxis("Up Down") > 0)
            {
                actions.Up();
            }
            else if(Input.GetAxis("Up Down") < 0)
            {
                actions.Down();
            }
            if(Input.GetAxis("Skill") > 0)
            {
                if( actions.attacks.Count > 0)
                {
                    if(actions.attacks[currentSkill] == 0)
                    {
                        actions.HeadAttack();
                    }
                    else if(actions.attacks[currentSkill] == 1)
                    {
                        actions.BodyAttack();
                    }
                    else if(actions.attacks[currentSkill] == 2)
                    {
                        actions.TailAttack();
                    }
                    else if(actions.attacks[currentSkill] > 2)
                    {
                        actions.LegAttack();
                    }
                }
            }

        }
        if(Input.GetAxis("Interact") > 0)
        {

            actions.ReproduceOrEat();

        }
        else
        {
            actions.FinishEat();
        }

        if(Input.GetAxis("Change Skill") > 0)
        {
            currentSkill ++;
            if(currentSkill > actions.attacks.Count -1)
            {
                currentSkill = 0;
            }
        }
        else if(Input.GetAxis("Change Skill") < 0)
        {
            currentSkill --;
            if(currentSkill < 0)
            {
                currentSkill = actions.attacks.Count - 1;
            }
        }
        if(Input.GetAxis("Vertical") == 0)
        {
            actions.StopZMovement();
        }
        if(Input.GetAxis("Horizontal") == 0)
        {
            actions.StopXMovement();
        }
        if(Input.GetAxis("Up Down") == 0)
        {
            actions.StopYMovement();
        }
        if( Input.GetAxis("Vertical") == 0 && Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Up Down") == 0)
        {
            actions.Idle();
        }
    }
}
