﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//
public class Species
{
	public string[] name = new string[2];
    public int headInt, bodyInt, tailInt, legsInt, index;
	public Color color, secondaryColor, eyeColor;

	public bool planktivore, carnivore, herbivore, playerNamed;
	public int herbivoreEfficacy, maxAttackInt, dietLevel;
    public float hungerRate, hungerFactor = 0.0004f, maxHunger, lifeFactor = 6, maxLife, speedFactor = 0.002f, terrestrialSpeed, aquaticSpeed,
     	defenseFactor = 1, defense, maxAttack, power;

    public Head head;
    public Body body;
    public Leg legs;
    public Tail tail;
    public PartData data;

	public List<int> previousEvolutions = new List<int>();
	public List<int> newEvolutions = new List<int>();
	public List<Attack> attacks = new List<Attack>();

	public Species antecessor;

	public Species (int hi, int bi , int ti , int li)
	{
		headInt = hi;
		bodyInt = bi;
		tailInt = ti;
		legsInt = li;
		UpdateStats();
	}

	public Species (int hi, int bi , int ti , int li, Color col, Color secCol, Color eyeCol)
	{
		headInt = hi;
		bodyInt = bi;
		tailInt = ti;
		legsInt = li;
		color = col;
		secondaryColor = secCol;
		eyeColor = eyeCol;
		UpdateStats();
	}
	public Species (Species sp)
	{
		headInt = sp.headInt;
		bodyInt = sp.bodyInt;
		tailInt = sp.tailInt;
		legsInt = sp.legsInt;
		color = sp.color;
		secondaryColor = sp.secondaryColor;
		eyeColor = sp.eyeColor;
		antecessor = sp;

		UpdateStats();
	}

	public Species Evolve ()
	{
		return Evolve (2);
	}

	public Species Evolve (int n)
	{
		Species descendance = new Species(this);

		for (int i = 0; i < n; i++)
		{
			descendance.RandomMutation();
		}

		for(int i = 0; i < descendance.newEvolutions.Count; i++)
		{
			descendance.previousEvolutions.Add(descendance.newEvolutions[i]);
		}

		descendance.newEvolutions.Clear();
		descendance.PseudoRandomColors();
		descendance.UpdateStats();

		return descendance;
	}
	public void ChangePart(Part part)
	{
		if(part.type == "head")
		{
			head = part as Head;
			headInt = data.heads.IndexOf(head);
		}
		else if (part.type == "body")
		{
			body = part as Body;
			bodyInt = data.bodies.IndexOf(body);
		}
		else if (part.type == "leg")
		{
			legs = part as Leg;
			legsInt = data.legs.IndexOf(legs);
		}
		else if (part.type == "tail")
		{
			tail = part as Tail;
			tailInt = data.tails.IndexOf(tail);
		}
		UpdateStats();
	}

	public void MainColor (Color col)
	{
		color = col;
	}

	public void SecondaryColor (Color col)
	{
		secondaryColor = col;
	}

	public void EyeColor (Color col)
	{
		eyeColor = col;
	}

	void PseudoRandomColors()
	{
		color = Misc.PseudoRandomColor();
		secondaryColor = Misc.PseudoRandomColor();
		eyeColor = Misc.PseudoRandomColor();
	}

	public Part SuggestMutation()
	{
		List<int> possibleEvolutions = new List<int>();
		for (int i = 0; i < 4; i ++)
		{
			if(!newEvolutions.Contains(i) && (antecessor==null || !antecessor.previousEvolutions.Contains(i)|| HasSoftEvolution(i)))
			{
				possibleEvolutions.Add(i);
			}
		}
		if(possibleEvolutions.Count > 0)
		{
			int rand = possibleEvolutions[Random.Range (0, possibleEvolutions.Count)];

			if(rand == 0)
			{
				if(antecessor!= null && antecessor.previousEvolutions.Contains(rand))
				{
					return head.softEvolutions[Random.Range(0, head.softEvolutions.Count)];
				}
				else
				{
					return head.evolutions[Random.Range(0, head.evolutions.Count)];
				}
			}
			else if (rand == 1)
			{
				if(antecessor!= null && antecessor.previousEvolutions.Contains(rand))
				{
					return body.softEvolutions[Random.Range(0, body.softEvolutions.Count)];
				}
				else
				{//AQUÍ SEGURO
					return body.evolutions[Random.Range(0, body.evolutions.Count)];
				}
			}
			else if (rand == 2)
			{
				if(antecessor!= null && antecessor.previousEvolutions.Contains(rand))
				{
					return legs.softEvolutions[Random.Range(0, legs.softEvolutions.Count)];
				}
				else
				{
					return legs.evolutions[Random.Range(0, legs.evolutions.Count)];
				}
			}
			else
			{
				if(antecessor!= null && antecessor.previousEvolutions.Contains(rand))
				{
					return tail.softEvolutions[Random.Range(0, tail.softEvolutions.Count)];
				}
				else
				{
					return tail.evolutions[Random.Range(0, tail.evolutions.Count)];
				}
			}
		}
		else
		{
			Debug.Log("Return null part");
			return null;
		}

	}

	public void RandomMutation()
	{
		List<int> possibleEvolutions = new List<int>();
		for (int i = 0; i < 4; i ++)
		{
			if(!newEvolutions.Contains(i) && (!antecessor.previousEvolutions.Contains(i)|| HasSoftEvolution(i)))
			{
				possibleEvolutions.Add(i);
			}
		}

		int rand = possibleEvolutions[Random.Range (0, possibleEvolutions.Count)];
		if(rand == 0)
		{
			if(antecessor!= null && antecessor.previousEvolutions.Contains(rand))
			{
				head = head.softEvolutions[Random.Range(0, head.softEvolutions.Count)] as Head;
				headInt = data.heads.IndexOf(head);
			}
			else
			{
				if(head.evolutions.Count > 0)
				{
					head = head.evolutions[Random.Range(0, head.evolutions.Count)] as Head;
				}
				else
				{
					head = head.softEvolutions[Random.Range(0, head.softEvolutions.Count)] as Head;
				}
				headInt = data.heads.IndexOf(head);
			}
		}
		else if (rand == 1)
		{
			if(antecessor!= null && antecessor.previousEvolutions.Contains(rand))
			{
				body = body.softEvolutions[Random.Range(0, body.softEvolutions.Count)] as Body;
				bodyInt = data.bodies.IndexOf(body);
			}
			else
			{
				if(body.evolutions.Count > 0)
				{
					body = body.evolutions[Random.Range(0, body.evolutions.Count)] as Body;
				}
				else
				{
					body = body.softEvolutions[Random.Range(0, body.softEvolutions.Count)] as Body;
				}
				bodyInt = data.bodies.IndexOf(body);
			}
		}
		else if (rand == 2)
		{
			if(antecessor!= null && antecessor.previousEvolutions.Contains(rand))
			{
				legs = legs.softEvolutions[Random.Range(0, legs.softEvolutions.Count)] as Leg;
				legsInt = data.legs.IndexOf(legs);
			}
			else
			{
				if(legs.evolutions.Count > 0)
				{
					legs = legs.evolutions[Random.Range(0, legs.evolutions.Count)] as Leg;
				}
				else
				{
					legs = legs.softEvolutions[Random.Range(0, legs.softEvolutions.Count)] as Leg;
				}
				legsInt = data.legs.IndexOf(legs);
			}
		}
		else if (rand == 3)
		{
			if(antecessor!= null && antecessor.previousEvolutions.Contains(rand))
			{
				tail = tail.softEvolutions[Random.Range(0, tail.softEvolutions.Count)] as Tail;
				tailInt = data.tails.IndexOf(tail);
			}
			else
			{
				if(tail.evolutions.Count > 0)
				{
					tail = tail.evolutions[Random.Range(0, tail.evolutions.Count)] as Tail;
				}
				else
				{
					tail = tail.softEvolutions[Random.Range(0, tail.softEvolutions.Count)] as Tail;
				}
				tailInt = data.tails.IndexOf(tail);
			}
		}
		newEvolutions.Add(rand);
	}

	bool HasSoftEvolution(int i)
	{
		if(i == 0)
		{
			if(head.softEvolutions.Count > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (i == 1)
		{
			if(body.softEvolutions.Count > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (i == 2)
		{
			if(legs.softEvolutions.Count > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(tail.softEvolutions.Count > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	}

	public void UpdateStats()
    {
		secondaryColor = ColorConversion.HSLtoRGB( ColorConversion.HSLFromRGB(color)[0], ColorConversion.HSLFromRGB(color)[1], ColorConversion.HSLFromRGB(color)[2]);
		attacks.Clear();
		dietLevel = 0;
		if(data == null)
		{
			data = GameObject.Find("Data").GetComponent<PartData>();
		}

        head = data.heads[headInt];
        body = data.bodies[bodyInt];
        tail = data.tails[tailInt];
        legs = data.legs[legsInt];

        planktivore =  head.planktivore;
        carnivore =  head.carnivore;
        herbivoreEfficacy =  head.herbivore;
        if(head.herbivore > 0)
        {
            herbivore = true;
        }
		if(carnivore)
		{
			dietLevel +=3;
		}
		else if (planktivore)
		{
			dietLevel +=3;
		}

		dietLevel += herbivoreEfficacy;

        defense = defenseFactor*( head.defense +  body.defense +  tail.defense +  legs.defense);

        terrestrialSpeed = speedFactor *( head.terrestrialSpeed +  body.terrestrialSpeed +  tail.terrestrialSpeed +  legs.terrestrialSpeed)
            * head.terrestrialSpeedFactor*body.terrestrialSpeedFactor*legs.terrestrialSpeedFactor*tail.terrestrialSpeedFactor;

        aquaticSpeed = speedFactor*( head.aquaticSpeed +  body.aquaticSpeed +  tail.aquaticSpeed +  legs.aquaticSpeed)
            * head.aquaticSpeedFactor*body.aquaticSpeedFactor*legs.aquaticSpeedFactor*tail.aquaticSpeedFactor;

        maxLife = lifeFactor*( head.life +  body.life +  tail.life +  legs.life);

        hungerRate = hungerFactor*maxLife;

        maxHunger = maxLife;

        if(aquaticSpeed < 0)
        {
            aquaticSpeed = 0;
        }
        if(terrestrialSpeed < 0)
        {
            terrestrialSpeed = 0;
        }

        maxAttack = 0;
        if( head.attack !=null)
        {
			attacks.Add(head.attack);
            if(head.attack.damage > maxAttack)
            {
                maxAttack =  head.attack.damage;
                maxAttackInt = 1;

            }
        }
        if( body.attack !=null)
        {
			attacks.Add(body.attack);
            if(body.attack.damage > maxAttack)
            {
                maxAttack =  body.attack.damage;
                maxAttackInt = 2;
            }
        }
        if ( tail.attack !=null)
        {
			attacks.Add(tail.attack);
            if(tail.attack.damage > maxAttack)
            {
                maxAttack =  tail.attack.damage;
                maxAttackInt = 3;
            }
        }
        if ( legs.attack !=null )
        {
			attacks.Add(legs.attack);
            if(legs.attack.damage > maxAttack)
            {
                maxAttack =  legs.attack.damage;
                maxAttackInt = 4;
            }
        }
        if ( legs.secondaryAttack != null )
        {
			attacks.Add(legs.secondaryAttack);
            if(legs.secondaryAttack.damage > maxAttack)
            {
                maxAttack =  legs.secondaryAttack.damage;
                maxAttackInt = 5;
            }
        }
        power = defense + maxAttack;
		if(!playerNamed)
		{
			GenerateName();
		}
    }

	void GenerateName()
	{
		for (int i = 0; i < name.Length; i++)
		{
			name[i] = head.nameFrag[i] + body.nameFrag[i] + legs.nameFrag[i] + tail.nameFrag[i];
		}
	}

	public bool IsMenace(Species rival)
	{
		if(rival.carnivore && rival.maxAttack > defense && rival != this)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
    public Part PartFromType(string type)
    {
        if(type == "head")
        {
            return head;
        }
        else if(type == "body")
        {
            return body;
        }
        else if (type == "tail")
        {
            return tail;
        }
        else
        {
            return legs;
        }
    }

}
