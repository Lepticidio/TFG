﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;


public class Animal : MonoBehaviour
{
    public bool agentSet, rearAttack, test, dead, attackedByPlayer;
    public float hunger, life, lifeToMeat = 50, attackPartDistance;
    float starvationRate;
    public AttackPart maxAttackPart, headAttack;
    float maxAttackDistance;
    public GameObject meat, landDeathParticles, seaDeathParticles, landAttackParticles, seaAttackParticles, minimapIcon, minimapMate;
    public NavMeshAgent agent;
    public Actions actions;
    public AnimalBuilder builder;
    public Species species;
    public Level level;
    public AudioSource source;
    public ParticleSystem landBlood, seaBlood, bubbles, drops;

    // Use this for initialization
    void Start ()
    {
        if(builder == null)
        {
            GetReferences();
        }
        if(!builder.updated)
        {
            UpdateStats();
        }
        var seaCollision = seaBlood.collision;
        seaCollision.SetPlane(0, GameObject.Find("bubbleParticlePlane").transform);
        var bubblesCollision = bubbles.collision;
        bubblesCollision.SetPlane(0, GameObject.Find("bubbleParticlePlane").transform);
        var dropsCollision = drops.collision;
        dropsCollision.SetPlane(0, GameObject.Find("dropsParticlePlane").transform);
        var landCollision = landBlood.collision;
        landCollision.SetPlane(0, GameObject.Find("landParticlePlane").transform);
        bubbles.transform.SetParent(builder.mouth);
        bubbles.transform.localPosition = Vector3.zero;
        bubbles.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        if(!agentSet && agent != null && agent.isOnNavMesh)
        {
            if(species.terrestrialSpeed > 0)
            {
                agent.SetAreaCost(0, 1/species.terrestrialSpeed);
            }
            else
            {
                agent.areaMask = 1 << NavMesh.GetAreaFromName("Water");
            }
            if(species.aquaticSpeed > 0)
            {
                agent.SetAreaCost(3, 1/species.aquaticSpeed);
            }
            else
            {
                agent.areaMask = 1 << NavMesh.GetAreaFromName("Walkable");
            }
            agentSet = true;
        }
        var seaBloodEmission = seaBlood.emission;
        var landBloodEmission = landBlood.emission;
        var dropsEmission = drops.emission;

        if(actions.swimming)
        {
            seaBloodEmission.rateOverTime = 5 * (1f - life/species.maxLife);
            landBloodEmission.rateOverTime = 0;
            if(!bubbles.gameObject.activeSelf)
            {
                bubbles.gameObject.SetActive(true);
            }
            if( transform.position.y > - 3f && actions.moving)
            {
                dropsEmission.rateOverTime = Mathf.Clamp( (transform.position.y + 3) / (-0.1f + 3), 0, 1) * 10;
            }
            else if (drops.gameObject.activeSelf &&( transform.position.y < - 0.5f || !actions.moving))
            {
                dropsEmission.rateOverTime = 0;
            }
        }
        else
        {
            dropsEmission.rateOverTime = 0;
            landBloodEmission.rateOverTime = 5 * (1f - life/species.maxLife);
            seaBloodEmission.rateOverTime = 0;
            if(bubbles.gameObject.activeSelf)
            {
                bubbles.gameObject.SetActive(false);
            }
        }
        if(!test && builder.render.activeSelf)
        {
            if(level.world.millionYears > 50)
            {
                hunger -= species.hungerRate;
            }
            else if (level.world.millionYears > 0 || level.tutorial.phase > 1)
            {
                hunger -= species.hungerRate * 0.67f;
            }
        }
        if(hunger <= 0)
        {
            hunger = 0;
            life -= starvationRate;
        }
        if(life <= 0 && !dead){
            Death();
        }


    }
    void Death()
    {
        dead = true;
        level.music.Play(level.music.fxSource, level.music.death);
        if(actions.swimming)
        {
            var deathCollision =
                Instantiate(seaDeathParticles, transform.position + new Vector3(0, 0, -0.5f), Quaternion.identity).GetComponent<ParticleSystem>().collision;
                deathCollision.SetPlane(0, GameObject.Find("bubbleParticlePlane").transform);

        }
        else
        {
            var deathCollision =
                Instantiate(landDeathParticles, transform.position + new Vector3(0, 0, -0.5f), Quaternion.identity).GetComponent<ParticleSystem>().collision;
                deathCollision.SetPlane(0, GameObject.Find("landParticlePlane").transform);
        }
        int n = (int)(species.maxLife/lifeToMeat);
        for (int i = 0; i < n; i++)
        {
            Transform meatTrans = Instantiate(meat, transform.position, Quaternion.identity).transform;
            meatTrans.Rotate(new Vector3(45, 0, 0));
            if(actions.swimming)
            {
                meatTrans.GetComponent<Rigidbody>().drag = 12;
            }
        }

        if(this.tag == "Player")
        {
            level.world.saver.ClearSaves();
            level.animator.SetTrigger("death");
            level.music.StopAllCoroutines();
            StartCoroutine(level.music.CrossFade(level.music.extinction, 2, 0.8f));
    		StartCoroutine(level.music.FadeOut(level.music.conflictSource, 2));
        }
        else
        {
            level.CreateAnimalHidden(species, level.world.player == species);
            Destroy(transform.parent.gameObject);
        }

    }
    public void UpdateStats()
    {


        GetReferences();
        builder.UpdateAnimal();
        species = builder.species;

        Head head = species.head;
        Body body = species.body;
        Tail tail = species.tail;
        Leg legs = species.legs;

        rearAttack = false;
        headAttack = builder.headPO.attackPart;
        headAttack.species = species;
        actions.attackHead = headAttack;

        if( head.attack !=null)
        {
            actions.attacks.Add(0);
            actions.attackHead.attack = head.attack;
        }
        if( body.attack !=null)
        {
            actions.attacks.Add(1);
            actions.attackBody = builder.bodyPO.attackPart;
            actions.attackBody.attack = body.attack;
        }
        if ( tail.attack !=null)
        {
            actions.attacks.Add(2);
            actions.attackTail = builder.tailPO.attackPart;
            actions.attackTail.attack = tail.attack;
        }
        if ( legs.attack !=null )
        {
            actions.attacks.Add(3);
            actions.attackLeg = builder.leg1.attackPart;
            actions.attackLeg.attack = legs.attack;
        }
        if ( legs.secondaryAttack != null )
        {
            actions.attacks.Add(4);
            actions.attackArm = builder.arm1.attackPart;
            actions.attackArm.attack = legs.secondaryAttack;
        }
        if(species.maxAttackInt == 1)
        {
            maxAttackPart = actions.attackHead;
            attackPartDistance = Mathf.Abs(builder.mouth.position.x - transform.position.x);
        }
        else if (species.maxAttackInt == 2)
        {
            maxAttackPart = actions.attackBody;
        }
        else if (species.maxAttackInt == 3)
        {
            maxAttackPart = actions.attackTail;
            attackPartDistance = Mathf.Abs(builder.centerTail.position.x - transform.position.x);
        }
        else if (species.maxAttackInt == 4)
        {
            maxAttackPart = actions.attackLeg;
            rearAttack = true;
        }
        else if (species.maxAttackInt == 5)
        {
            maxAttackPart = actions.attackArm;
            attackPartDistance = Mathf.Abs(builder.centerArms.position.x - transform.position.x);
        }
        starvationRate = species.hungerRate*2;
        hunger = species.maxHunger;
        life = species.maxLife;
        actions.aquaticSpeed = species.aquaticSpeed;
        actions.terrestrialSpeed = species.terrestrialSpeed;
        actions.senses.species = species;
        actions.minMateDistance = builder.size;
    }
    public void RandomAnimal()
    {
        if(builder == null)
        {
            GetReferences();
        }
        builder.RandomAnimal();
        UpdateStats();
    }
    public void GetReferences()
    {
        agent = transform.parent.GetComponent<NavMeshAgent>();
        actions = GetComponent<Actions>();
        builder = GetComponent<AnimalBuilder>();
        builder.GetReferences();
        level = GameObject.Find("Controller").GetComponent<Level>();
        species = builder.species;

    }
    public void RecieveAttack(Attack attack, AudioClip clip)
    {
        float damage = attack.damage - species.defense;
        if(!dead && !actions.reproduced && (this.tag == "Player" || attackedByPlayer))
        {
            if(life > 0.33f)
            {
                builder.headPO.Play(false);
            }
            else
            {
                builder.headPO.Play(true);
            }
            if(!level.music.inConflict)
            {
                StartCoroutine(level.music.CrossFade(level.music.conflictSource, level.music.mainSource, level.music.conflict, 2, 0.5f));
                level.music.inConflict = true;
            }
            level.music.timeWithoutConflict = 0;
            attackedByPlayer = false;
            source.clip = clip;
            source.volume = 0.7f;
            source.Play();
        }
        if(damage > 0)
        {
            if(actions.swimming)
            {
                var attackCollision =
                    Instantiate(seaAttackParticles, transform.position + new Vector3(0, 0, -0.5f), Quaternion.identity).GetComponent<ParticleSystem>().collision;
                    attackCollision.SetPlane(0, GameObject.Find("bubbleParticlePlane").transform);

            }
            else
            {
                var attackCollision =
                    Instantiate(landAttackParticles, transform.position + new Vector3(0, 0, -0.5f), Quaternion.identity).GetComponent<ParticleSystem>().collision;
                    attackCollision.SetPlane(0, GameObject.Find("landParticlePlane").transform);
            }
            life -= damage;
            if(life < 0)
            {
                life = 0;
            }
        }
    }
}
