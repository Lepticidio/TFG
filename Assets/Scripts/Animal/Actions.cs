﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actions : MonoBehaviour {

    public bool moving, idle, right = true, eating, swimming, breathing, breathes = true, reproduced, isPlayer;
    public int nAttacks;
    public float terrestrialSpeed, aquaticSpeed, angleH, angleV, mouthRadius, minDistance, xSpeed, ySpeed, zSpeed, minMateDistance,
        consumed, maxConsumed = 1f, consumeRate = 1f;
    public AnimationManager animationManager;
    public Senses senses;
    public Transform parent;
    Grid grid;
    Rigidbody rigidbody;
    public Animal animal;
    public AttackPart attackHead, attackBody, attackTail, attackLeg, attackArm;
    public GameFX gameFx;
    public List<int> attacks = new List<int>();
    Mate mate;
    public Tutorial tutorial;
    //
    // Use this for initialization
    public void Awake ()
    {
        parent = transform.parent;
        animal = GetComponent<Animal>();
        grid = GameObject.Find("Controller").GetComponent<Grid>();
        animationManager = gameObject.GetComponentInChildren<AnimationManager>();
        rigidbody = parent.GetComponent<Rigidbody>();
        isPlayer = this.tag == "Player";
        if(isPlayer)
        {
            gameFx = GetComponent<GameFX>();
            tutorial = grid.GetComponent<Level>().tutorial;
        }
    }

    void FixedUpdate()
    {
        if(senses.mouth == null)
        {
            senses.mouth = animal.builder.mouth;
        }
        if(transform.eulerAngles.x != 45)
        {
            transform.eulerAngles = new Vector3 (45, transform.eulerAngles.y, transform.eulerAngles.z);
        }
        if(transform.position.y < -0.1f)
        {
            rigidbody.useGravity = false;
            rigidbody.velocity = Vector3.zero;
            if(breathing)
            {
                breathing = false;
                animationManager.StopBreathing();
            }
        }
        else
        {
            if(breathes&&!breathing)
            {
                breathing = true;
                animationManager.StartBreathing();
            }
            rigidbody.useGravity = true;

        }
        if(Mathf.Abs(xSpeed) > 0 && Mathf.Abs(zSpeed) > 0 )
        {
            xSpeed *= 0.707f;
            zSpeed *= 0.707f;
        }
        rigidbody.MovePosition (transform.parent.position + new Vector3(xSpeed, ySpeed, zSpeed));
    }

    public void Right()
    {
        if
        (
            grid==null||
            (
                (parent.localPosition.x < (grid.limitX-aquaticSpeed*2) && swimming)||
                (parent.localPosition.x < (grid.limitX-terrestrialSpeed*2) && !swimming)
            )
        )
        {
            float speed = 0;
            if(swimming)
            {
                speed = aquaticSpeed;
            }
            else
            {
                speed = terrestrialSpeed;
            }
            Cell cell = CellConversor.GetCell(transform.position.x + speed*2, transform.position.z, grid.cells);
            if
            (
                (cell.aquatic && aquaticSpeed > 0)
                ||
                (!cell.aquatic && terrestrialSpeed > 0)

            )
            {
                xSpeed = speed;
            }
            else
            {
                xSpeed = 0;
            }
            CheckMovement(cell);
        }
        if(!right)
        {
            Flip();
        }
    }
    public void Left()
    {
        if
        (
            grid==null||
            (
                (parent.localPosition.x > (-grid.limitX+aquaticSpeed*2) && swimming)||
                (parent.localPosition.x > (-grid.limitX+terrestrialSpeed*2) && !swimming)
            )
        )
        {
            float speed = 0;
            if(swimming)
            {
                speed = aquaticSpeed;
            }
            else
            {
                speed = terrestrialSpeed;
            }
            Cell cell = CellConversor.GetCell(transform.position.x - speed*2, transform.position.z, grid.cells);
            if
            (
                (cell.aquatic && aquaticSpeed > 0)
                ||
                (!cell.aquatic && terrestrialSpeed > 0)

            )
            {
                xSpeed = -speed;
            }
            else
            {
                xSpeed = 0;
            }
            CheckMovement(cell);
        }
        if(right)
        {
            Flip();
        }

    }
    public void Straight()
    {
        if
        (
            grid==null||
            (
                (parent.localPosition.z < (grid.limitZ-aquaticSpeed*2) && swimming)||
                (parent.localPosition.z < (grid.limitZ-terrestrialSpeed*2) && !swimming)
            )
        )
        {
            float speed = 0;
            if(swimming)
            {
                speed = aquaticSpeed;
            }
            else
            {
                speed = terrestrialSpeed;
            }
            Cell cell = CellConversor.GetCell(transform.position.x, transform.position.z + speed*2, grid.cells);
            if
            (
                (cell.aquatic && aquaticSpeed > 0)
                ||
                (!cell.aquatic && terrestrialSpeed > 0)

            )
            {
                zSpeed = speed;
            }
            else
            {
                zSpeed = 0;
            }
            CheckMovement(cell);
        }
    }
    public void Back()
    {
        if
        (
            grid==null||
            (
                (parent.localPosition.z > (-grid.limitZ+aquaticSpeed*2) && swimming)||
                (parent.localPosition.z > (-grid.limitZ+terrestrialSpeed*2) && !swimming)
            )
        )
        {
            float speed = 0;
            if(swimming)
            {
                speed = aquaticSpeed;
            }
            else
            {
                speed = terrestrialSpeed;
            }
            Cell cell = CellConversor.GetCell(transform.position.x, transform.position.z - speed*2, grid.cells);
            if
            (
                (cell.aquatic && aquaticSpeed > 0)
                ||
                (!cell.aquatic && terrestrialSpeed > 0)

            )
            {
                zSpeed = -speed;
            }
            else
            {
                zSpeed = 0;
            }
            CheckMovement(cell);
        }

    }

    public void Up()
    {
        if(swimming&&transform.position.y<-0.1f)
        {
            float speed = aquaticSpeed;
            ySpeed = speed;
        }
        CheckMovement(CellConversor.GetCell(transform.position,grid.cells));
    }
    public void Down()
    {
        if(swimming)
        {
            float speed = aquaticSpeed;
            ySpeed = -speed;
        }
        CheckMovement(CellConversor.GetCell(transform.position,grid.cells));

    }

    public void StopXMovement()
    {
        xSpeed = 0;
    }

    public void StopYMovement()
    {
        ySpeed = 0;
    }

    public void StopZMovement()
    {
        zSpeed = 0;
    }

    public void Eat()
    {
        animationManager.Eat();
        if( attackHead.possibleMeals.Count > 0)
        {
            Food food = attackHead.possibleMeals[0];
            eating = true;
            consumed += consumeRate*Time.deltaTime;
            if(!food.eatingParticles.isPlaying)
            {
                food.eatingParticles.Play();
            }
            if(consumed > maxConsumed)
            {
                animal.hunger += food.nutrition;
                if(animal.hunger > animal.species.maxHunger)
                {
                    animal.hunger = animal.species.maxHunger;
                }
                attackHead.possibleMeals.Remove(food);
                senses.RemoveFood(food);
                food.eatingParticles.Stop();
                food.Eaten(isPlayer);
                consumed = 0;
                if(isPlayer)
                {
                    tutorial.eaten = true;
                    gameFx.Play(gameFx.eating, gameFx.swallowing);
                }
            }
            else if (isPlayer && ! gameFx.eating.isPlaying)
            {
                if(food.plankton)
                {
                    gameFx.Play(gameFx.eating, gameFx.filterFeeding);
                }
                else
                {
                    gameFx.Play(gameFx.eating, gameFx.chewing);
                }
            }
        }
    }

    public void FinishEat()
    {
        consumed = 0;
        eating = false;
        animationManager.FinishEat();
        if(isPlayer && gameFx.eating.clip == gameFx.chewing && gameFx.eating.isPlaying)
        {
            gameFx.eating.Stop();
        }
    }

    public void HeadAttack()
    {
        if(attackHead != null && !attackHead.attacking)
        {
            attackHead.Attack(isPlayer);
            animationManager.HeadAttack();
        }
    }

    public void BodyAttack()
    {
        if(attackBody != null && !attackBody.attacking)
        {
            attackBody.Attack(isPlayer);
            animationManager.BodyAttack();
        }
    }
    public void LegAttack()
    {
        if(attackArm != null && !attackArm.attacking)
        {
            attackArm.Attack(isPlayer);
            animationManager.ArmAttack();
        }
        else if(attackLeg != null && !attackLeg.attacking)
        {
            attackLeg.Attack(isPlayer);
            animationManager.LegAttack();
        }
    }

    public void TailAttack()
    {
        if(attackTail != null && !attackTail.attacking)
        {
            attackTail.Attack(isPlayer);
            animationManager.TailAttack();
        }
    }

    void CheckWater(Cell cell){
        if(cell.aquatic)
        {
            if(!swimming)
            {
                if (moving)
                {
                    animationManager.SetMovementSpeed(1f);
                    animationManager.Swim();
                }
                else
                {
                    animationManager.SetMovementSpeed(0.35f);
                    animationManager.Swim();

                }
                swimming = true;
            }
        }
        else
        {
            if(swimming)
            {
                transform.eulerAngles = new Vector3 (0,0, 0);
                if (moving)
                {
                    animationManager.SetMovementSpeed(1f);
                    animationManager.Walk();
                }
                else
                {
                    animationManager.SetMovementSpeed(1f);
                    animationManager.Idle();
                }
                swimming = false;
            }
        }
    }
//
    void CheckMovement(Cell cell)
    {
        if(eating)
        {
            FinishEat();
        }
        CheckWater(cell);
        if(!moving)
        {
            animationManager.SetMovementSpeed(1f);
            if (swimming)
            {
                animationManager.Swim();
                if(isPlayer && transform.position.y > -3f)
                {
                    gameFx.Play(gameFx.movement, gameFx.swim, 0.5f*(1f - transform.position.y/-3f));
                }
            }
            else
            {
                animationManager.Walk();
                if(isPlayer)
                {
                    gameFx.Play(gameFx.movement, gameFx.steps, 0.7f);
                }
            }
            idle= false;
            moving = true;
        }
    }
    public void Idle()
    {
        CheckWater(CellConversor.GetCell(transform.position,grid.cells));
        if(!idle)
        {
            if(isPlayer)
            {
                gameFx.movement.Stop();
            }
            xSpeed = 0;
            zSpeed = 0;
            ySpeed = 0;
            angleH = 42;
            angleV = 42;
            if(swimming)
            {
                animationManager.SetMovementSpeed(0.35f);
                animationManager.Swim();
            }
            else{
                animationManager.Idle();
            }
            moving = false;
            idle = true;
        }
    }
    public void Flip()
    {
        parent.localScale = new Vector3 (-parent.localScale.x,parent.localScale.y,parent.localScale.z );
        if(right)
        {
            right = false;
        }
        else
        {
            right = true;
        }
    }

    public void Move (Vector3 dir)
    {
        if(dir.x > 0)
        {
            Right();
        }
        else if (dir.x < 0)
        {
            Left();
        }
        else
        {
            StopXMovement();
        }
        if(dir.y > 0)
        {
            Up();
        }
        else if (dir.y < 0)
        {
            Down();
        }
        else
        {
            StopYMovement();
        }
        if(dir.z > 0)
        {
            Straight();
        }
        else if (dir.z < 0)
        {
            Back();
        }
        else
        {
            StopZMovement();
        }
    }
    public void ReproduceOrEat()
    {
        minMateDistance = animal.builder.size;
        if(senses.mateDistance < minMateDistance)
        {
            Debug.Log("Reproducing");
            Level level = grid.GetComponent<Level>();
            if(level.matingSeason && !reproduced)
            {
                World world = level.world;
                Part part = senses.closestMate.GetComponent<Mate>().part;
        		world.ChangePlayerPart(part);
        		world.AddPlayerMutation();
                world.Save(1);
                level.LoadEvolution();
                GetComponent<AmbienceControl>().StartFadeOut();
                reproduced = true;
            }
        }
        else
        {
            Eat();
        }
    }
    public void ReproduceCheat()
    {
        if(!reproduced)
        {
            Level level = grid.GetComponent<Level>();
            World world = level.world;
            Part part = world.futurePlayer.SuggestMutation();
    		world.ChangePlayerPart(part);
    		world.AddPlayerMutation();
            world.Save(1);
            level.LoadEvolution();
            GetComponent<AmbienceControl>().StartFadeOut();
            reproduced = true;
        }
    }
}
