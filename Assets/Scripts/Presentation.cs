﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;


public class Presentation : MonoBehaviour
{
	bool evolving;
	int current;
	public List <Slide> slides = new List <Slide>();
	public List <TextMeshProUGUI> texts = new List <TextMeshProUGUI>();
	public TextProcessor processor;
	Slide previous;
	Animator animator;
	public Transform slidesParent;


	public AnimalBuilder whiteBuilder, colorBuilder;
	Disassembler disassembler;
	public float evolutionTime = 2;



    void Evolving()
    {
		if(evolving)
		{
			whiteBuilder.species = whiteBuilder.species.Evolve(1);
			whiteBuilder.UpdateAnimal();
			colorBuilder.species = whiteBuilder.species;
			colorBuilder.UpdateAnimal();
			if(whiteBuilder.species.aquaticSpeed > whiteBuilder.species.terrestrialSpeed)
			{
				whiteBuilder.animationManager.Swim();
				colorBuilder.animationManager.Swim();
			}
			else
			{
				whiteBuilder.animationManager.Walk();
				colorBuilder.animationManager.Walk();
			}
		}
	//	Invoke("Evolving", evolutionTime);
    }

	void Start()
	{
		animator = GetComponent<Animator>();
		whiteBuilder.pureWhite = true;
		whiteBuilder.species = new Species(0,0,0,0);
		whiteBuilder.UpdateAnimal();
		colorBuilder.species = whiteBuilder.species;
		colorBuilder.UpdateAnimal();
	//	Invoke("Evolving", evolutionTime + 0.56f);
		slides = slidesParent.GetComponentsInChildren<Slide>().ToList();

		Debug.Log("starting");
		CheckChanges();
	}

	void Update  ()
	{
		if(Input.GetKeyDown("right") || Input.GetKeyDown("space") || Input.GetKeyDown("d"))
		{
			Next();
		}
		if(Input.GetKeyDown("left") || Input.GetKeyDown("a"))
		{
			Previous();
		}
	}
	public void Next()
	{
		previous = slides[current];
		if(current < slides.Count - 1)
		{
			current++;
			animator.SetTrigger(slides[current].name + "In");
			CheckChanges();
		}
		else
		{
			Misc.LoadScene("Start");
		}
	}
	public void Previous()
	{
		previous = slides[current];
		if(current > 0)
		{
			current--;
			animator.SetTrigger(previous.name + "Out");
			CheckChanges();
		}
	}
	public void CheckChanges()
	{
		Debug.Log("checking");
		for(int i = 0; i < texts.Count; i++)
		{
			Debug.Log("iteration");
			if(previous == null || slides[current].strings[i] != previous.strings[i])
			{
				Debug.Log("going to write");
				processor.Write(slides[current].strings[i], texts[i]);
			}
		}
		if(slides[current].disassembler)
		{
			evolving = false;
			disassembler.Disassemble();
		}
		else
		{
			evolving = true;
		}
		if(previous && previous.white && !slides[current].white)
		{
			whiteBuilder.StartFadeOut();
			colorBuilder.StartFadeIn();
			Invoke("DeactivateWhite", 1);
			//evolving = false;

		}
		else if (previous && !previous.white && slides[current].white)
		{
			whiteBuilder.StartFadeIn();
			colorBuilder.StartFadeOut();
			//evolving = false;

		}
		else if (previous)
		{
			evolving = true;
			Evolving();
		}
	}

	void DeactivateWhite()
	{
		whiteBuilder.gameObject.SetActive(false);
	    colorBuilder.ChangeMaterial(colorBuilder.cutoutMaterial);
	}
}
