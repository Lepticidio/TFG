using UnityEngine;
using UnityEditor;
using Anima2D;

#if UNITY_EDITOR
public class EditModeFunctions : EditorWindow
 {
    static GUIStyle randomizers, allCreators, destroyers, defaultCreators, newCreators, updators;

     [MenuItem("Window/Edit Mode Functions")]
     public static void ShowWindow()
     {
         GetWindow<EditModeFunctions>("Edit Mode Functions");
     }

     private void OnGUI()
     {
         randomizers = new GUIStyle(GUI.skin.GetStyle("Button"));
         allCreators = new GUIStyle(GUI.skin.GetStyle("Button"));
         destroyers = new GUIStyle(GUI.skin.GetStyle("Button"));
         defaultCreators = new GUIStyle(GUI.skin.GetStyle("Button"));
         newCreators = new GUIStyle(GUI.skin.GetStyle("Button"));
         updators = new GUIStyle(GUI.skin.GetStyle("Button"));
         randomizers.fontStyle = FontStyle.Bold;
         randomizers.normal.textColor = new Color (0.25f, 0.25f, 0);
         allCreators.fontStyle = FontStyle.Bold;
         allCreators.normal.textColor = Color.blue;
         destroyers.fontStyle = FontStyle.Bold;
         destroyers.normal.textColor = Color.red;
         defaultCreators.fontStyle = FontStyle.Bold;
         defaultCreators.normal.textColor=  new Color (0,0.5f,0);
         newCreators.fontStyle = FontStyle.Bold;
         newCreators.normal.textColor =  new Color (0,0.25f,0);
         updators.fontStyle = FontStyle.Bold;
         updators.normal.textColor = new Color(0.5f,0,1f);

         if (GUILayout.Button("Start",newCreators))
         {
             GoToStart();
         }

         if (GUILayout.Button("Menu",newCreators))
         {
             GoToMenu();
         }

         if (GUILayout.Button("Level",newCreators))
         {
             GoToLevel();
         }

         if (GUILayout.Button("Editor",defaultCreators))
         {
             GoToEditor();
         }

         if (GUILayout.Button("Evolution",defaultCreators))
         {
             GoToEvolution();
         }

         if (GUILayout.Button("Test",newCreators))
         {
             GoToTest();
         }


         if (GUILayout.Button("Random Animal",randomizers))
         {
             RandomAnimal();
         }
         if (GUILayout.Button("Create Animal",allCreators))
         {
             CreateAnimal();
         }
         if (GUILayout.Button("Destroy Animal",destroyers))
         {
             DestroyAnimal();
             GUI.skin.button.onNormal.textColor = Color.red;
         }
	      if (GUILayout.Button("Update Colors", allCreators))
         {
             UpdateColor();
         }
				 if (GUILayout.Button("Clear Part Lists", updators))
         {
             ClearLists();
         }
				 if (GUILayout.Button("Load Part Lists", updators))
         {
             LoadLists();
         }

         if (GUILayout.Button("Presentation",newCreators))
         {
             GoToPresentation();
         }

         if (GUILayout.Button("Trailer",allCreators))
         {
             GoToTrailer();
         }
     }

     private static void GoToStart()
     {
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Scenes/Start.unity");

     }

     private static void GoToPresentation()
     {
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Scenes/Presentation.unity");

     }

     private static void GoToTrailer()
     {
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Scenes/Trailer.unity");

     }

     private static void GoToMenu()
     {
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Scenes/Menu.unity");

     }

     private void GoToEditor()
     {
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Scenes/Editor.unity");

     }

     private void GoToLevel()
     {
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Scenes/Level.unity");

     }

     private void GoToEvolution()
     {
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Scenes/Evolution.unity");

     }

     private void GoToTest()
     {
        UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Scenes/Testing.unity");

     }

     private void RandomAnimal()
     {
        GameObject.Find("Controller").GetComponent<PartPositionSaver>().RandomAnimal();

     }

     private void CreateAnimal()
     {
         GameObject.Find("Controller").GetComponent<PartPositionSaver>().Create();
     }

     private void UpdateColor()
     {
          /*
         GameObject[] gos = GameObject.FindGameObjectsWithTag("NPC");
         foreach(GameObject go in gos)
         {
           go.GetComponent<AnimalBuilder>().UpdateColor();
         }
         */
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Color");
        foreach(GameObject go in gos)
        {
          go.GetComponent<SpriteMeshInstance>().color = new Color (0,0,0);
        }
        GameObject[] gos2 = GameObject.FindGameObjectsWithTag("SecondaryColor");
        foreach(GameObject go in gos2)
        {
         go.GetComponent<SpriteMeshInstance>().color = new Color (0,0,0);
        }
        GameObject[] gos3 = GameObject.FindGameObjectsWithTag("EyeColor");
        foreach(GameObject go in gos3)
        {
          go.GetComponent<SpriteMeshInstance>().color = new Color (0,0,0);
        }

     }

     private void DestroyAnimal()
     {
         GameObject.Find("Controller").GetComponent<PartPositionSaver>().DestroyAnimal();
     }

     private void ClearLists()
     {
         GameObject.Find("Data").GetComponent<PartData>().Clear();
     }

     private void LoadLists()
     {
         GameObject.Find("Data").GetComponent<PartData>().Load();
     }
 }
 #endif
