﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorConversion {


	public static Color HSLtoRGB(float h, float s,float l){
		float c, hh, x, rr = 0, gg = 0, bb = 0, m;
		if (h< 0){
			h+=360;
		}
		c = (1 - Mathf.Abs (2 * l - 1)) * s;
		hh = h / 60;
		x = c * (1 - Mathf.Abs (hh % 2 - 1));
		if (hh < 1) {
			rr = c;
			gg = x;
			bb = 0;
		}
		else if(hh<2){
			rr = x;
			gg = c;
			bb = 0;
		}
		else if(hh<3){
			rr = 0;
			gg = c;
			bb = x;
		}
		else if(hh<4){
			rr = 0;
			gg = x;
			bb = c;
		}
		else if(hh<5){
			rr = x;
			gg = 0;
			bb = c;
		}
		else if(hh<6){
			rr = c;
			gg = 0;
			bb = x;
		}
		m = l - c / 2;
		return new Color(rr+m,gg+m,bb+m);

	}
	public static float[] HSLFromRGB (Color color)
	{
		return HSLFromRGB(color.r, color.g, color.b);
	}



	public static float[] HSLFromRGB(float R, float G, float B)
	{
		float Min = Mathf.Min(Mathf.Min(R, G), B);
		float Max = Mathf.Max(Mathf.Max(R, G), B);
		float Chroma = Max - Min;

		float hue = 0;
		float saturation = 0;
		float luminosity = (float)((Max + Min) / 2.0f);
		if (luminosity < 1f)
		{
			saturation = (float)(Chroma / (1-Mathf.Abs(2*luminosity-1)));
		}
		else
		{
			saturation = 0;
		}

		if (Chroma != 0) {


			if (R == Max) {
				hue = 60 * (((G - B) / Chroma) % 6);
			} else if (G == Max) {
				hue = 60 * (2f + (B - R) / Chroma);
			} else if (B == Max) {
				hue = 60 * (4f + (R - G) / Chroma);
			}
		} else {
			hue = 0;
		}

		return new float[] { hue, saturation, luminosity};
	}
}
