﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockGenerator : MonoBehaviour
{
	List <int> tempInts = new List<int>();
	public static List<int> possibleInts = new List<int>();
	public int numberShapesRocks, numberTypesRocks;
	public static int smallRock, mediumRock, bigRock;
	public List<Color> colors = new List<Color>();
	public static Color color;

	public void GenerateRocks()
	{
		color = colors[Random.Range(0, colors.Count)];

		for (int i = 0; i < numberShapesRocks; i++)
		{
			tempInts.Add(i);
		}
		for (int i = 0; i < numberTypesRocks; i++)
		{
			int random = Random.Range(0, tempInts.Count);
			possibleInts.Add(tempInts[random]);
		//	tempInts.RemoveAt(random);
		}
	}
}
