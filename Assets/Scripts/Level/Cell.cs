﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell
{
    public bool filled, blocked, aquatic, closed;
    public int x, y, g, h, f;
    public float altitude, blockedHeight;
    public GameObject item;
    public Cell parent;
    //In neighbours, 0 is up, 1 is upRight, 2 is right, 3 is downRight...
    //Clockwise direction for numering starting with up, evens are direct and odds are diagonal.
    public Cell[] neighbours = new Cell[8];

    public Cell (int xx, int yy){
        x = xx;
        y = yy;
    }

    public Cell (Cell cell)
    {
        x = cell.x;
        y = cell.y;
        filled = cell.filled;
        blocked = cell.blocked;
        aquatic = cell.aquatic;
        altitude = cell.altitude;
        neighbours = cell.neighbours;
        item = cell.item;
    }
}
