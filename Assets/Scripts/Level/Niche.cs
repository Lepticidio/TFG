﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Niche
{

	public bool carnivore, planktivore, aquatic, speed, power;
	public int herbivore, abundance;
	public float minAttack = 0, itemAbundance, sealevelAbundance = 0;
	public Item item;
	public Biome biome;

	public Species species;

	public Niche (Biome bio, Item itemGiven, bool sp, float abun)
	{
		biome = bio;
		item = itemGiven;
		itemAbundance = abun;
		if (item.type == "Food")
		{
			Food food = item as Food;
			if(food.plankton)
			{
				planktivore = true;
			}
			else if(food.vegetable)
			{
				herbivore =item.hardness;
			}
			else if(food.meat)
			{
				carnivore = true;
			}
		}
		else
		{
			herbivore = item.hardness;
		}
		if(item.maxAltitude < 0)
		{
			aquatic = true;
		}

		float minLevel =  - biome.sealevel;
		float maxLevel = 1 - biome.sealevel;
		float diffUp = 0;
		float diffDown = 0;
		if (minLevel > item.minAltitude)
		{
			diffDown = minLevel - item.minAltitude;
		}
		if (item.maxAltitude > maxLevel)
		{
			diffUp = item.maxAltitude - maxLevel;
		}

		sealevelAbundance = item.maxAltitude - item.minAltitude - diffUp - diffDown;


		if(sp)
		{
			speed = true;
		}
		else
		{
			power = true;
		}
		abundance = (int)(itemAbundance * item.nutrition*4*sealevelAbundance);
	}

	public Niche (Biome bio, Niche niche)
	{
		if(niche.species != null)
		{
			biome = bio;
			carnivore = true;
			aquatic = niche.aquatic;
			speed = niche.speed;
			power = niche.power;
			minAttack = niche.species.defense;
			abundance = niche.abundance / 4;

		}
	}
	public void Compete(Species competitor)
	{
		if((aquatic && competitor.aquaticSpeed > 0)||(!aquatic && competitor.terrestrialSpeed > 0))
		{
			if((!carnivore && !planktivore && competitor.herbivoreEfficacy >= herbivore) || (carnivore &&competitor.carnivore && competitor.maxAttack > minAttack) ||  (planktivore &&competitor.planktivore) )
			{
				if(species == null)
				{
					species = competitor;
				}
				else if(power && competitor.power > species.power)
				{
					if(competitor.power > species.power)
					{
						species = competitor;
					}
					else if (competitor.power == species.power)
					{
						if(aquatic &&competitor.aquaticSpeed > species.aquaticSpeed)
						{
							species = competitor;
						}
						else if (!aquatic && competitor.terrestrialSpeed > species.terrestrialSpeed)
						{
							species = competitor;
						}
					}
				}
				else if (speed)
				{
					if(aquatic)
					{
						if(competitor.aquaticSpeed > species.aquaticSpeed)
						{
							species = competitor;
						}
						else if (competitor.aquaticSpeed == species.aquaticSpeed &&
						 	(competitor.power >= species.power && competitor.terrestrialSpeed >= species.terrestrialSpeed && competitor.dietLevel >= species.dietLevel))
						{
							species = competitor;
						}
					}
					else
					{
						if(competitor.terrestrialSpeed > species.terrestrialSpeed)
						{
							species = competitor;
						}
						else if (competitor.terrestrialSpeed == species.terrestrialSpeed &&
						 	(competitor.power >= species.power && competitor.aquaticSpeed >= species.aquaticSpeed && competitor.dietLevel >= species.dietLevel))
						{
							species = competitor;
						}

					}
				}
			}
		}

	}
}
