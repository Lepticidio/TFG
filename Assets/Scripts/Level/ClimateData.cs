﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimateData : MonoBehaviour
{
	public float temperature = 35f, humidity = 150f, sealevel = 0.5f;
}
