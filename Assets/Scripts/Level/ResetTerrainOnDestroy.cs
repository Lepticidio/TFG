﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTerrainOnDestroy : MonoBehaviour
{

		public Terrain terrain;

		private float[,] originalHeights;



		private void OnDestroy()
		{
			float[,] zeroHeights = new float[terrain.terrainData.heightmapWidth,terrain.terrainData.heightmapHeight];
			terrain.terrainData.SetHeights(0, 0, zeroHeights);
		}
}
