using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using System.Linq;

public class Level : MonoBehaviour
{
    public bool terrainTest, animalTest, matingSeason, finishedLoading, finishedItems;
    public int width = 1024, height = 1024, depth = 1024, minMates = 20, nMates;
    int terrainWidth, terrainHeight, terrainSize, hmWidth, hmHeight;
    public float sealevel, itemAbundance, offsetX, offsetZ, scale=1, heteroscale =1, textureScale = 10, altitudeFactor, altitudeOffset, seaDepth,
        initialDistancePredators = 10;
    public float matingTime = 120, time, colorTransition = 3;
    public Vector3 initialPlayerPos = Vector3.zero;
    public Transform plants;
    public Transform decorations;
    public Transform foods;
    public Transform animals;
    public GameObject terrainPrefab, terrainPrefab2, animal, player;
    public Terrain terr, borderTerr;
    public ItemData itemData;
    public Grid grid;
    RockGenerator rockGenerator;
    NavMeshSurface surface;
    public Biome biome;
    public World world;
    public EvolutionUI evolutionUI;
    float[,] heights = new float[513, 513];
    public TerrainCell[,] terrainCells = new TerrainCell[64,64];
    public List<float> itemHeteroffsets = new List<float>();
    public List<Niche> niches = new List<Niche>();
    public List<Species> species = new List<Species>();
    public Image loadingImage;
    public GameObject hud;
    public Image fadeImage;
    public Animator animator;
    public MusicControl music;
    public Tutorial tutorial;

    // Use this for initialization
    void Start ()
    {
        StartCoroutine("GenerateLevel");
        if(world.millionYears < world.yearsPerGeneration)
        {
            matingTime *= 0.4f;
            minMates *= 5;
            tutorial.gameObject.SetActive(true);
        }
        else if (world.millionYears < world.yearsPerGeneration * 2)
        {
            matingTime *= 0.7f;
            minMates *= 3;
        }
        else if (world.millionYears < world.yearsPerGeneration * 3)
        {
            minMates *= 2;
        }
        if(world.player.antecessor != null && world.player.antecessor.planktivore && !world.player.planktivore)
        {
            tutorial.gameObject.SetActive(true);
            tutorial.phase = 4;
        }
    }

    void Update()
    {

        if(Input.GetKeyDown("u"))
        {
            hud.transform.parent.gameObject.SetActive(false);
        }
        if(!matingSeason && finishedLoading)
        {
            time += Time.deltaTime;
            if(time > matingTime)
            {
                matingSeason = true;
            }
        }
    }

    float CalculateAltitude(int x, int y)
    {

        float xPerl = ((float)x / width) * scale + offsetX;
        float yPerl = ((float)y / height) * scale + offsetZ;
        float perlinNoise = Mathf.PerlinNoise (xPerl, yPerl);
        return perlinNoise;
    }

    float CalculateHeterogeneity(int x, int y, float heteroffset)
    {

        float xPerl = ((float)x / width) * heteroscale + heteroffset;
        float yPerl = ((float)y / height) * heteroscale + heteroffset;
        float perlinNoise = Mathf.PerlinNoise (xPerl, yPerl);
        return perlinNoise;
    }

    float CalculateTextureNoise(int x, int y, float textureOffset, float umbral, float localScale)
    {

        float xPerl = ((float)x / width) * localScale + textureOffset;
        float yPerl = ((float)y / height) * localScale + textureOffset;
        float perlinNoise = Mathf.PerlinNoise (xPerl, yPerl);
        return Mathf.Clamp((perlinNoise - (umbral - 0.1f)) * 5, 0, 1);
    }

    void GenerateCells()
    {
        float textureOffset = Random.Range(0f, 99999f);
        float underwaterOffset = Random.Range(0f, 99999f);
        float temperatureOffset = Random.Range(0f, 99999f);
        float humidityOffset = Random.Range(0f, 99999f);

        float [,] tempHeightMap = new float[513,513];
        float [,] tempHeightMap2 = new float[513,513];
        float[,,] tempSplatMap = new float[512,512, 10];
        float[,,] tempSplatMap2 = new float[512,512,10];
        for (int i = 0; i< width;i++)
        {
            for (int j = 0; j< height; j++)
            {
                Cell cell  = grid.cells[i,j];
                cell.altitude = CalculateAltitude(i,j);
                float textureAltitude = Mathf.Clamp(((cell.altitude - sealevel) * 10), 0, 1);

                float textureNoise = CalculateTextureNoise(i, j, textureOffset, 0.75f, 6);
                float underWaterNoise = CalculateTextureNoise(i, j, underwaterOffset, 0.25f, 10);

                float snowAmount = 1 - Mathf.Clamp(biome.temperature / 10, 0, 1);
                float snowNoise = CalculateTextureNoise(i, j, temperatureOffset, snowAmount, 2);

                float sandAmount = 1 - Mathf.Clamp(biome.humidity / 50, 0, 1);
                float sandNoise = CalculateTextureNoise(i, j, humidityOffset, sandAmount, 2);

                float mudAmount = Mathf.Clamp((biome.humidity / 400) - 300, 0, 1);
                float mudNoise = CalculateTextureNoise(i, j, humidityOffset, mudAmount, 2);

                int remainderI = (int)(i%(width/(hmWidth-1)));
                int remainderJ = (int)(j%(width/(hmWidth-1)));
                if (cell.altitude<sealevel){
                    cell.aquatic = true;
                    if (remainderI==0&&remainderJ==0)
                    {
                        if(cell.altitude > sealevel - seaDepth)
                        {
                            tempHeightMap[j/2,i/2] = cell.altitude;
                        }
                        else
                        {
                            tempHeightMap[j/2,i/2] = sealevel- seaDepth;
                        }
                    }

                }
                else{
                    if (remainderI==0&&remainderJ==0)
                    {
                        tempHeightMap[j/2,i/2] = sealevel;
                    }
                }
                if(i != 513 && j != 513)
                {
                    tempSplatMap[j/2,i/2, 0] = 1;
                    tempSplatMap[j/2,i/2, 1] = 1;
                    tempSplatMap[j/2,i/2, 2] = 1;
                    tempSplatMap[j/2,i/2, 3] = 1;
                    tempSplatMap[j/2,i/2, 4] = 1;
                    tempSplatMap[j/2,i/2, 5] = 1;
                    tempSplatMap[j/2,i/2, 6] = 1;
                    tempSplatMap[j/2,i/2, 7] = 1;
                    tempSplatMap[j/2,i/2, 8] = 1;
                    tempSplatMap[j/2,i/2, 9] = 1;

                    tempSplatMap[j/2,i/2, 0] *= 1 - textureAltitude;
                    tempSplatMap[j/2,i/2, 1] *= 1 - textureAltitude;
                    tempSplatMap[j/2,i/2, 2] *= textureAltitude;
                    tempSplatMap[j/2,i/2, 3] *= textureAltitude;
                    tempSplatMap[j/2,i/2, 4] *= textureAltitude;
                    tempSplatMap[j/2,i/2, 5] *= textureAltitude;
                    tempSplatMap[j/2,i/2, 6] *= textureAltitude;
                    tempSplatMap[j/2,i/2, 7] *= textureAltitude;
                    tempSplatMap[j/2,i/2, 8] *= textureAltitude;
                    tempSplatMap[j/2,i/2, 9] *= textureAltitude;

                    tempSplatMap[j/2,i/2, 0] *= underWaterNoise;
                    tempSplatMap[j/2,i/2, 2] *= textureNoise;
                    tempSplatMap[j/2,i/2, 4] *= textureNoise;
                    tempSplatMap[j/2,i/2, 6] *= textureNoise;
                    tempSplatMap[j/2,i/2, 8] *= textureNoise;
                    tempSplatMap[j/2,i/2, 1] *= 1 - underWaterNoise;
                    tempSplatMap[j/2,i/2, 3] *= 1 - textureNoise;
                    tempSplatMap[j/2,i/2, 5] *= 1 - textureNoise;
                    tempSplatMap[j/2,i/2, 7] *= 1 - textureNoise;
                    tempSplatMap[j/2,i/2, 9] *= 1 - textureNoise;

                    if(biome.temperature < 10)
                    {
                        tempSplatMap[j/2,i/2, 2] *= 1 - snowNoise;
                        tempSplatMap[j/2,i/2, 3] *= 1 - snowNoise;
                        tempSplatMap[j/2,i/2, 4] *= snowNoise;
                        tempSplatMap[j/2,i/2, 5] *= snowNoise;
                        tempSplatMap[j/2,i/2, 6] *= snowNoise;
                        tempSplatMap[j/2,i/2, 7] *= snowNoise;
                        tempSplatMap[j/2,i/2, 8] *= snowNoise;
                        tempSplatMap[j/2,i/2, 9] *= snowNoise;
                    }
                    else
                    {
                        tempSplatMap[j/2,i/2, 2] *= 0;
                        tempSplatMap[j/2,i/2, 3] *= 0;

                    }
                    if(biome.humidity < 150)
                    {
                        tempSplatMap[j/2,i/2, 4] *= sandNoise;
                        tempSplatMap[j/2,i/2, 5] *= sandNoise;
                        tempSplatMap[j/2,i/2, 6] *= 1 - sandNoise;
                        tempSplatMap[j/2,i/2, 7] *= 1 - sandNoise;
                        tempSplatMap[j/2,i/2, 8] *= 0;
                        tempSplatMap[j/2,i/2, 9] *= 0;
                    }
                    else if (biome.humidity > 200)
                    {
                        tempSplatMap[j/2,i/2, 4] *= mudNoise;
                        tempSplatMap[j/2,i/2, 5] *= mudNoise;
                        tempSplatMap[j/2,i/2, 6] *= 0;
                        tempSplatMap[j/2,i/2, 7] *= 0;
                        tempSplatMap[j/2,i/2, 8] *= 1 - mudNoise;
                        tempSplatMap[j/2,i/2, 9] *= 1 - mudNoise;
                    }
                    else
                    {
                        tempSplatMap[j/2,i/2, 4] *= 1;
                        tempSplatMap[j/2,i/2, 5] *= 1;
                        tempSplatMap[j/2,i/2, 6] *= 0;
                        tempSplatMap[j/2,i/2, 7] *= 0;
                        tempSplatMap[j/2,i/2, 8] *= 0;
                        tempSplatMap[j/2,i/2, 9] *= 0;

                    }

                }
            }
        }
        for (int i = 0; i< width;i++)
        {
            for (int j = height; j< height + height; j++)
            {

                float altitude = CalculateAltitude(i,j);
                float textureAltitude = Mathf.Clamp(((altitude - sealevel) * 10), 0, 1);

                float textureNoise = CalculateTextureNoise(i, j, textureOffset, 0.75f, 6);
                float underWaterNoise = CalculateTextureNoise(i, j, underwaterOffset, 0.25f, 10);

                float snowAmount = 1 - Mathf.Clamp(biome.temperature / 10, 0, 1);
                float snowNoise = CalculateTextureNoise(i, j, temperatureOffset, snowAmount, 2);

                float sandAmount = 1 - Mathf.Clamp(biome.humidity / 50, 0, 1);
                float sandNoise = CalculateTextureNoise(i, j, humidityOffset, sandAmount, 2);

                float mudAmount = Mathf.Clamp((biome.humidity / 400) - 300, 0, 1);
                float mudNoise = CalculateTextureNoise(i, j, humidityOffset, mudAmount, 2);


                int remainderI = (int)(i%(width/(hmWidth-1)));
                int remainderJ = (int)(j%(width/(hmWidth-1)));

                if (altitude < sealevel)
                {
                    if (remainderI==0&&remainderJ==0)
                    {
                        if(altitude > sealevel - seaDepth)
                        {
                            tempHeightMap2[(j-height)/2,i/2] = altitude;
                        }
                        else
                        {
                            tempHeightMap2[(j-height)/2,i/2] = sealevel- seaDepth;
                        }
                    }
                }
                else{
                    if (remainderI==0&&remainderJ==0)
                    {
                        tempHeightMap2[(j-height)/2,i/2] = sealevel;
                    }
                }
                if(i != 513 && j != 513)
                {
                    tempSplatMap2[(j-height)/2,i/2, 0] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 1] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 2] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 3] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 4] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 5] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 6] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 7] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 8] = 1;
                    tempSplatMap2[(j-height)/2,i/2, 9] = 1;

                    tempSplatMap2[(j-height)/2,i/2, 0] *= 1 - textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 1] *= 1 - textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 2] *= textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 3] *= textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 4] *= textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 5] *= textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 6] *= textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 7] *= textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 8] *= textureAltitude;
                    tempSplatMap2[(j-height)/2,i/2, 9] *= textureAltitude;

                    tempSplatMap2[(j-height)/2,i/2, 0] *= underWaterNoise;
                    tempSplatMap2[(j-height)/2,i/2, 2] *= textureNoise;
                    tempSplatMap2[(j-height)/2,i/2, 4] *= textureNoise;
                    tempSplatMap2[(j-height)/2,i/2, 6] *= textureNoise;
                    tempSplatMap2[(j-height)/2,i/2, 8] *= textureNoise;
                    tempSplatMap2[(j-height)/2,i/2, 1] *= 1 - underWaterNoise;
                    tempSplatMap2[(j-height)/2,i/2, 3] *= 1 - textureNoise;
                    tempSplatMap2[(j-height)/2,i/2, 5] *= 1 - textureNoise;
                    tempSplatMap2[(j-height)/2,i/2, 7] *= 1 - textureNoise;
                    tempSplatMap2[(j-height)/2,i/2, 9] *= 1 - textureNoise;

                    if(biome.temperature < 10)
                    {
                        tempSplatMap2[(j-height)/2,i/2, 2] *= 1 - snowNoise;
                        tempSplatMap2[(j-height)/2,i/2, 3] *= 1 - snowNoise;
                        tempSplatMap2[(j-height)/2,i/2, 4] *= snowNoise;
                        tempSplatMap2[(j-height)/2,i/2, 5] *= snowNoise;
                        tempSplatMap2[(j-height)/2,i/2, 6] *= snowNoise;
                        tempSplatMap2[(j-height)/2,i/2, 7] *= snowNoise;
                        tempSplatMap2[(j-height)/2,i/2, 8] *= snowNoise;
                        tempSplatMap2[(j-height)/2,i/2, 9] *= snowNoise;
                    }
                    else
                    {
                        tempSplatMap2[(j-height)/2,i/2, 2] *= 0;
                        tempSplatMap2[(j-height)/2,i/2, 3] *= 0;

                    }
                    if(biome.humidity < 150)
                    {
                        tempSplatMap2[(j-height)/2,i/2, 4] *= sandNoise;
                        tempSplatMap2[(j-height)/2,i/2, 5] *= sandNoise;
                        tempSplatMap2[(j-height)/2,i/2, 6] *= 1 - sandNoise;
                        tempSplatMap2[(j-height)/2,i/2, 7] *= 1 - sandNoise;
                        tempSplatMap2[(j-height)/2,i/2, 8] *= 0;
                        tempSplatMap2[(j-height)/2,i/2, 9] *= 0;
                    }
                    else if (biome.humidity > 200)
                    {
                        tempSplatMap2[(j-height)/2,i/2, 4] *= mudNoise;
                        tempSplatMap2[(j-height)/2,i/2, 5] *= mudNoise;
                        tempSplatMap2[(j-height)/2,i/2, 6] *= 0;
                        tempSplatMap2[(j-height)/2,i/2, 7] *= 0;
                        tempSplatMap2[(j-height)/2,i/2, 8] *= 1 - mudNoise;
                        tempSplatMap2[(j-height)/2,i/2, 9] *= 1 - mudNoise;
                    }
                    else
                    {
                        tempSplatMap2[(j-height)/2,i/2, 4] *= 1;
                        tempSplatMap2[(j-height)/2,i/2, 5] *= 1;
                        tempSplatMap2[(j-height)/2,i/2, 6] *= 0;
                        tempSplatMap2[(j-height)/2,i/2, 7] *= 0;
                        tempSplatMap2[(j-height)/2,i/2, 8] *= 0;
                        tempSplatMap2[(j-height)/2,i/2, 9] *= 0;

                    }

                }
            }
        }
     	terr.terrainData.SetHeights(0,0, tempHeightMap);
     	borderTerr.terrainData.SetHeights(0,0, tempHeightMap2);
     	terr.terrainData.SetAlphamaps(0,0, tempSplatMap);
     	borderTerr.terrainData.SetAlphamaps(0,0, tempSplatMap2);
        for (int i = 0; i< terrainWidth; i++)
        {
            for (int j = 0; j< terrainHeight; j++)
            {
                TerrainCell terrainCell  = new TerrainCell (i,j);
                terrainCells[i,j]	= terrainCell;
                if(i!=0){
                    terrainCell.left =terrainCells[i-1,j];
                    terrainCell.left.right = terrainCell;
                    if (j!=0){
                        terrainCell.upLeft =terrainCells[i-1,j-1];
                        terrainCell.upLeft.downRight = terrainCell;
                    }
                    if(j!=terrainHeight-1)
                    {
                        terrainCell.downLeft =terrainCells[i-1, j+1];
                        terrainCell.downLeft.upRight =terrainCell;
                    }
                }
                if (j!=0)
                {
                    terrainCell.up =terrainCells[i, j-1];
                    terrainCell.up.down = terrainCell;

                }

            }
        }
    }
    IEnumerator GenerateItems()
    {
        for (int i = 0; i< itemData.items.Count; i++)
        {
            itemHeteroffsets.Add (Random.Range (0f, 99999f));
        }
        for (int i = 0; i < width; i++)
        {
            if(i == width/3)
            {
                loadingImage.fillAmount = 0.25f;
                yield return new WaitForSeconds(0.1f);
            }
            else if ( i == 2*width/3)
            {
                loadingImage.fillAmount = 0.42f;
            }
            for (int j = 0; j< height; j++)
            {
                Cell cell = grid.cells[i,j];
                int abundancesCount = 0;
                float rand2 = 0;
                float abundanceCounter = 0;
                float relativeAltitude = cell.altitude - sealevel;
                float totalItemAbundances = 0;

                List <float> cellAbundances = new List <float>();

                for (int k = 0; k < biome.itemAbundances.Count; k++)
                {
                    Item item = itemData.items[k];

                    if(relativeAltitude > item.minAltitude && relativeAltitude < item.maxAltitude)
                    {
                        cellAbundances.Add(itemAbundance * biome.itemAbundances[k] * CalculateHeterogeneity(i, j, itemHeteroffsets[k]));
                        totalItemAbundances += itemAbundance * biome.itemAbundances[k];
                    }
                    else
                    {
                        cellAbundances.Add(0);
                    }
                }


                rand2 = Random.Range(0f, totalItemAbundances + 1);
                abundanceCounter = totalItemAbundances + 1;
                abundancesCount = cellAbundances.Count;


                for (int k = 0; k < abundancesCount; k ++)
                {
                    abundanceCounter -= cellAbundances[k];
                    if(abundanceCounter < rand2)
                    {
                        Item item = itemData.items[k];
                        float itemAltitude = 0;
                        if (item.floating)
                        {
                            float minAltitude = Mathf.Max(cell.altitude - sealevel, - item.maxDepth);
                            float maxAltitude = Mathf.Min(0, - item.minDepth);
                            itemAltitude =  Random.Range(minAltitude, maxAltitude);
                            itemAltitude = - (itemAltitude * itemAltitude)*terr.terrainData.size.y;
                        }
                        else
                        {
                            itemAltitude = grid.FloorAltitude(cell);
                        }
                        if(!grid.CheckIfBlocked(cell, item.width, item.height, itemAltitude))
                        {

                            GameObject itemGo =	Instantiate(item.gameObject, CellConversor.GetPosition(i,itemAltitude,j), Quaternion.identity);
                    		if(item.sizeChange)
                    		{
                    			itemGo.GetComponent<Item>().ChangeSize();
                    		}
                            if(item.type == "Plant")
                            {
                                itemGo.transform.SetParent(plants);
                            }
                            else if (item.type == "Decoration")
                            {
                                itemGo.transform.SetParent(decorations);
                            }
                            else if (item.type == "Food")
                            {
                                itemGo.transform.SetParent(foods);
                            }
                            if(!item.notRotated)
                            {
                                itemGo.transform.Rotate(45,0,0);
                            }
                            if(item.unmoving)
                            {
                                cell.item = itemGo;
                            }
                            k=abundancesCount;
                        }
                    }
                }
            }
        }
        finishedItems = true;
    }
    void GenerateAnimals()
    {

        if (animalTest)
        {
            Animal theOther = CreateAnimal(world.player, true);
            nMates ++;

            Transform playerTrans = Instantiate (player, theOther.transform.position, Quaternion.identity).transform;
            playerTrans.SetParent(animals);
            Animal animalPlayer = playerTrans.GetComponentInChildren<Animal>();
            AnimalBuilder builderPlayer = playerTrans.GetComponentInChildren<AnimalBuilder>();
            builderPlayer.species = world.player;
            animalPlayer.UpdateStats();
        }
        else
        {
            Transform playerTrans = Instantiate (player, SuitablePlayerLocation(world.player), Quaternion.identity).transform;
            playerTrans.SetParent(animals);
            Animal animalPlayer = playerTrans.GetComponentInChildren<Animal>();
            AnimalBuilder builderPlayer = playerTrans.GetComponentInChildren<AnimalBuilder>();
            builderPlayer.species = world.player;
            animalPlayer.UpdateStats();

            foreach (Niche niche in biome.niches)
            {
                if(niche.species != null)
                {
                    for (int i = 0; i < niche.abundance; i++)
                    {
                        if(niche.species == world.player)
                        {
                            CreateAnimal(niche.species, true);
                            nMates ++;
                        }
                        else
                        {
                            CreateAnimal(niche.species, false);
                        }
                    }
                }
            }

            while (nMates < minMates)
            {
                CreateAnimal(world.player, true);
                nMates ++;
            }
        }
    }
    public void CreateAnimalHidden(Species species, bool isMate)
    {
        CreateAnimal(species, isMate).builder.hidden = true;
    }
    Animal CreateAnimal (Species species, bool isMate)
    {
        Transform aniTrans = Instantiate (animal, SuitableLocation(species), Quaternion.identity).transform;
        aniTrans.SetParent(animals);
        Animal newAnimal = aniTrans.GetComponentInChildren<Animal>();
        AnimalBuilder builder = aniTrans.GetComponentInChildren<AnimalBuilder>();
        builder.species = species;
        newAnimal.UpdateStats();
        aniTrans.gameObject.name = builder.species.name[0];
        if(isMate)
        {
            newAnimal.minimapIcon.SetActive(false);
            newAnimal.minimapMate.SetActive(true);
            Mate mate = newAnimal.gameObject.AddComponent<Mate>();
            mate.part = world.player.SuggestMutation();
            mate.distanceIcon = builder.height*2f;
            aniTrans.GetComponentInChildren<Senses>().player = true;
    		if(mate.part.type == "head")
    		{
    			mate.bestImprovement = species.head.Compare(mate.part);
    		}
    		else if(mate.part.type == "body")
    		{
    			mate.bestImprovement = species.body.Compare(mate.part);
    		}
    		else if(mate.part.type == "legs")
    		{
    			mate.bestImprovement = species.legs.Compare(mate.part);
    		}
    		else if(mate.part.type == "tail")
    		{
    			mate.bestImprovement = species.tail.Compare(mate.part);
    		}

        }
        return newAnimal;

    }

    Vector3 SuitableLocation (Species species)
    {
        int x = Random.Range(0, grid.cells.GetLength(0));
        int z = Random.Range(0, grid.cells.GetLength(1));
        int counter = 0;
        while((!IsSuitable(grid.cells[x, z], species) || (world.player.IsMenace(species)
            && Vector3.Distance(CellConversor.GetPosition(x, z), initialPlayerPos) < initialDistancePredators))&& counter < 100)
        {
            x = Random.Range(0, grid.cells.GetLength(0));
            z = Random.Range(0, grid.cells.GetLength(1));
            counter ++;
        }
        return CellConversor.GetPosition(x, z);
    }

    Vector3 SuitablePlayerLocation (Species species)
    {
        int Lx = grid.cells.GetLength(0);
        int Lz = grid.cells.GetLength(1);
        int x = Random.Range(Lx/4, (Lx*3)/4);
        int z = Random.Range(Lz/4, (Lz*3)/4);
        int counter = 0;
        while(!IsSuitable(grid.cells[x, z], species) && counter < 100)
        {
            x = Random.Range(Lx/4, (Lx*3)/4);
            z = Random.Range(Lz/4, (Lz*3)/4);
            counter ++;
        }
        initialPlayerPos = CellConversor.GetPosition(x, z);
        return initialPlayerPos;
    }

    bool IsSuitable(Cell cell, Species species)
    {
        if(((cell.aquatic && species.aquaticSpeed >= species.terrestrialSpeed) || (!cell.aquatic && species.terrestrialSpeed >= species.aquaticSpeed)) && ! cell.blocked)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void InitializeLevel()
    {
        world = GameObject.Find("Data").GetComponent<World>();
        music = world.GetComponent<MusicControl>();
        biome = world.currentBiome;


        surface = GetComponent<NavMeshSurface>();
        offsetX = Random.Range (0f, 99999f);
        offsetZ = Random.Range (0f, 99999f);

        sealevel = biome.sealevel;
		if(biome.humidity >= 300 && biome.sealevel < 1)
		{
            scale *=2;
		}
        seaDepth = seaDepth*sealevel;

        terrainWidth = terrainCells.GetLength(0);
        terrainHeight = terrainCells.GetLength(1);
        terrainSize = width/terrainWidth;

        GameObject terrGO = Instantiate(terrainPrefab);
        terr = terrGO.GetComponent<Terrain>();
        terrGO.name = "Terrain";
        terr.terrainData.size = new Vector3(width*CellConversor.factor, depth*CellConversor.factor, height*CellConversor.factor);
        terrGO.transform.position = new Vector3 ( -terr.terrainData.size.x/2, -terr.terrainData.size.y*sealevel, -terr.terrainData.size.z/2);
        hmWidth = terr.terrainData.heightmapWidth;
        hmHeight = terr.terrainData.heightmapHeight;
        terr.terrainData.SetHeights(0, 0, heights);

        GameObject terrGO2 = Instantiate(terrainPrefab2);
        borderTerr = terrGO2.GetComponent<Terrain>();
        terrGO2.name = "TerrainBorder";
        borderTerr.terrainData.size = new Vector3(width*CellConversor.factor, depth*CellConversor.factor, height*CellConversor.factor);
        terrGO2.transform.position = new Vector3 ( -borderTerr.terrainData.size.x/2, -borderTerr.terrainData.size.y*sealevel, borderTerr.terrainData.size.z/2 - 0.2f);

        itemData = GameObject.Find("Data").GetComponent<ItemData>();
        plants = GameObject.Find("Plants").transform;
        decorations = GameObject.Find("Decorations").transform;
        foods = GameObject.Find("Foods").transform;
        animals = GameObject.Find("Animals").transform;
        rockGenerator = GetComponent<RockGenerator>();

        grid.depth = depth;
        grid.sizeY = terr.terrainData.size.y;
        grid.sealevel = sealevel;
        grid.seaDepth = seaDepth;

    }
    IEnumerator GenerateLevel()
    {
        InitializeLevel();
        yield return new WaitForSeconds(0.01f);
        loadingImage.fillAmount = 0.1f;
        yield return new WaitForSeconds(0.01f);
        GenerateCells();
        yield return new WaitForSeconds(0.01f);
        rockGenerator.GenerateRocks();
        if(!terrainTest)
        {
            StartCoroutine("GenerateItems");

        }
        while(!finishedItems)
        {
            yield return new WaitForSeconds(0.05f);
        }
        loadingImage.fillAmount = 0.675f;
        yield return new WaitForSeconds(0.01f);
        surface.BuildNavMesh();
        loadingImage.fillAmount = 1f;
        yield return new WaitForSeconds(0.01f);
        GenerateAnimals();
        finishedLoading = true;
        animator.SetTrigger("loaded");
        StartCoroutine(music.FadeOut(3));
        StartCoroutine(music.FadeOut(music.secondarySource, 3));
        float randomTime = Random.Range (4f, 120f);
        Invoke ("ChangeMusic", randomTime);
        music.mainSource.loop = false;
    }

    public void LoadEvolution()
    {
        animator.SetTrigger("exit");
        StartCoroutine(music.CrossFade(music.mating, 2, 0.8f));
		StartCoroutine(music.FadeOut(music.conflictSource, 2));
        music.mainSource.loop = true;
        Invoke("GoToEvolution", colorTransition);

    }

    void GoToEvolution()
    {
        Misc.LoadScene("Evolution");
    }

    void ChangeMusic()
    {
        if(music.secondarySource.volume <= 0 || !music.secondarySource.isPlaying)
        {
            music.maxVolume = 0.4f;
            if(biome.type[0] == "sea")
            {
                StartCoroutine(music.FadeIn(music.mainSource, music.sea, 12, true));
            }
            else
            {
                int rand = Random.Range(0, 2);
                if (biome.sealevel < 1 && rand == 0)
                {
                    StartCoroutine(music.FadeIn(music.mainSource, music.sea, 12, true));
                }
                else if (biome.type[0] == "tundra" || biome.type[0] == "desert")
                {
                    StartCoroutine(music.FadeIn(music.mainSource, music.arid, 12, true));
                }
                else if(biome.type[0] == "grassland")
                {
                    StartCoroutine(music.FadeIn(music.mainSource, music.grass, 12, true));
                }
                else if (biome.type[0] == "forest")
                {
                    StartCoroutine(music.FadeIn(music.mainSource, music.forest, 12, true));
                }
                else
                {
                    StartCoroutine(music.FadeIn(music.mainSource, music.sea, 12, true));
                }
            }

        }
        Invoke ("ChangeMusic", Random.Range (240, 480));
    }
}
