﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

	public bool blockAtStart = true, blocked;
	public int width, height, altitude;
	public Cell[,] cells;
	public Grid grid;
	public GameObject sign;

	void Update()
	{
		if(!blocked && blockAtStart)
		{
			BlockGrid();
			blockAtStart = false;
		}

	}

	public void BlockGrid()
	{
		if(!blocked)
		{
			if(grid==null)
			{
				grid = GameObject.Find("Controller").GetComponent<Grid>();
				cells = grid.cells;
			}
			Cell cell = CellConversor.GetCell(transform.position, cells);
			for(int i = cell.x- width/2; i< cell.x + width/2+1; i++)
			{
				for	(int j = cell.y; j< cell.y + height; j++)
				{
					if(i<cells.GetLength(0)&&j<cells.GetLength(1)&&i>0&&j>0)
					{
						cells[i,j].blocked = true;
						cells[i,j].blockedHeight = altitude;
						/*
						if(sign != null)
						{
							Instantiate (sign, CellConversor.PositionFromCell(cells[i,j]), Quaternion.identity);
						}
						*/
					}
				}
			}
			blocked = true;
		}
	}

	public void UnblockGrid()
	{
		Cell cell = CellConversor.GetCell(transform.position, cells);
		for(int i = cell.x- width/2; i< cell.x + width/2; i++)
		{
			for	(int j = cell.y- height/2; j< cell.y + height/2; j++)
			{
				cells[i,j].blocked = false;
			}
		}
	}

}
