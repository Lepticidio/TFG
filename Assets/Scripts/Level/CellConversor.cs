﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CellConversor
{

	public static float  factor = 0.1f, xOffset = 51.2f, zOffset = 51.2f;
	//public static float  factor = 1, xOffset = 8, zOffset = 5;
	public static Vector3 GetPosition(Cell cell){

		float xx = cell.x*factor -xOffset;
		float zz = cell.y*factor- zOffset;

		return new Vector3 (xx,0,zz);

	}

	public static Vector3 GetPosition(int x, int z){

		float xx = x*factor -xOffset;
		float zz = z*factor- zOffset;

		return new Vector3 (xx,0,zz);

	}
	public static Vector3 GetPosition(int x, float y, int z){

		float xx = x*factor -xOffset;
		float zz = z*factor- zOffset;

		return new Vector3 (xx,y,zz);

	}
	public static Cell GetCell(Vector3 position, Cell[,] cells){
		int x = (int)((position.x + xOffset)/factor);
		int z = (int)((position.z + zOffset)/factor);
		return cells[x,z];
	}
	public static Cell GetCell(float xx, float yy, Cell[,] cells){
		int x = (int)((xx + xOffset)/factor);
		int z = (int)((yy + zOffset)/factor);
		return cells[x,z];
	}

}
