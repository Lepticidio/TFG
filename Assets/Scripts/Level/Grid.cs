﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {
	public Cell[,] cells = new Cell[1024,1024];
	public float  limitX, limitZ, sizeY, sealevel, seaDepth, depth;
	public GameObject indicador, indi2, indi3;
	void Awake(){

		Vector3 limits = CellConversor.GetPosition(cells.GetLength(0)-1, cells.GetLength(1)-1);
		limitX = limits.x;
		limitZ = limits.z;
		for (int i = 0; i< cells.GetLength(0);i++){
			for (int j = 0; j< cells.GetLength(1); j++){
				Cell cell  = new Cell (i,j);
				if(i!=0){
					cell.neighbours[6] =cells[i-1,j];
					cell.neighbours[6].neighbours[2] = cell;
					if (j!=0){
						cell.neighbours[7] =cells[i-1,j-1];
						cell.neighbours[7].neighbours[3] = cell;
					}
					if(j!=cells.GetLength(1)-1){
						cell.neighbours[5] =cells[i-1, j+1];
						cell.neighbours[5].neighbours[1] =cell;
					}
				}
				if (j!=0){
					cell.neighbours[0] =cells[i, j-1];
					cell.neighbours[0].neighbours[4] = cell;

				}
				cells[i,j]	= cell;
			}
		}

		Instantiate (indicador, CellConversor.GetPosition(0,0), Quaternion.identity);
	}
	public bool CheckIfBlocked(Cell cell, int width, int height)
	{

		for(int i = cell.x- width/2; i< cell.x + width/2; i++)
		{
			for	(int j = cell.y; j< cell.y + height; j++)
			{
				if(cells[i,j].blocked)
				{
					return true;

				}
			}
		}
		return false;
	}
	public bool AboveFloor (Cell cell, float altitude)
	{
		return (cell.altitude-sealevel)*sizeY + cell.blockedHeight/10 < altitude ;
	}
	public bool CheckIfBlocked(Cell cell, int width, int height, float altitude)
	{
		if(cell.x - width/2 < 0 || cell.x + width/2 >= cells.GetLength(0)|| cell.y < 0 || cell.y + height >= cells.GetLength(1))
		{
			return true;
		}
		for(int i = cell.x- width/2; i< cell.x + width/2; i++)
		{
			for	(int j = cell.y; j< cell.y + height; j++)
			{
				if(cells[i,j].blocked)
				{
					if(cells[i,j].altitude < sealevel - seaDepth&& -seaDepth*sizeY + cells[i,j].blockedHeight/10 > altitude)
					{
						return true;
					}
					else if (cells[i,j].altitude >= sealevel - seaDepth && (cells[i,j].altitude-sealevel)*sizeY + cells[i,j].blockedHeight/10 > altitude)
					{
						return true;
					}

				}
			}
		}
		return false;
	}
    public float FloorAltitude(Cell cell)
    {
        if (cell.altitude > sealevel)
        {
            return 0;
        }
        else if (cell.altitude > sealevel - seaDepth)
        {
            return (cell.altitude-sealevel)*depth * CellConversor.factor;
        }
        else
        {
            return -seaDepth*depth*CellConversor.factor;
        }
    }
    public float FloorAltitude(int i, int j)
    {
        return FloorAltitude(cells[i, j]);

    }
    public float FloorAltitude(Transform trans)
    {
        return FloorAltitude(CellConversor.GetCell(trans.position, cells));

    }
    public float FloorDistance (Transform trans)
    {
        return trans.position.y - FloorAltitude(trans);
    }

}
