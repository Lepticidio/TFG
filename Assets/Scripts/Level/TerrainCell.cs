﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainCell {

	public bool aquatic;
	public int x, y;
	public enum Location {central, up, down, right, left, upRight,upLeft, downRight, downLeft};
	public Location location;
	public GameObject terrain;
	public Cell[,] cells = new Cell[64,64];

	public TerrainCell up, down, right, left, upRight, upLeft, downRight, downLeft;

	public TerrainCell (int xx, int yy){
		x = xx;
		y = yy;
		location = Location.central;
	}





}
