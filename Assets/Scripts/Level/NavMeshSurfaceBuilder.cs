﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshSurfaceBuilder : MonoBehaviour
{
	public NavMeshSurface surface;
	// Use this for initialization
	void Start ()
	{
		surface.BuildNavMesh();
	}
}
