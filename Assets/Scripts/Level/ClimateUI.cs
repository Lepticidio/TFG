﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClimateUI : MonoBehaviour {

	public GameObject panel;
	public ClimateData data;
	public DevAnimalGenerator devGen;
	public Text temperature, humidity, sealevel, nFlee, nFight, nSeek;

	void Awake()
	{
		data = GameObject.Find("Data").GetComponent<ClimateData>();
		devGen = GameObject.Find("Data").GetComponent<DevAnimalGenerator>();
	}

	public void Set()
	{
		if(temperature.text == "")
		{
			data.temperature = 30;
		}
		else
		{
			data.temperature = float.Parse(temperature.text);
		}
		if(humidity.text == "")
		{
			data.humidity = 150;
		}
		else
		{
			data.humidity = float.Parse(humidity.text);
		}
		if(sealevel.text == "")
		{
			data.sealevel = 0.5f;
		}
		else
		{
			data.sealevel = float.Parse(sealevel.text);
		}

		if(nFlee.text == "")
		{
			devGen.nFlee = 0;
		}
		else
		{
			devGen.nFlee = int.Parse(nFlee.text);

		}
		if(nFight.text == "")
		{
			devGen.nFight = 0;
		}
		else
		{
			devGen.nFight = int.Parse(nFight.text);
		}
		if(nSeek.text == "")
		{
			devGen.nSeek = 0;
		}
		else
		{
			devGen.nSeek = int.Parse(nSeek.text);
		}
		devGen.CreateAnimals();
	}

	void Update()
	{
		if(Input.GetKeyDown("k"))
		{
			if(panel.activeSelf)
			{
				panel.SetActive(false);
			}
			else
			{
				panel.SetActive(true);
			}
		}
	}




}
