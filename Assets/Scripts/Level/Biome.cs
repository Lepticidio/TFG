﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Biome
{
	public bool forested, grassy;
	public float temperature, humidity, sealevel, baseSealevel;
	public World world;
	public string[] name = new string[2];
	public List<float> itemAbundances = new List<float>();
    public List<Niche> niches = new List<Niche>();
	public List<Biome> connectedBiomes = new List<Biome>();
	public Sprite sprite, button;
	public string[] type = new string[2];
	public List<string> floraStrings = new List<string>();
//
	public Biome (float temp, float hum, float sea, World w)
	{
		temperature = temp;
		humidity = hum;
		baseSealevel = sea;
		world = w;
		connectedBiomes.Add(this);
		UpdateBiome();

	}

	public void UpdateBiome()
	{
		floraStrings.Clear();
		niches.Clear();
		itemAbundances.Clear();
		sealevel = baseSealevel;
		/*
		if(humidity >= 300 && sealevel < 1)
		{
			sealevel += (humidity - 300)/400;
		}
		*/
		forested = false;
		grassy = false;
        for (int i = 0; i< world.itemData.items.Count; i++)
        {
            Item item = world.itemData.items[i];

            float abundance = 0;
			if(!(sealevel == 1 && item.minAltitude > 0 ) && ! (sealevel == 0 && item.maxAltitude < 0 ))
			{
	            if(world.millionYears >= item.millionYears && temperature> item.minTemperature&& temperature< item.maxTemperature)
	            {
	                float difT = 0;
	                float relT = 0;
	                float abundanceT = 0;

	                float difHum = 0;
	                float relHum = 0;
	                float abundanceHum = 0;
	                if(temperature < item.minPerfectTemperature)
	                {
	                    float temp = temperature/item.minPerfectTemperature;
	                    abundanceT = temp*temp;
	                }
	                else if(temperature > item.maxPerfectTemperature)
	                {
	                    float temp = item.maxPerfectTemperature/temperature;
	                    abundanceT = temp*temp;

	                }
	                else{
	                    abundanceT = 1;
	                }

	                if (item.maxAltitude > 0 && humidity> item.minHumidity&& humidity< item.maxHumidity)
	                {

	                    if(humidity<item.minPerfectHumidity)
	                    {
	                        float temp = humidity/item.minPerfectHumidity;
	                        abundanceHum = temp*temp;
	                    }
	                    else if(humidity>item.maxPerfectHumidity)
	                    {
	                        float temp = item.maxPerfectHumidity/humidity;
	                        abundanceHum = temp*temp;
	                    }
	                    else
	                    {
	                        abundanceHum = 1f;
	                    }

	                }
	                else if(item.maxAltitude < 0)
	                {
	                    abundanceHum = 1;
	                }
	                else
	                {
	                    abundance = 0;
	                }
	                abundance = abundanceT*abundanceHum*item.abundance;
	            }
			}

            itemAbundances.Add( abundance);
            if(abundance > 0 && item.nutrition > 0)
            {
				floraStrings.Add(item.name[0]);
				Niche nich = null;
				if(Random.Range(0, 2) == 1)
				{
                	nich = new Niche (this, item, true, abundance);
				}
				else
				{
                	nich = new Niche (this, item, false, abundance);
				}
                niches.Add(nich);
                niches.Add(new Niche (this, nich));
				if(item.tree)
				{
					forested = true;
				}
				else if(item.grassy)
				{
					grassy = true;
				}
            }

        }
		UpdateType();
		name = BiomeName();
	}
	public void UpdateType()
	{
		if(forested)
		{
			type[0] = "forest";
			type[1] = "Bosque";
		}
		else if (grassy)
		{
			type[0] = "grassland";
			type[1] = "Pradera";
		}
		else
		{
			if(sealevel < 1)
			{
				if(temperature < 5)
				{
					type[0] = "tundra";
					type[1] = "Tundra";
				}
				else
				{
					type[0] = "desert";
					type[1] = "Desierto";
				}
			}
			else
			{
				type[0] = "sea";
				type[1] = "Mar";
			}
		}
		string coast = "";
		if(sealevel < 1f && sealevel > 0.25f)
		{
			coast = "Coast";
		}
		sprite = Resources.Load<Sprite>("Sprites/Interface/Biomes/"+type[0] + coast);
		button = Resources.Load<Sprite>("Sprites/Interface/Biomes/"+type[0] + coast + "Button");

	}

	public string[] BiomeName()
	{
		string[] result = new string[2];
		result[0] = "";
		result[1] = "";

		string spanishEnd ="";
		string spanishMiddle = "";

		if (humidity < 100 && sealevel < 1)
		{
			result[0] += "dry ";
			spanishEnd += " y seca";
		}
		else if (humidity >250 && sealevel < 1)
		{
			result[0] += "wet ";
			spanishEnd += " y humeda";
		}
		if(temperature < 5)
		{
			result[0] += "polar ";
			spanishMiddle += " polar";
		}
		else if (temperature < 15)
		{
			result[0] += "cold ";
			spanishMiddle += " fría";
		}
		else if (temperature < 25)
		{
			result[0] += "warm ";
			spanishMiddle += " templada";

		}
		else if (temperature < 35)
		{
			result[0] += "tropical ";
			spanishMiddle += " tropical";
		}
		else
		{
			result[0] += "torrid ";
			spanishMiddle += " tórrida";
		}
		result[0] += type[0];
		result[1] += type[1];

		result[1] += spanishMiddle;
		result[1] += spanishEnd;
		return result;
	}
}
