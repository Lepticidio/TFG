﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class World : MonoBehaviour
{
	public int mutationInt;
	public float plantHue, yearsPerGeneration, millionYears, minTemperature, maxTemperature, minHumidity, maxHumidity;
	public Part previousChange;
	public Species player, futurePlayer;
	public Biome currentBiome;
	public Biome[] biomes = new Biome[5];
	public ItemData itemData;
	public PartData partData;
	public Saver saver;
	public string[] mutationTypes = new string[4];
	public int[] mutationIndexes = new int[4];
	public Color[] mutationColors = new Color[6];
	public Biome[] biomesSelected = new Biome[3];

	public List<List<Species>> generations = new List<List<Species>>();
	public List<Species> allSpecies = new List<Species>();


	public void NewBiomes()
	{

		for (int i = 0; i< biomes.Length; i++)
		{

			biomes[i].temperature = Random.Range(minTemperature, maxTemperature);
			biomes[i].humidity = Random.Range(minHumidity, maxHumidity);
			biomes[i].UpdateBiome();
			biomes[i].niches.OrderByDescending(n => (n.abundance)).ToList();
			biomes[i].niches.OrderByDescending(n => (n.herbivore)).ToList();
		}

	}

	public void NewGeneration()
	{
		millionYears += yearsPerGeneration;

		List<Species> tempGen = new List<Species>();
		List<Species> newGen = generations[generations.Count - 1];

		foreach (Species sp in generations[generations.Count - 2])
		{
			tempGen.Add(sp);
			tempGen.Add(sp.Evolve());
			tempGen.Add(sp.Evolve());
			tempGen.Add(sp.Evolve());
		}

		for (int i = 0; i< biomes.Length; i++)
		{
			List<Species> usedSpecies = new List<Species>();
			for (int j = 0; j < biomes[i].niches.Count; j++)
			{
				for (int k = 0; k < tempGen.Count; k++)
				{
					if(!usedSpecies.Contains(tempGen[k]))
					{
						biomes[i].niches[j].Compete(tempGen[k]);
					}
				}
				Species species = biomes[i].niches[j].species;
				if(species != null && !newGen.Contains (species))
				{
					newGen.Add(species);
					if(!allSpecies.Contains(species))
					{
						species.index = allSpecies.Count;
						allSpecies.Add(species);
					}
					usedSpecies.Add(species);
				}
			}
		}
		player = futurePlayer;
		futurePlayer = new Species(player);
		futurePlayer.index = allSpecies.Count;
		allSpecies.Add(futurePlayer);
		generations.Add(new List <Species>());
		generations[generations.Count -1].Add(futurePlayer);

	}

	public void ChangePlayerPart(Part part)
	{
		if(part.type == "head")
		{
			mutationInt = 0;
			previousChange = part;
		}
		else if (part.type == "body")
		{
			mutationInt = 1;
			previousChange = part;
		}
		else if (part.type == "leg")
		{
			mutationInt = 2;
			previousChange = part;
		}
		else if (part.type == "tail")
		{
			mutationInt = 3;
			previousChange = part;
		}
		futurePlayer.ChangePart(part);

	}
	public void AddPlayerMutation()
	{
		futurePlayer.newEvolutions.Add(mutationInt);
	}
	public void Initialize()
	{
		millionYears = 0;
		biomes[0] = new Biome(30, 300, 1, this);
		biomes[1] = new Biome(30, 300, 0.5f, this);
		biomes[2] = new Biome(30, 300, 0.5f, this);
		biomes[3] = new Biome(30, 300, 0, this);
		biomes[4] = new Biome(30, 300, 0, this);

		generations.Clear();
		allSpecies.Clear();

		StandardConnections();

		List <Species> firstGen = new List<Species>();
		firstGen.Add(new Species(0, 0, 0, 0, new Color(0.7f, 0.21f, 0.33f, 1f), new Color(0.1f, 0.1f, 0.1f, 1f), new Color (1f, 0.55f, 0.1f, 1f)));
		player = firstGen[0];
		generations.Add(firstGen);
		allSpecies.Add(player);
		currentBiome = biomes[0];
		for (int j = 0; j < biomes[0].niches.Count; j++)
		{
			currentBiome.niches[j].Compete(player);

		}
		generations.Add(new List <Species>());
		futurePlayer = new Species(player);
		futurePlayer.index = 1;
		allSpecies.Add(futurePlayer);
		generations[generations.Count -1].Add(futurePlayer);
	}

	public void StandardConnections()
	{
		biomes[0].connectedBiomes.Add(biomes[1]);
		biomes[0].connectedBiomes.Add(biomes[2]);
		biomes[1].connectedBiomes.Add(biomes[0]);
		biomes[1].connectedBiomes.Add(biomes[3]);
		biomes[2].connectedBiomes.Add(biomes[0]);
		biomes[2].connectedBiomes.Add(biomes[4]);
		biomes[3].connectedBiomes.Add(biomes[1]);
		biomes[3].connectedBiomes.Add(biomes[4]);
		biomes[4].connectedBiomes.Add(biomes[2]);
		biomes[4].connectedBiomes.Add(biomes[3]);
	}

	public void Save (int state)
	{
		saver.Save(this, state);
	}

}
