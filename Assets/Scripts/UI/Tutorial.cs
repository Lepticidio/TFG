﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tutorial : MonoBehaviour
{
	public bool moved, ascended, descended, eaten, attacked;
	TextMeshProUGUI text;
	List<string[]> tutorialText = new List<string[]>();
	public int phase = 0;
	TextProcessor processor;
	Animator animator;

	// Use this for initialization
	public void Start ()
	{
		text = GetComponentInChildren<TextMeshProUGUI>();
		processor = GetComponent<TextProcessor>();
		animator = GetComponent<Animator>();
		text.text = "";

		tutorialText.Add( new string[] {
			"\n\nMove with the arrow keys or asdw\n"+
			"(f to skip the tutorial)" ,
			"\n\nMuévete con las flechas del\n"+
			"teclado o con asdw\n"+
			"(f para saltar el tutorial)" });

		tutorialText.Add( new string[] {
			"\n\nTo go down, use alt or ctrl\n"+
			"To go up, use space\n"+
			"(f to skip the tutorial)" ,
			"\n\nPara bajar, utiliza alt o ctrl.\n"+
			"Para ir arriba, utiliza espacio\n"+
			"(f para saltar el tutorial)" });

		tutorialText.Add( new string[] {
			"\n\nYou can eat placing your mouth\n"+
			"in a plankton cloud and holding\n"+
			"the left mouse button (f to skip)" ,
			"\n\nPuedes comer poniendo tu boca\n"+
			"en una nube de plancton y manteniendo\n"+
			"el botón izquierdo del ratón (f para saltar)" });


		tutorialText.Add( new string[] {
			"\n\nTo reproduce, hold the left button next\n"+
			"to a member of your species. They will\n"+
			"give you different mutations (f to skip)" ,
			"\n\nPara reproducirte, mantén el botón\n"+
			"izquierdo cerca de un miembro de tu\n"+
			"especie (f para saltar el tutorial)" });


		tutorialText.Add( new string[] {
			"\n\nTo attack, hold the right button\n"+
			"next to a creature\n"+
			"(f to skip the tutorial)" ,
			"\n\nPara atacar, mantén el botón derecho\n"+
			"izquierdo cerca de otra criatura\n"+
			"(f para saltar el tutorial)" });

		Invoke("StartWriting", 1.5f);
	}
	void StartWriting()
	{
		processor.WriteText(tutorialText[phase][0], text);
	}
	void Update ()
	{
		if(Input.GetKey("f"))
		{
			animator.SetTrigger("out");
		}
		if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
		{
			moved = true;
		}
		if(Input.GetAxis("Up Down") < 0)
		{
			descended = true;
		}
		if(Input.GetAxis("Up Down") > 0)
		{
			ascended = true;
		}
		if(moved && phase == 0)
		{
			phase ++;
			processor.WriteText(tutorialText[phase][0], text);
		}
		if(descended && ascended && phase == 1)
		{
			phase ++;
			processor.WriteText(tutorialText[phase][0], text);
		}
		if(eaten && phase == 2)
		{
			phase ++;
			processor.WriteText(tutorialText[phase][0], text);
		}
		if(eaten && phase == 2)
		{
			phase ++;
			processor.WriteText(tutorialText[phase][0], text);
		}
		if(Input.GetAxis("Skill") > 0 && phase == 4)
		{
			animator.SetTrigger("out");
		}

	}
}
