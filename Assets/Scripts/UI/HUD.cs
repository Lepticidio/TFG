﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUD : MonoBehaviour
{
	public Level level;
	Animal player;
	PlayerControls controls;
	public Image matingClock, hunger, health, hudImage;
	public Animator matingAnimator, healthAnimator, hungerAnimator;
	public Button[] skillButtons = new Button[3];
	public GameObject[] skillFrame = new GameObject[3];
	public Sprite transparent;
	public TextMeshProUGUI timeSurvived, pauseTimeSurvived;


	// Update is called once per frame
	void Update ()
	{
		if(player == null)
		{
			UpdateHud();
		}
		else
		{
			if(player.species.attacks.Count > 0)
			{
				for(int i = 0; i < skillFrame.Length; i++)
				{
					skillFrame[i].SetActive(i == controls.currentSkill);
					if(i == controls.currentSkill)
					{
						skillButtons[i].image.color = new Color (1, 1, 1, 1);
					}
					else
					{
						skillButtons[i].image.color = new Color (1, 1, 1, 0.5f);
					}
				}
			}
			hunger.fillAmount = player.hunger / player.species.maxHunger;
			if(hunger.fillAmount < 0.25f)
			{
				hungerAnimator.SetBool("critical", true);
			}
			else
			{
				hungerAnimator.SetBool("critical", false);
			}
			health.fillAmount = player.life / player.species.maxLife;
			if(health.fillAmount < 0.25f)
			{
				healthAnimator.SetBool("critical", false);
			}
			else
			{
				healthAnimator.SetBool("critical", false);
			}
			matingClock.fillAmount = level.time / level.matingTime;
			if(matingClock.fillAmount >= 1)
			{
				matingAnimator.SetBool("critical", false);
			}
		}
	}
	public void UpdateHud()
	{
		timeSurvived.text = "Time survived: " + level.world.millionYears.ToString()+ " million years";
		pauseTimeSurvived.text = "You have survived " + level.world.millionYears.ToString()+ " million years";
		GameObject po = GameObject.Find("Player");
		if(po != null)
		{
			player = GameObject.Find("Player").GetComponent<Animal>();
			controls = player.GetComponent<PlayerControls>();
			for(int i = 0; i < skillButtons.Length; i++)
			{
				if(i < player.species.attacks.Count)
				{
					skillButtons[i].image.sprite = player.species.attacks[i].sprite;
				}
				else
				{
					skillButtons[i].image.sprite = transparent;
				}
			}
			if( player.species.attacks.Count > 0)
			{
				hudImage.gameObject.SetActive(true);
				hudImage.rectTransform.anchorMin = new Vector2(1f - ((float)player.species.attacks.Count / (float)skillButtons.Length) - 0.055f, -1);
				hudImage.rectTransform.offsetMin = new Vector2(0, 0);
			}
			else
			{
				hudImage.gameObject.SetActive(false);
			}
		}
	}
	public void GoToMenu()
	{
		level.animator.SetTrigger("pauseToMenu");
		StartCoroutine(level.music.FadeOut(level.music.secondarySource, 0.5f));
		Invoke("LoadMenu", 0.5f);

	}
	public void LoadMenu()
	{
		Misc.LoadScene("Menu");
	}
	public void PlayAgain()
	{
		level.loadingImage.fillAmount = 0;
		level.animator.SetTrigger("playAgain");
		level.world.Invoke("Initialize", 1.4f);
		Invoke("RestartLevel", 1.5f);

	}
	public void RestartLevel()
	{
		Misc.LoadScene("Level");
	}
}
