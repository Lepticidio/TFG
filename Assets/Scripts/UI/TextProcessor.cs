﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextProcessor: MonoBehaviour
{

	public bool writing;
	public int CurrentChar;
	public float characterTime;
	public string CurrentText ="", wholeText ="";
	public float totalTextTime = 2;

	public void WriteText(string text, Text dialogueBox, Color color)
	{
		Clear ();
		wholeText = text;
		writing = true;
		dialogueBox.color = color;
		StartCoroutine (WriteNext(dialogueBox));
	}

	public void WriteText(string text, TextMeshProUGUI dialogueBox)
	{
		Debug.Log("escribe 2");
		Clear ();
		wholeText = text;
		writing = true;
		StartCoroutine (WriteNext(dialogueBox));
	}

	public void Write(string text, TextMeshProUGUI dialogueBox)
	{
		StartCoroutine (WriteCoroutine(text, dialogueBox));
	}

	public void WriteText(string text, Text dialogueBox)
	{
		Debug.Log("escribe");
		Clear ();
		wholeText = text;
		writing = true;
		StartCoroutine (WriteNext(dialogueBox));
	}

	IEnumerator WriteCoroutine(string text, TextMeshProUGUI dialogueBox)
	{
		float localCharacterTime = (totalTextTime / 2)/dialogueBox.text.Length;
		int currentChar = 0;
		float time = 0;
		string currentText = "";
		while (dialogueBox.text.Length > 0)
		{
			time += Time.deltaTime;
			if(time > localCharacterTime/4)
			{
				dialogueBox.text = dialogueBox.text.TrimEnd(dialogueBox.text[dialogueBox.text.Length - 1]);
				time = 0;
			}
			yield return null;

		}
		localCharacterTime = totalTextTime/text.Length;
		while(currentChar < text.Length)
		{
			time += Time.deltaTime;
			if(time > localCharacterTime)
			{
				currentText += text[currentChar];
				currentChar ++;
				dialogueBox.text = currentText;
				time = 0;
			}
			yield return null;
		}

	}
	IEnumerator WriteNext(Text dialogueBox)
	{
		dialogueBox.supportRichText = true;
		yield return new WaitForSeconds(characterTime);
		if (writing)
		{
			if (CurrentChar < wholeText.Length)
			{
				CurrentText += wholeText [CurrentChar];
				CurrentChar++;
				dialogueBox.text = CurrentText;
				StartCoroutine (WriteNext(dialogueBox));
			}
			else
			{
				writing = false;
			}
		}
	}

	IEnumerator WriteNext(TextMeshProUGUI dialogueBox)
	{
		yield return new WaitForSeconds(characterTime);
		if (writing)
		{
			if (CurrentChar < wholeText.Length)
			{
				CurrentText += wholeText [CurrentChar];
				CurrentChar++;
				dialogueBox.text = CurrentText;
				StartCoroutine (WriteNext(dialogueBox));
			}
			else
			{
				writing = false;
			}
		}
	}
	public void Clear()
	{
		CurrentChar = 0;
		CurrentText = "";
		wholeText = "";
	}
}
