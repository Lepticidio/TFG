﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
	public GameObject pausePanel;
	public Animator animator;

	public void StartPause()
	{
		animator.SetTrigger("pause");
		Invoke("PauseGame", 0.5f);
	}
	public void PauseGame()
	{
		Time.timeScale = 0;
	}
	public void ResumeGame()
	{
		Time.timeScale = 1;
		animator.SetTrigger("resume");
	}
	public void Restart()
	{
		Misc.LoadScene("Level");
	}
	public void StartRestart()
	{
		Time.timeScale = 1;
		animator.SetTrigger("restart");
		Invoke("Restart", 2);
	}
	public void StartGoToMenu()
	{
		Time.timeScale = 1;
		animator.SetTrigger("pauseToMenu");
		Invoke("GoToMenu", 1);

	}
	public void GoToMenu()
	{
		Misc.LoadScene("Menu");
	}

	void Update()
	{
		if(Input.GetKeyDown("escape"))
		{
			StartPause();
		}
	}

}
