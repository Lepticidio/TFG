﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class EvolutionUI : MonoBehaviour
{
	public bool mutationChosen, biomeChosen;
	public float shineUpTransition, shineDownTransition, speedFactor = 500, healthFactor = 10, alphaPreview;
	public GameObject panelMutation, panelEnd, partsPanel, colorPanel;
	public Item planktonItem, meatItem, seaweedItem, lycopodiumItem, fernItem, cycadItem, grassItem;
	public Image[] partButtons = new Image[4];
	public Image[] colorButtons = new Image[6];
	public Button[] biomeButtons = new Button[3];
	public TextMeshProUGUI years, health, defense, terrestrialSpeed, aquaticSpeed, newHealth, newDefense, newTerrestrialSpeed,
	 newAquaticSpeed, maxAttack, newMaxAttack, temperature, humidity, sealevel, dietTooltip, floraTooltip, speciesText;
	public World world;
	public AnimalBuilder builder, whiteBuilder, previewBuilder;
	public Image healthArrow, defenseArrow, terrestrialSpeedArrow, aquaticSpeedArrow, maxAttackArrow,
		temperatureImage, humidityImage, sealevelImage;
	public Sprite upArrow, downArrow, transparent, dry, average, humid, veryHumid, freezing, cold, temperate, hot, scorching,
		landSprite, coastSprite, seaSprite, plankton, meat, herbivore1, seaweed, lycopodium, fern, cycad, pine, grass,
		planktonWhite, meatWhite, seaweedWhite, lycopodiumWhite, fernWhite, cycadWhite, grassWhite, frameIcon;
	public Image[] attackImages, newAttackImages, dietImages, whiteDietImages, floraImages, whiteFloraImages, framesDiet;
	public TextMeshProUGUI[] attackText , newAttackText, attackTooltip, newAttackTooltip;
	public List<Item> foods = new List<Item>();
	public TextMeshPro evolveText, continueText;
	public Animator animator, chooseColor, chooseMut, chooseBiome;
	PostProcessingControl fxControl;
	Biome tempBiome;
	public Image fadeImage;
	int selectedPartIndex, selectedMainColorIndex, selectedSecondaryColorIndex, selectedEyeColorIndex;
	public MusicControl music;

	void Start()
	{
		int state = PlayerPrefs.GetInt("state");
		int mutationsSelected = PlayerPrefs.GetInt("mutationsSelected");
		fxControl = GetComponent<PostProcessingControl>();
		whiteBuilder.colorTransition = shineUpTransition;
		world = GameObject.Find("Data").GetComponent<World>();
		music = world.GetComponent<MusicControl>();

		builder.species = world.player;
		if(state == 2)
		{
			builder.preview = false;
		}
		builder.UpdateAnimal();
		previewBuilder.species = world.futurePlayer;
		previewBuilder.UpdateAnimal();
		builder.transform.parent.localScale *= 3;
		tempBiome = world.currentBiome;
		world.previousChange = null;
		years.text = Mathf.Ceil(world.millionYears).ToString();
		chooseMut.SetTrigger("Highlighted");
		chooseColor.SetTrigger("Highlighted");
		if(mutationsSelected != 1)
		{
			animator.SetTrigger("0");
		}
		else
		{
			animator.SetTrigger(state.ToString());
		}
		if(state !=2)
		{
			for (int i = 0; i < partButtons.Length; i++)
			{
				Part part = null;
				if(mutationsSelected !=1)
				{
					part = world.futurePlayer.SuggestMutation();
					world.mutationTypes[i] = part.type;
					world.mutationIndexes[i] = world.partData.IndexOfPart(part);
				}
				else
				{
					part = world.partData.PartFromIndex(world.mutationTypes[i], world.mutationIndexes[i]);
				}
				Image image = partButtons[i];
				image.sprite = Resources.Load<Sprite>("Sprites/Interface/Parts/" + part.type);
				Button button = image.GetComponentInParent<Button>();
				Animator anim = button.GetComponent<Animator>();
				int index = i;

				button.onClick.AddListener
				(
					delegate
					{
						anim.SetBool("selected", true);
						if (world.previousChange != null)
						{
							world.ChangePlayerPart(world.player.PartFromType(world.previousChange.type));
						}
						world.ChangePlayerPart(part);
						chooseMut.SetTrigger("Normal");
						if(!mutationChosen)
						{
							StartCoroutine("OpaqueEvolveText");
							mutationChosen = true;
						}
						selectedPartIndex = index;
						for (int l = 0; l < partButtons.Length; l++)
						{
							if(l != selectedPartIndex)
							{
								partButtons[l].GetComponentInParent<Animator>().SetBool("selected", false);
							}
						}
						builder.transform.parent.localScale /= 3;
						previewBuilder.UpdateAnimal();
						builder.transform.parent.localScale *= 3;
						UpdateUI();
					}
				);
			}
			for (int i = 0; i < colorButtons.Length; i++)
			{
				Color color = new Color(0, 0, 0, 1);
				if(mutationsSelected !=1)
				{
					color = Misc.PseudoRandomColor();
					world.mutationColors[i] = color;
				}
				else
				{
					color = world.mutationColors[i];
				}
				colorButtons[i].color = color;
				Button button = colorButtons[i].GetComponentInParent<Button>();
				Animator anim = button.GetComponent<Animator>();
				int index = i;

				if( i % 3 == 0 || i % 3 == 1)
				{
					colorButtons[i].sprite = Resources.Load<Sprite>("Sprites/Interface/Parts/mainColor");
					button.onClick.AddListener
					(
						delegate
						{
							anim.SetBool("selected", true);
							world.futurePlayer.MainColor(color);
							chooseColor.SetTrigger("Normal");
							selectedMainColorIndex = index;
							for (int l = 0; l < colorButtons.Length; l++)
							{
								if(l != selectedMainColorIndex && ( l % 3 == 0 || l % 3 == 1))
								{
									colorButtons[l].GetComponentInParent<Animator>().SetBool("selected", false);
								}
							}
							builder.transform.parent.localScale /= 3;
							previewBuilder.UpdateAnimal();
							builder.transform.parent.localScale *= 3;

						}
					);
				}/*
				else if ( i % 3 == 1)
				{
					colorButtons[i].sprite = Resources.Load<Sprite>("Sprites/Interface/Parts/secondaryColor");
					colorButtons[i].GetComponentInParent<Button>().onClick.AddListener
					(
						delegate
						{
							anim.SetBool("selected", true);
							world.futurePlayer.SecondaryColor(color);
							chooseColor.SetTrigger("Normal");
							selectedSecondaryColorIndex = index;
							for (int l = 0; l < colorButtons.Length; l++)
							{
								if(l != selectedSecondaryColorIndex && l % 3 == 1)
								{
									colorButtons[l].GetComponentInParent<Animator>().SetBool("selected", false);
								}
							}
							builder.transform.parent.localScale /= 3;
							previewBuilder.UpdateAnimal();
							builder.transform.parent.localScale *= 3;

						}
					);
				}*/
				else
				{
					colorButtons[i].sprite = Resources.Load<Sprite>("Sprites/Interface/Parts/eyeColor");
					colorButtons[i].GetComponentInParent<Button>().onClick.AddListener
					(
						delegate
						{
							anim.SetBool("selected", true);
							world.futurePlayer.EyeColor(color);
							chooseColor.SetTrigger("Normal");
							selectedEyeColorIndex = index;
							for (int l = 0; l < colorButtons.Length; l++)
							{
								if(l != selectedEyeColorIndex && l % 3 == 2)
								{
									colorButtons[l].GetComponentInParent<Animator>().SetBool("selected", false);
								}
							}
							builder.transform.parent.localScale /= 3;
							previewBuilder.UpdateAnimal();
							builder.transform.parent.localScale *= 3;

						}
					);
				}
			}

		}
		UpdateUI();
		if(state != 2)
		{
			previewBuilder.StartFadeIn();
			builder.StartFadeOut();
			world.saver.SaveMutations(world);
		}
		else
		{
			chooseBiome.enabled = true;
			Invoke("HighlightChooseBiome",0.1f);
			builder.ChangeMaterial(builder.cutoutMaterial);
		}
		music.maxVolume = 0.8f;
		music.Play(music.evolutionI);
	}

	public void Confirm ()
	{
		StartCoroutine(music.CrossFade( music.evolutionTransition, 1.5f, 0.8f));
		animator.SetTrigger("preEvolution");
		Invoke ("StartShine",1.5f);
		previewBuilder.preview = false;
		builder.preview = false;
		previewBuilder.StopAllCoroutines();
		builder.StopAllCoroutines();
		previewBuilder.maxAlpha = previewBuilder.meshes[0].color.a;
		previewBuilder.StartFadeOut();
		builder.StartFadeIn();
	}
	void StartShine()
	{
		whiteBuilder.species = world.player;
		builder.transform.parent.localScale /= 3;
		whiteBuilder.UpdateAnimal();
		builder.transform.parent.localScale *= 3;
		whiteBuilder.StartFadeIn();
		builder.ChangeMaterial();
		Invoke ("ShowNewEvolution", shineUpTransition);
		StartCoroutine("ShineUp");

	}

	void ShowNewEvolution()
	{
		world.NewBiomes();
		for (int i = 0; i < biomeButtons.Length; i++)
		{
			Biome newBiome = world.currentBiome.connectedBiomes[i];
			Animator anim = biomeButtons[i].GetComponent<Animator>();
			int k = 0;
			biomeButtons[i].GetComponentInChildren<Image>().sprite = newBiome.button;
			biomeButtons[i].onClick.AddListener
			(
				delegate
				{
					tempBiome = newBiome;
					chooseBiome.SetTrigger("Normal");
					anim.SetBool("selected", true);
					for (int l = 0; l < biomeButtons.Length; l++)
					{
						if(world.currentBiome.connectedBiomes[l]!=newBiome)
						{
							biomeButtons[l].GetComponent<Animator>().SetBool("selected", false);
						}
					}
					if(!biomeChosen)
					{
						StartCoroutine("OpaqueContinueText");
						biomeChosen = true;
					}
					UpdateUI();
				}
			);
		}
		world.AddPlayerMutation();

		for(int i = 0; i < world.futurePlayer.newEvolutions.Count; i++)
		{
			world.futurePlayer.previousEvolutions.Add(world.futurePlayer.newEvolutions[i]);
		}
		world.futurePlayer.newEvolutions.Clear();
	    world.NewGeneration();
		builder.species = world.player;
		previewBuilder.gameObject.SetActive(false);
		whiteBuilder.species = world.player;
		builder.transform.parent.localScale /= 3;
		whiteBuilder.UpdateAnimal();
		builder.UpdateAnimal();
		whiteBuilder.StartFadeOut();
		StartCoroutine("ShineDown");
		builder.transform.parent.localScale *= 3;
		panelMutation.SetActive(false);
		panelEnd.SetActive(true);
		chooseBiome.enabled = true;
		world.Save(2);
		UpdateUI();
	}
	public void StartNextLevel()
	{
		world.Save(0);
		world.currentBiome = tempBiome;
		animator.SetTrigger("outro");
		Invoke("LoadNext", 1.5f);
	}
	public void LoadNext()
	{
		PlayerPrefs.SetInt("mutationsSelected", 0);
		Misc.LoadScene("Level");
	}

    IEnumerator OpaqueEvolveText()
    {
        float opaqueTime = 0;
		float maxOpaqueTime = 0.5f;

        while(evolveText.color.a < 1)
        {
			evolveText.color = new Color (1f, 1f, 1f, opaqueTime/ maxOpaqueTime);
            opaqueTime += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator OpaqueContinueText()
    {
        float opaqueTime = 0;
		float maxOpaqueTime = 0.5f;

        while(continueText.color.a < 1)
        {
			continueText.color = new Color (1f, 1f, 1f, opaqueTime/ maxOpaqueTime);
            opaqueTime += Time.deltaTime;
            yield return null;
        }
    }



    IEnumerator ShineUp()
    {
        float shineTime = 0;

        while(shineTime < shineUpTransition)
        {
            fxControl.bloomRadius = (shineTime/shineUpTransition)*7;
	        fxControl.bloomIntensity = (shineTime/shineUpTransition)*50;
			years.text = Mathf.Ceil((world.millionYears + world.yearsPerGeneration*(shineTime/shineUpTransition))).ToString();
            shineTime += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator ShineDown()
    {
        float shineTime = 0;
		Invoke("PlayEvolutionII", shineDownTransition - 1);

        while(shineTime < shineDownTransition)
        {
            fxControl.bloomRadius = (1 - (shineTime/shineDownTransition))*7;
	        fxControl.bloomIntensity = (1 - (shineTime/shineDownTransition))*50;
            shineTime += Time.deltaTime;
            yield return null;
        }
		animator.SetTrigger("postEvolution");
		chooseBiome.enabled = true;
		Invoke("HighlightChooseBiome",0.1f);
    }

	void UpdateUI()
	{

		int dietCount = 0;

		if(world.futurePlayer.planktivore)
		{
			dietImages[dietCount].sprite = plankton;
			whiteDietImages[dietCount].sprite = planktonWhite;
			foods[dietCount] = planktonItem;
			framesDiet[dietCount].sprite = frameIcon;

			dietCount ++;
		}
		if(world.futurePlayer.carnivore)
		{
			dietImages[dietCount].sprite = meat;
			whiteDietImages[dietCount].sprite = meatWhite;
			foods[dietCount] = meatItem;
			framesDiet[dietCount].sprite = frameIcon;
			dietCount ++;
		}

		if(world.futurePlayer.herbivoreEfficacy > 0)
		{
			dietImages[dietCount].sprite = seaweed;
			whiteDietImages[dietCount].sprite = seaweedWhite;
			foods[dietCount] = seaweedItem;
			framesDiet[dietCount].sprite = frameIcon;
			dietCount ++;
			dietImages[dietCount].sprite = lycopodium;
			whiteDietImages[dietCount].sprite = lycopodiumWhite;
			foods[dietCount] = lycopodiumItem;
			framesDiet[dietCount].sprite = frameIcon;
			dietCount ++;
			if(world.futurePlayer.herbivoreEfficacy > 1)
			{
				dietImages[dietCount].sprite = fern;
				whiteDietImages[dietCount].sprite = fernWhite;
				foods[dietCount] = fernItem;
				framesDiet[dietCount].sprite = frameIcon;
				dietCount ++;
				if(world.futurePlayer.herbivoreEfficacy > 2)
				{
					dietImages[dietCount].sprite = cycad;
					whiteDietImages[dietCount].sprite = cycadWhite;
					foods[dietCount] = cycadItem;
					framesDiet[dietCount].sprite = frameIcon;
					dietCount ++;
					if(world.futurePlayer.herbivoreEfficacy > 3)
					{
						dietImages[dietCount].sprite = grass;
						whiteDietImages[dietCount].sprite = grassWhite;
						foods[dietCount] = grassItem;
						framesDiet[dietCount].sprite = frameIcon;
						dietCount ++;
					}
				}
			}
		}

		for ( ; dietCount < dietImages.Length; dietCount++)
		{
			dietImages[dietCount].sprite = transparent;
			whiteDietImages[dietCount].sprite = transparent;
			framesDiet[dietCount].sprite = transparent;
		}

		for (int i = 0; i < floraImages.Length; i++)
		{
			if(tempBiome.floraStrings.Count > i)
			{
				floraImages[i].sprite = Resources.Load<Sprite>("Sprites/Interface/Biomes/" + tempBiome.floraStrings[i] + "Icon");
				whiteFloraImages[i].sprite = Resources.Load<Sprite>("Sprites/Interface/Biomes/" + tempBiome.floraStrings[i] + "IconWhiter");
			}
			else
			{
				floraImages[i].sprite = transparent;
				whiteFloraImages[i].sprite = transparent;
			}
		}
		health.text = ((int)(healthFactor * world.futurePlayer.maxLife)).ToString();
		if(world.futurePlayer.maxLife > world.player.maxLife)
		{
			if(world.player.maxLife == 0)
			{
				newHealth.text = "+" + ((int)(world.futurePlayer.maxLife)).ToString();
			}
			else
			{
				newHealth.text = "+" + ((int)((world.futurePlayer.maxLife - world.player.maxLife)/world.player.maxLife*100)).ToString() + "%";
			}
			healthArrow.sprite = upArrow;
		}
		else if(world.futurePlayer.maxLife < world.player.maxLife)
		{
			newHealth.text = "-" + ((int)((world.player.maxLife - world.futurePlayer.maxLife)/world.player.maxLife*100)).ToString() + "%";
			healthArrow.sprite = downArrow;
		}
		else
		{
			healthArrow.sprite = transparent;
			newHealth.text =  "";
		}
		defense.text = ((int)(world.futurePlayer.defense)).ToString();
		if(world.futurePlayer.defense > world.player.defense)
		{
			if(world.player.defense == 0)
			{
				newDefense.text = "+" + ((int)(world.futurePlayer.defense)).ToString();
			}
			else
			{

				newDefense.text = "+" + ((int)((world.futurePlayer.defense - world.player.defense)/world.player.defense*100)).ToString() + "%";
			}
			defenseArrow.sprite = upArrow;
		}
		else if(world.futurePlayer.defense < world.player.defense)
		{
			newDefense.text = "-" + ((int)((world.player.defense - world.futurePlayer.maxLife)/world.player.defense*100)).ToString() + "%";
			defenseArrow.sprite = downArrow;

		}
		else
		{
			defenseArrow.sprite = transparent;
			newDefense.text =  "";
		}
		terrestrialSpeed.text = ((int)(speedFactor*world.futurePlayer.terrestrialSpeed)).ToString();
		if(world.futurePlayer.terrestrialSpeed > world.player.terrestrialSpeed)
		{
			if(world.player.terrestrialSpeed == 0)
			{
				newTerrestrialSpeed.text = "+" + ((int)(world.futurePlayer.terrestrialSpeed)).ToString();
			}
			else
			{

				newTerrestrialSpeed.text = "+" + ((int)((world.futurePlayer.terrestrialSpeed - world.player.terrestrialSpeed)/world.player.terrestrialSpeed*100)).ToString() + "%";
			}
			terrestrialSpeedArrow.sprite = upArrow;
		}
		else if(world.futurePlayer.terrestrialSpeed < world.player.terrestrialSpeed)
		{
			newTerrestrialSpeed.text = "-" + ((int)((world.player.terrestrialSpeed - world.futurePlayer.terrestrialSpeed)/world.player.terrestrialSpeed*100)).ToString() + "%";
			terrestrialSpeedArrow.sprite = downArrow;

		}
		else
		{
			terrestrialSpeedArrow.sprite = transparent;
			newTerrestrialSpeed.text =  "";
		}
		aquaticSpeed.text = ((int)(speedFactor*world.futurePlayer.aquaticSpeed)).ToString();
		if(world.futurePlayer.aquaticSpeed > world.player.aquaticSpeed)
		{
			if(world.player.aquaticSpeed == 0)
			{
				newAquaticSpeed.text = "+" + ((int)(world.futurePlayer.aquaticSpeed)).ToString();
			}
			else
			{

				newAquaticSpeed.text = "+" + ((int)((world.futurePlayer.aquaticSpeed - world.player.aquaticSpeed)/world.player.aquaticSpeed*100)).ToString() + "%";
			}
			aquaticSpeedArrow.sprite = upArrow;
		}
		else if(world.futurePlayer.aquaticSpeed < world.player.aquaticSpeed)
		{
			newAquaticSpeed.text = "-" + ((int)((world.player.aquaticSpeed - world.futurePlayer.aquaticSpeed)/world.player.aquaticSpeed*100)).ToString() + "%";
			aquaticSpeedArrow.sprite = downArrow;

		}
		else
		{
			aquaticSpeedArrow.sprite = transparent;
			newAquaticSpeed.text =  "";
		}
		maxAttack.text = ((int)(world.futurePlayer.maxAttack)).ToString();
		if(world.futurePlayer.maxAttack > world.player.maxAttack)
		{
			if(world.player.maxAttack == 0)
			{
				newMaxAttack.text = "+" + ((int)(world.futurePlayer.maxAttack)).ToString();
			}
			else
			{

				newMaxAttack.text = "+" + ((int)((world.futurePlayer.maxAttack - world.player.maxAttack)/world.player.maxAttack*100)).ToString() + "%";
			}
			maxAttackArrow.sprite = upArrow;
		}
		else if(world.futurePlayer.maxAttack < world.player.maxAttack)
		{
			newMaxAttack.text = "-" + ((int)((world.player.maxAttack - world.futurePlayer.maxAttack)/world.player.maxAttack*100)).ToString() + "%";
			maxAttackArrow.sprite = downArrow;
		}
		else
		{
			maxAttackArrow.sprite = transparent;
			newMaxAttack.text =  "";
		}

		for (int i = 0; i < attackImages.Length; i++)
		{
			if(i < world.player.attacks.Count)
			{
				attackImages[i].transform.parent.gameObject.SetActive(true);
				attackImages[i].sprite = world.player.attacks[i].sprite;
				attackText[i].text = world.player.attacks[i].damage.ToString();
				attackTooltip[i].text = Misc.FirstCharToUpper(world.player.attacks[i].name[0]);
			}
			else
			{
				attackImages[i].transform.parent.gameObject.SetActive(false);
				attackText[i].text = "";
				attackTooltip[i].text ="";

			}
			if(!panelEnd.activeSelf)
			{
				if(i < world.futurePlayer.attacks.Count)
				{
					newAttackImages[i].transform.parent.gameObject.SetActive(true);
					newAttackImages[i].sprite = world.futurePlayer.attacks[i].sprite;
					newAttackText[i].text = world.futurePlayer.attacks[i].damage.ToString();
					newAttackTooltip[i].text = Misc.FirstCharToUpper(world.futurePlayer.attacks[i].name[0]);
				}
				else
				{
					newAttackImages[i].transform.parent.gameObject.SetActive(false);
					newAttackText[i].text = "";
					newAttackTooltip[i].text ="";

				}
			}
			else
			{
				newAttackImages[i].transform.parent.gameObject.SetActive(false);
				newAttackText[i].text = "";
				newAttackTooltip[i].text ="";

			}
		}
		temperature.text = ((int)tempBiome.temperature).ToString() + " °C";

		if(tempBiome.temperature < 5)
		{
			temperatureImage.sprite = freezing;
		}
		else if (tempBiome.temperature < 15)
		{
			temperatureImage.sprite = cold;
		}
		else if (tempBiome.temperature < 25)
		{
			temperatureImage.sprite = temperate;
		}
		else if (tempBiome.temperature < 35)
		{
			temperatureImage.sprite = hot;
		}
		else
		{
			temperatureImage.sprite = scorching;
		}

		humidity.text = ((int)tempBiome.humidity).ToString();
		if(tempBiome.humidity < 100)
		{
			humidity.text = "Dry";
			humidityImage.sprite = dry;
		}
		else if (tempBiome.humidity < 200)
		{
			humidity.text = "Average";
			humidityImage.sprite = average;
		}
		else if (tempBiome.humidity < 300)
		{
			humidity.text = "Humid";
			humidityImage.sprite = humid;
		}
		else
		{
			humidity.text = "Very Humid";
			humidityImage.sprite = veryHumid;
		}

		if(tempBiome.sealevel < 0.25f)
		{
			sealevel.text = "Land";
			sealevelImage.sprite = landSprite;
		}
		else if (tempBiome.sealevel == 1)
		{
			sealevel.text = "Sea";
			sealevelImage.sprite = seaSprite;
		}
		else
		{
			sealevel.text = "Coast";
			sealevelImage.sprite = coastSprite;
		}
		int animalCount = 0;
		foreach (Niche nich in tempBiome.niches)
		{
			if(nich.species != null && !(nich.species == world.player))
			{
				animalCount += nich.abundance;
			}
		}
		speciesText.text = "Animals: " + animalCount.ToString();
	}
	public void HighlightChooseBiome()
	{
		chooseBiome.SetTrigger("Highlighted");
		biomeButtons[0].GetComponent<Animator>().SetBool("selected", true);
	}
	public void SelectColor()
	{
		partsPanel.SetActive(false);
		colorPanel.SetActive(true);
	}
	public void SelectPart()
	{
		partsPanel.SetActive(true);
		colorPanel.SetActive(false);
	}
	public void ChangeFloraDescription(int n)
	{
		if(n < tempBiome.floraStrings.Count)
		{
			floraTooltip.text = Misc.FirstCharToUpper(tempBiome.floraStrings[n]);
		}
	}
	public void EraseFloraDescription()
	{
		floraTooltip.text = "";
	}
	public void ChangeFoodDescription(int n)
	{
		if(foods[n]!= null)
		{
			dietTooltip.text = Misc.FirstCharToUpper(foods[n].name[0]);
		}
	}
	public void EraseFoodDescription()
	{
		dietTooltip.text = "";
	}
	public void PrevisualizeAttack(int i)
	{
		if(attackTooltip[i].text == "Bite")
		{
			builder.GetComponent<AnimationManager>().HeadAttack();
		}
		else if (attackTooltip[i].text == "Lash")
		{
			builder.GetComponent<AnimationManager>().TailAttack();
		}
		else if(attackTooltip[i].text == "Punch")
		{
			builder.GetComponent<AnimationManager>().ArmAttack();
		}
	}
	public void PrevisualizeFutureAttack(int i)
	{
		if(newAttackTooltip[i].text == "Bite")
		{
			previewBuilder.GetComponent<AnimationManager>().HeadAttack();
		}
		else if (newAttackTooltip[i].text == "Lash")
		{
			previewBuilder.GetComponent<AnimationManager>().TailAttack();
		}
		else if(newAttackTooltip[i].text == "Punch")
		{
			previewBuilder.GetComponent<AnimationManager>().ArmAttack();
		}
	}
	void PlayEvolutionII()
	{
		music.Play(music.evolutionII);
	}
}
