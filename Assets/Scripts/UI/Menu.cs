﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Menu : MonoBehaviour
{
	public AnimalBuilder builder;
	public float evolutionTime = 2;
	public PostProcessingControl fxPlay;
	public Animator animator;
	public World world;
	public Button continueButton;
	public MusicControl music;

	// Use this for initialization
	void Start ()
	{
		world = GameObject.Find("Data").GetComponent<World>();
		music = world.GetComponent<MusicControl>();
		fxPlay.SetDefault();
		builder.pureWhite = true;
		builder.species = new Species(0,0,0,0);
		builder.UpdateAnimal();
		if(PlayerPrefs.GetInt("saved")!=1)
		{
			continueButton.interactable = false;
			continueButton.GetComponentInChildren<TextMeshPro>().color = new Color (1, 1, 1, 0.25f);
			continueButton.GetComponent<EventTrigger>().enabled = false;
		}
		music.maxVolume = 1;
		Invoke("Evolving", evolutionTime + 0.56f);
		music.Play(music.main);
	}
    void Evolving()
    {

		builder.species = builder.species.Evolve(1);
		builder.UpdateAnimal();
		Invoke("Evolving", evolutionTime);
    }
	public void Play(bool newGame)
	{
		animator.SetTrigger("objective");
		Invoke("PlayAfterObjective", 3f);
		if(newGame)
		{
			world.saver.ClearSaves();
			Invoke("NewGame", 5.5f);
		}
		else
		{
			Invoke("ContinueGame", 5.5f);
		}

	}
	public void PlayAfterObjective()
	{
		animator.SetTrigger("play");

	}
	public void NewGame()
	{
		world.Initialize();
		Misc.LoadScene("Level");
	}
	public void ContinueGame()
	{
		Misc.LoadScene(world.saver.Load(this));
	}
	public void IntroCredits()
	{
		animator.SetTrigger("introCredits");
	}
	public void OutCredits()
	{
		animator.SetTrigger("outCredits");
	}
	void Update()
	{
		if(Input.GetKeyDown("h"))
		{
			animator.SetTrigger("objective");
			Invoke("PlayAfterObjective", 3f);
			world.saver.ClearSaves();
			Invoke("NewGame", 5.5f);

		}
	}

}
