﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PostProcessingControl : MonoBehaviour
{

    public PostProcessingProfile fxProfile;
    public float bloomRadius, bloomIntensity, bloomThreshold, defaultRadius, defaultIntensity, defaultThreshold;
    BloomModel.Settings bloomSettings;
    void Awake()
    {
        defaultRadius = bloomRadius;
        defaultIntensity = bloomIntensity;
        defaultThreshold = bloomThreshold;
    }
    // Use this for initialization
    void Start()
    {
        //copy current bloom settings from the profile into a temporary variable
        BloomModel.Settings bloomSettings = fxProfile.bloom.settings;
    }
    void Update()
    {
         //change the intensity in the temporary settings variable
     	bloomSettings.bloom.radius = bloomRadius;
     	bloomSettings.bloom.intensity = bloomIntensity;
     	bloomSettings.bloom.threshold = bloomThreshold;

         //set the bloom settings in the actual profile to the temp settings with the changed value
         fxProfile.bloom.settings = bloomSettings;
    }
    public void SetDefault()
    {
        bloomRadius = defaultRadius;
        bloomIntensity = defaultIntensity;
        bloomThreshold = defaultThreshold;
        bloomSettings.bloom.radius = bloomRadius;
        bloomSettings.bloom.intensity = bloomIntensity;
        bloomSettings.bloom.threshold = bloomThreshold;
        fxProfile.bloom.settings = bloomSettings;

    }
    void OnDisable()
    {
        SetDefault();
    }
}
