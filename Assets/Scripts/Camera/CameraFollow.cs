﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public Level level;
	public Grid grid;
	public Camera camera;
	public float smoothSpeed = 12.5f, x, y,z,  xMargin, yMargin, marginUp, marginDown, xOffset, yOffset, zOffset;

	void Awake()
	{
		camera = gameObject.GetComponent<Camera>();
		xMargin = camera.orthographicSize*camera.aspect * (-zOffset * yOffset)/5;
		marginUp = camera.orthographicSize  * (-zOffset * yOffset)/3;
		marginDown = camera.orthographicSize  * (-zOffset * yOffset)/10;
	}
	void LateUpdate () {
        if(level.finishedLoading)
        {
			if(target != null)
			{
				float xTarget = target.position.x;
				float zTarget = target.position.z;
				if(xTarget<grid.limitX-xMargin&& xTarget>-(grid.limitX-xMargin))
				{
					x = xTarget + xOffset;
				}
				if(zTarget<grid.limitZ-marginUp&& zTarget>-(grid.limitZ-marginDown))
				{
					z = zTarget + zOffset;
				}
				y = target.position.y + yOffset;
				Vector3 desiredPosition = new Vector3(x,y,z);
				Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
				transform.position = smoothedPosition;
			}
			else
			{
				target = GameObject.FindWithTag("Player").transform;

			}
		}
	}
}
