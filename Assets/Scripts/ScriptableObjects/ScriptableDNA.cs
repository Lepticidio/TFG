﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ScriptableDNA", order = 1)]
public class ScriptableDNA : ScriptableObject {
//

    public List<List<float>> genes = new List<List<float>>();
    public int id = 0;

}
