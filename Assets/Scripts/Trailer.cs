﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trailer : MonoBehaviour
{

	public AnimalBuilder mainBuilder;
	public Color mainColor1 = new Color(0.7f, 0.21f, 0.33f, 1f);
	public List<Color> mainColors = new List<Color>();
	public List<Color> eyeColors = new List<Color>();





	// Use this for initialization
	void Start ()
	{
		mainColor1 = new Color(0.7f, 0.21f, 0.33f, 1f);
		mainBuilder.color = mainColor1;
		mainBuilder.UpdateAnimal();
		mainBuilder.ChangeColor(mainColor1);
		mainBuilder.animationManager.Swim();

	}

	// Update is called once per frame
	void Update ()
	{

	}
}
