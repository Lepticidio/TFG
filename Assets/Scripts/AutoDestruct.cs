﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestruct : MonoBehaviour {

	public float destroyTime;
	// Use this for initialization
	void Start ()
	{
		Invoke("DestroyItself", destroyTime);
	}

	void DestroyItself()
	{
		Destroy(gameObject);
	}
}
