﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceActivator : MonoBehaviour {

    public Transform player;
    bool activated = false;
    public float maxDistance= 900;
    List<GameObject> childs = new List<GameObject>();
    public Level level;

    void Awake()
    {
        Invoke("CheckDistances", Random.Range(0f, 1f));
    }
    void CheckDistances()
    {
        if(level.finishedLoading)
        {
            if(player == null)
            {
                player = GameObject.Find("Player").transform;

            }
            else
            {
                foreach (Transform child in transform)
                {
                    GameObject go = child.gameObject;
                    if(!go.activeSelf && (child.position - player.position).sqrMagnitude<maxDistance && child.position.z > player.position.z - 9)
                    {
                        child.gameObject.SetActive(true);
                    }
                    else if(go.activeSelf &&((child.position - player.position).sqrMagnitude>maxDistance || child.position.z < player.position.z - 9))
                    {
                        child.gameObject.SetActive(false);
                    }
                }
            }
        }
        Invoke("CheckDistances", 0.5f);
    }
}
