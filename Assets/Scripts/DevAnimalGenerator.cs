﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevAnimalGenerator : MonoBehaviour
{

	public GameObject fleeAnimal, fightAnimal, seekAnimal;
	public int nFlee, nFight, nSeek;
	public float xMax, zMax;


	public void CreateAnimals()
	{
		for (int i = 0; i< nFlee; i++)
		{
			Instantiate(fleeAnimal, new Vector3(Random.Range (-xMax, xMax), 0.2f, Random.Range (-zMax, zMax)), Quaternion.identity);
		}
		for (int i = 0; i< nFight; i++)
		{
			Instantiate(fightAnimal, new Vector3(Random.Range (-xMax, xMax), 0.2f, Random.Range (-zMax, zMax)), Quaternion.identity);
		}
		for (int i = 0; i< nSeek; i++)
		{
			Instantiate(seekAnimal, new Vector3(Random.Range (-xMax, xMax), 0.2f, Random.Range (-zMax, zMax)), Quaternion.identity);
		}

	}
}
