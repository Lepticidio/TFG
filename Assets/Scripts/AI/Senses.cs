﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Senses : MonoBehaviour {

    public bool upBlocked, downBlocked, frontBlocked, rightBlocked, leftBlocked, testing, player;
    public float preyDistance, predatorDistance, foodDistance, mateDistance;
    public int herbivoreEfficacy = 1;
    public Transform mouth;
    public Vector3  preyDir = new Vector3 (0, 0, 0), predatorDir = new Vector3 (0, 0, 0), foodDir = Vector3.zero, mateDir = Vector3.zero;
    List<Animal> preys = new List<Animal>();
    List<Animal> predators = new List<Animal>();
    List<Animal> mates = new List<Animal>();
    List<Food> foods = new List<Food>();
    public Species species;
    public Animal closestPrey, closestPredator, closestMate;
    public Food closestFood;

    void OnTriggerEnter (Collider col)
    {
        if(((col.tag == "Player" && !testing )|| col.tag == "NPC"))
        {
            Animal other = col.GetComponentInParent<Animal>();
            Species otherSpecies = other.builder.species;
            if(otherSpecies != species)
            {
                if(otherSpecies.carnivore && otherSpecies.maxAttack > species.defense)
                {
                    predators.Add(other);
                }
                if (species.carnivore && otherSpecies.defense < species.maxAttack)
                {
                    preys.Add(other);
                }

            }
            else if( player)
            {
                mates.Add (other);
            }

        }
        else if (species.carnivore && col.tag == "Meat")
        {
            Food meat = col.GetComponentInParent<Food>();
            foods.Add(meat);
        }

        else if (species.planktivore && col.tag == "Plankton")
        {
            Food plankton = col.GetComponentInParent<Food>();

            foods.Add(plankton);

        }
        else if(species.herbivore && col.tag == "Vegetable")
        {
            Food vegetable = col.GetComponentInParent<Food>();
            if(vegetable.hardness<=herbivoreEfficacy)
            {
                foods.Add(vegetable);
            }
        }
    }
    void OnTriggerExit (Collider col)
    {

        if((col.tag == "Player" && !testing) || col.tag == "NPC")
        {

            Animal other = col.GetComponentInParent<Animal>();
            Species otherSpecies = other.builder.species;
            if(otherSpecies != species)
            {
                if(predators.Contains(other))
                {
                    predators.Remove(other);
                }
                if(species.carnivore)
                {
                    if(preys.Contains(other))
                    {
                        preys.Remove(other);
                    }
                }
            }
            else if( player)
            {
                mates.Remove(other);
            }
        }
        if(species.herbivore)
        {
            if(col.tag == "Vegetable")
            {
                RemoveFood(col.GetComponentInParent<Food>());
                FindClosestFood();
            }
        }
        if(species.carnivore)
        {
            if(col.tag == "Meat")
            {
                RemoveFood(col.GetComponentInParent<Food>());
                FindClosestFood();
            }
        }
        if(species.planktivore)
        {
            if(col.tag == "Plankton")
            {
                RemoveFood(col.GetComponentInParent<Food>());
                FindClosestFood();
            }
        }
    }

    void Update()
    {

        closestFood = FindClosestFood();
        closestPredator = FindClosestPredator();

        if(species.carnivore)
        {
            closestPrey = FindClosestPrey();
        }

        if (player)
        {
            closestMate = FindClosestMate();
        }
    }

    public Animal FindClosestPrey()
    {
        return Misc.FindClosest<Animal>(ref preys, transform.parent, ref preyDir, ref preyDistance);
    }

    public Animal FindClosestPredator()
    {
        return Misc.FindClosest<Animal>(ref predators, transform.parent, ref predatorDir, ref predatorDistance);
    }
    public Animal FindClosestMate()
    {
        return Misc.FindClosest<Animal>(ref mates, transform.parent, ref mateDir, ref mateDistance);
    }
    public Food FindClosestFood()
    {
        for (int i = 0; i < foods.Count; i++)
        {
            if(foods[i].consumed)
            {
                RemoveFood(foods[i]);
            }
        }
        return Misc.FindClosest<Food>(ref foods, transform.parent, ref foodDir, ref foodDistance);

    }

    public void RemoveFood(Food food)
    {
        if(food != null)
        {
            foods.Remove(food);
        }
    }
    public void RemoveClosestPrey()
    {
        if(closestPrey != null)
        {
            preys.Remove(closestPrey);
        }
    }
    public void RemoveClosestFood()
    {
        RemoveFood(closestFood);
    }

}
