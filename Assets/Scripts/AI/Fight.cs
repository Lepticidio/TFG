﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Fight : AnimalBehaviour
{

    IEnumerator coroutine;
    Animal target;

    void Start ()
    {
        coroutine = CheckPath();
    }
    // Update is called once per frame
    void FixedUpdate ()
    {
        Animal tempTarget = brain.senses.closestPrey;
        if(!brain.species.carnivore)
        {
            tempTarget = brain.senses.closestPredator;
        }
        if(tempTarget != null)
        {
            if(target != tempTarget)
            {
                target = tempTarget;

                if(brain.grid.FloorDistance (target.transform) < floorMargin)
                {
                    InitiatePath();
                }
            }

            Vector3 dir = target.transform.position - brain.animal.maxAttackPart.transform.position;
            Vector3 mirrorPos = new Vector3(2*transform.position.x - brain.animal.maxAttackPart.transform.position.x,
                brain.animal.maxAttackPart.transform.position.y, brain.animal.maxAttackPart.transform.position.z);
            Vector3 mirrorDir = target.transform.position - mirrorPos;


            if(!brain.agent.enabled && brain.grid.FloorDistance (target.transform) < floorMargin)
            {
                InitiatePath();
            }

            if(brain.animal.maxAttackPart.possibleVictims.Contains(target))
            {
                MaxAttack();
            }
            else if (brain.grid.FloorDistance (target.transform) > floorMargin)
            {
                brain.agent.enabled = false;
                if(!brain.animal.rearAttack)
                {
                    Follow (dir);
                }
                else
                {
                    Follow (mirrorDir);
                }
            }
            else if(brain.agent.isOnNavMesh && FollowPath())
            {
                /*
                for (int i = 0; i < path.corners.Length; i++)
                {
                    if(i > 0)
                    {
                        if(i< currentCorner)
                        {
                            Debug.DrawRay(path.corners[i -1], path.corners[i] - path.corners[i -1], Color.black);
                        }
                        else
                        {
                            Debug.DrawRay(path.corners[i -1], path.corners[i] - path.corners[i -1], Color.red);
                        }

                    }
                }
                */
            }
            else
            {
                if(!brain.animal.rearAttack)
                {
                    Follow (dir);
                }
                else
                {
                    Follow (mirrorDir);
                }
            }
            if(brain.animal.rearAttack && brain.animal.headAttack.possibleVictims.Contains(target))
            {
                Debug.Log("flipping");
                brain.actions.Flip();
            }
        }
        else
        {
            Wander();
        }
    }

    void InitiatePath()
    {
        brain.agent.enabled = true;
        if(brain.agent.isOnNavMesh && !brain.agent.CalculatePath(DesiredVector(), path))
        {
            if(brain.species.carnivore)
            {
                brain.senses.RemoveClosestPrey();
                brain.senses.FindClosestPrey();
            }
        }
        else if ( brain.agent.isOnNavMesh)
        {
            StartCoroutine(coroutine);

            currentCorner = 0;
        }
    }
    IEnumerator CheckPath()
    {
        while (brain.agent.enabled && target != null)
        {
            if(brain.agent.isOnNavMesh && !brain.agent.CalculatePath(DesiredVector(), path))
            {
                if(brain.species.carnivore)
                {
                    brain.senses.RemoveClosestPrey();
                    brain.senses.FindClosestPrey();
                }
                brain.agent.enabled = false;

            }
            if(brain.grid.FloorDistance (target.transform) > floorMargin)
            {
                brain.agent.enabled = false;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
    Vector3 DesiredVector()
    {
        float zMargin = 0;
        int zFactor = 0;
        float xMargin = 0;
        int xFactor = 0;
        if(transform.position.z < target.transform.position.z)
        {
            zFactor = -1;
        }
        else
        {
            zFactor = 1;
        }
        if(brain.animal.rearAttack)
        {
            zMargin = brain.builder.height * 2 * zFactor;
        }
        if(rightIfTie)
        {
            xFactor = 1;
        }
        else
        {
            xFactor = -1;
        }
        xMargin = xFactor*brain.animal.attackPartDistance;
        return new Vector3(xMargin, 0, zMargin) + target.transform.position;

    }
    void MaxAttack()
    {
        if(brain.species.maxAttackInt == 1)
        {
            brain.actions.HeadAttack();
        }
        else if (brain.species.maxAttackInt == 2)
        {
            brain.actions.BodyAttack();
        }
        else if(brain.species.maxAttackInt == 3)
        {
            brain.actions.TailAttack();
        }
        else
        {
            brain.actions.LegAttack();
        }

    }
}
