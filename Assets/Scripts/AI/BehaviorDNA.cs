﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourDNA {
//
    public List<List<float>> genes = new List<List<float>>();
    int dnaLength = 0;
    int stateNumber = 0;
    float maxValues = 0;

    public BehaviourDNA(int sn, int l, float v)
    {
        stateNumber = sn;
        dnaLength = l;
        maxValues = v;
        SetRandom();
    }

    public void SetRandom()
    {
        genes.Clear();
        for (int j = 0; j < stateNumber; j++)
        {
            List<float> stateGenes = new List<float>();
            for(int i = 0; i < dnaLength; i++)
            {
                stateGenes.Add(Random.Range(-maxValues, maxValues));
            }
            genes.Add(stateGenes);
        }
    }

    public void Setfloat(int state, int pos, float value)
    {
        genes[state][pos] = value;
    }

    public void Combine(BehaviourDNA d1, BehaviourDNA d2)
    {
        for (int j = 0; j < stateNumber; j++)
        {
            for(int i = 0; i < dnaLength; i++)
            {
                genes[j][i] = Random.Range(0,10) < 5 ? d1.genes[j][i] : d2.genes[j][i];
            }
        }
    }

    public void Mutate()
    {
        genes[Random.Range(0,stateNumber)][Random.Range(0,dnaLength)] = Random.Range(-maxValues, maxValues);
    }

    public float GetGene(int state, int pos)
    {
        return genes[state][pos];
    }


}
