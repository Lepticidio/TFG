﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Brain : MonoBehaviour
{
    public bool test;
    public float dangerDistance, maxBehaviourValue = 60;
    public float[] behaviourValues = new float[4];
    AnimalBehaviour currentBehaviour;
    AnimalBehaviour[] behaviours = new AnimalBehaviour[4];
    public Animal animal;
    public Actions actions;
    public AnimalBuilder builder;
    public Senses senses;
    public Vector3 fearVector;
    public NavMeshAgent agent;
    public Grid grid;
    public Species species;
    public Level level;

    void Awake()
    {
        animal = GetComponent<Animal>();
        actions = GetComponent<Actions>();
        builder = GetComponentInChildren<AnimalBuilder>();
        agent = transform.parent.GetComponent<NavMeshAgent>();
        grid = GameObject.Find("Controller").GetComponent<Grid>();
        level = grid.GetComponent<Level>();
        behaviours[0] = GetComponent<SeekFood>();
        behaviours[1] = GetComponent<Fight>();
        behaviours[2] = GetComponent<Flee>();
        behaviours[3] = GetComponent<SeekMate>();
        behaviours[0].enabled = true;
        behaviourValues[0] = maxBehaviourValue;
        currentBehaviour = behaviours[0];
        senses = transform.parent.GetComponentInChildren<Senses>();
        fearVector = new Vector3(Random.Range(-100f, 100f), Random.Range(-100f, 100f), Random.Range(-100f, 100f)).normalized;

        //
    }
    // Update is called once per frame
    void Update()
    {
        /*
        if(test)
        {
            Debug.DrawRay(transform.position, fearVector, Color.magenta);
        }
        */

    }
    void FixedUpdate()
    {

        //SeekFood is 0
        //Fight is 1
        //Flee is 2
        //SeekMate is 3
        if(species == null)
        {
            species = builder.species;
        }
        if(senses.player && level.matingSeason && senses.closestMate != null && animal.hunger > species.maxHunger / 3)
        {
            IncreaseBehaviour(3);
        }
        else if(senses.closestPredator != null)
        {
            if(ShouldFight(senses.closestPredator))
            {
                IncreaseBehaviour(1);
            }
            else
            {
                IncreaseBehaviour(2);
            }

        }
        else if (species.carnivore && senses.closestPrey != null && senses.closestFood == null  && ShouldFight(senses.closestPrey))
        {
            IncreaseBehaviour(1);
        }
        else
        {
            IncreaseBehaviour(0);
        }
        CheckBehaviour();
        if(test)
        {
            if(currentBehaviour.wandering)
            {
                if(currentBehaviour == behaviours[1])
                {
                    animal.builder.ChangeColor(Color.magenta);
                }
                else if (currentBehaviour == behaviours[0])
                {
                    animal.builder.ChangeColor(Color.yellow);
                }
                else if (currentBehaviour == behaviours[2])
                {
                    animal.builder.ChangeColor(Color.cyan);
                }
                else if (currentBehaviour == behaviours[3])
                {
                    animal.builder.ChangeColor(Color.grey);
                }
            }
            else
            {
                if(currentBehaviour == behaviours[1])
                {
                    animal.builder.ChangeColor(Color.red);
                }
                else if (currentBehaviour == behaviours[0])
                {
                    animal.builder.ChangeColor(Color.green);
                }
                else if (currentBehaviour == behaviours[2])
                {
                    animal.builder.ChangeColor(Color.blue);
                }
                else if (currentBehaviour == behaviours[3])
                {
                    animal.builder.ChangeColor(Color.white);
                }
            }
        }
    }

    void CheckBehaviour()
    {
        float temp = 0;
        for (int i = 0; i < behaviours.Length; i++)
        {
            if(behaviourValues[i] == maxBehaviourValue)
            {
                SetCurrent(behaviours[i]);
            }
        }
    }

    void IncreaseBehaviour(int n)
    {
        for (int i = 0; i < behaviourValues.Length; i++)
        {
            if(i == n)
            {
                if(behaviourValues[i] < maxBehaviourValue)
                {
                    behaviourValues[i] ++;
                }
            }
            else
            {
                if(behaviourValues[i] > 0)
                {
                    behaviourValues[i] --;
                }
            }
        }
    }

    void SetCurrent (AnimalBehaviour behaviour)
    {
        if(currentBehaviour != behaviour)
        {
            currentBehaviour.enabled = false;
            currentBehaviour = behaviour;
            currentBehaviour.enabled = true;
        }
    }
    bool ShouldFight(Animal other)
    {
        Species otherSpecies = other.builder.species;
        float strength = (species.maxAttack - otherSpecies.defense) * animal.life;
        float otherStrength = (otherSpecies.maxAttack - species.defense)*other.life;
        if( otherStrength > strength)
        {
            if(strength/otherStrength > 0.75f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }

    }

}
