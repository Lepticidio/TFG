﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekMate : AnimalBehaviour
{

    IEnumerator coroutine;
    Animal target;

    void Start ()
    {
        coroutine = CheckPath();
    }
    // Update is called once per frame
    void FixedUpdate ()
    {
        Animal tempTarget = brain.senses.closestMate;
        if(tempTarget != null)
        {
            if(target != tempTarget)
            {
                target = tempTarget;

                if(brain.grid.FloorDistance (target.transform) < floorMargin)
                {
                    InitiatePath();
                }
            }

            if(brain.senses.mateDistance > brain.actions.minMateDistance)
            {
                if(!brain.agent.enabled && brain.grid.FloorDistance (target.transform) < floorMargin)
                {
                    InitiatePath();
                }
                if (brain.grid.FloorDistance (target.transform) > floorMargin)
                {
                    brain.agent.enabled = false;
                    Follow (brain.senses.mateDir);
                }
                else if(brain.agent.isOnNavMesh && FollowPath())
                {
    				/*
                    for (int i = 0; i < path.corners.Length; i++)
                    {
                        if(i > 0)
                        {
                            if(i< currentCorner)
                            {
                                Debug.DrawRay(path.corners[i -1], path.corners[i] - path.corners[i -1], Color.black);
                            }
                            else
                            {
                                Debug.DrawRay(path.corners[i -1], path.corners[i] - path.corners[i -1], Color.red);
                            }

                        }
                    }
    				*/
                }
                else
                {
                    Follow (brain.senses.mateDir);
                }
            }
            else
            {
                brain.actions.Idle();
            }
        }
        else
        {
            Wander();
        }
    }

    void InitiatePath()
    {
        brain.agent.enabled = true;
        if(brain.agent.isOnNavMesh && !brain.agent.CalculatePath(DesiredVector(), path))
        {
            //    brain.senses.RemoveClosestMate();

        }
        else if ( brain.agent.isOnNavMesh)
        {
            StartCoroutine(coroutine);

            currentCorner = 0;
        }
    }
    IEnumerator CheckPath()
    {
        while (brain.agent.enabled && target != null)
        {
            if(brain.agent.isOnNavMesh && !brain.agent.CalculatePath(DesiredVector(), path))
            {
                //brain.senses.RemoveClosestMate();

                brain.agent.enabled = false;

            }
            if(brain.grid.FloorDistance (target.transform) > floorMargin)
            {
                brain.agent.enabled = false;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
    Vector3 DesiredVector()
    {
        float zMargin = 0;
        int zFactor = 0;
        float xMargin = 0;
        int xFactor = 0;
        if(transform.position.z < target.transform.position.z)
        {
            zFactor = -1;
        }
        else
        {
            zFactor = 1;
        }
        if(brain.animal.rearAttack)
        {
            zMargin = brain.builder.height * 2 * zFactor;
        }
        if(rightIfTie)
        {
            xFactor = 1;
        }
        else
        {
            xFactor = -1;
        }
        xMargin = xFactor*brain.animal.attackPartDistance;
        return new Vector3(xMargin, 0, zMargin) + target.transform.position;

    }
}
