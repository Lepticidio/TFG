using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Flee : AnimalBehaviour
{
    IEnumerator coroutine;
    Animal target;
    public float checkingDistance = 7;
    float rotationSpeed;


    void Start ()
    {
        coroutine = CheckPath();
        rotationSpeed = Time.fixedDeltaTime * 6;
    }
    void FixedUpdate ()
    {
        if(brain.senses.closestPredator != null)
        {
            if(target != brain.senses.closestPredator)
            {
                target = brain.senses.closestPredator;

                if(brain.grid.FloorDistance (target.transform) < floorMargin)
                {
                    InitiatePath();
                }
            }
             brain.fearVector = Vector3.RotateTowards(brain.fearVector, brain.senses.predatorDir, -rotationSpeed, 0.0f);
        }

        if(!brain.agent.enabled && brain.grid.FloorDistance (transform) < floorMargin)
        {
            InitiatePath();
        }
        if (brain.grid.FloorDistance (transform) > floorMargin)
        {
            brain.agent.enabled = false;
            Follow (brain.fearVector);
        }
        else if(brain.agent.isOnNavMesh && FollowPath())
        {
            /*
            for (int i = 0; i < path.corners.Length; i++)
            {
                if(i > 0)
                {
                    if(i< currentCorner)
                    {
                        Debug.DrawRay(path.corners[i -1], path.corners[i] - path.corners[i -1], Color.cyan);
                    }
                    else
                    {
                        Debug.DrawRay(path.corners[i -1], path.corners[i] - path.corners[i -1], Color.blue);
                    }

                }
            }
            */
        }
        else
        {
            Follow (brain.fearVector);
        }
    }

    void InitiatePath()
    {
        Vector3 result = Vector3.zero;
        if(GetFarPosition(out result))
        {
            brain.agent.enabled = true;
            if(brain.agent.isOnNavMesh && !brain.agent.CalculatePath(result, path))
            {
            }
            else if ( brain.agent.isOnNavMesh)
            {
                StartCoroutine(coroutine);

                currentCorner = 0;
            }
        }
    }
    IEnumerator CheckPath()
    {
        Vector3 result = Vector3.zero;
        while (brain.agent.enabled && GetFarPosition( out result))
        {
            if(brain.agent.isOnNavMesh && !brain.agent.CalculatePath(result, path))
            {
                brain.agent.enabled = false;

            }
            if(brain.grid.FloorDistance (transform) > floorMargin)
            {
                brain.agent.enabled = false;
            }
            yield return new WaitForSeconds(0.5f);
        }
        brain.agent.enabled = false;
    }

    bool GetFarPosition(out Vector3 result)
    {
        float x, y, z;
        Vector3 positionToSample = (brain.fearVector).normalized*checkingDistance;

        NavMeshHit hit;
        if (NavMesh.SamplePosition(positionToSample, out hit, checkingDistance, brain.agent.areaMask))
        {
            result = hit.position;
            return true;
        }
        result = Vector3.zero;
        return false;
    }
}
