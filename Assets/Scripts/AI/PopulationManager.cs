﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

public class PopulationManager : MonoBehaviour
{
/*
    public GameObject animalPrefab;
    public int populationSize = 50;
    List<Brain> population = new List<Brain>();
    public static float elapsed = 0;
    public float xMax, zMax;
    public float trialTime = 5;
    int generation = 1;
    float meanTimeAliveLastGen;

    public ScriptableDNA scrDNA;

    GUIStyle guiStyle = new GUIStyle();
    void OnGUI()
    {
        guiStyle.fontSize = 25;
        guiStyle.normal.textColor = Color.white;
        GUI.BeginGroup (new Rect (10, 10, 250, 150));
        GUI.Box (new Rect (0,0,140,140), "Stats", guiStyle);
        GUI.Label(new Rect (10,25,200,30), "Gen: " + generation, guiStyle);
        GUI.Label(new Rect (10,50,200,30), string.Format("Time: {0:0.00}",elapsed), guiStyle);
        //GUI.Label(new Rect (10,75,200,30), "Population: " + population.Count, guiStyle);
        GUI.Label(new Rect (10,75,200,30), "MeanLife: " + meanTimeAliveLastGen, guiStyle);
        GUI.EndGroup ();
    }


    // Use this for initialization
    void Start () {
        for(int i = 0; i < populationSize; i++)
        {
            GameObject b = Instantiate(animalPrefab, new Vector3(Random.Range(-xMax, xMax), 0, Random.Range(-zMax, zMax)), this.transform.rotation);
            b.GetComponentInChildren<AnimalBuilder>().RandomAnimal();
            Brain brain = b.GetComponentInChildren<Brain>();
            brain.Init();
            population.Add(brain);
        }
    }

    Brain Breed(Brain parent1, Brain parent2)
    {
        Brain offspring = Instantiate(animalPrefab, new Vector3(Random.Range(-xMax, xMax), 0, Random.Range(-zMax, zMax)), this.transform.rotation).GetComponentInChildren<Brain>();
        if(Random.Range(0,50) == 1) //mutate 1 in 100
        {
            offspring.Init();
            offspring.dna.Mutate();
        }
        else
        {
            offspring.Init();
            offspring.dna.Combine(parent1.GetComponentInChildren<Brain>().dna,parent2.GetComponentInChildren<Brain>().dna);
        }
        offspring.GetComponent<AnimalBuilder>().RandomAnimal();
        return offspring;
    }

    void BreedNewPopulation()
    {
        List<Brain> sortedList = population.OrderBy(o => (o.timeAlive)).ToList();
        float totalBest = 0;
        population.Clear();
        for (int i = (int) (3*sortedList.Count / 4.0f) - 1; i < sortedList.Count - 1; i++)
        {
            totalBest += sortedList[i].timeAlive;
    		population.Add(Breed(sortedList[i], sortedList[i + 1]));
    		population.Add(Breed(sortedList[i + 1], sortedList[i]));
    		population.Add(Breed(sortedList[i], sortedList[i + 1]));
    		population.Add(Breed(sortedList[i + 1], sortedList[i]));
        }
        meanTimeAliveLastGen = totalBest / (sortedList.Count/4);
        if(generation%10 == 0)
        {
            SaveResults(sortedList[sortedList.Count -1].dna);
        }


        //destroy all parents and previous population
        for(int i = 0; i < sortedList.Count; i++)
        {
            Destroy(sortedList[i]);
        }
        generation++;

    }

    // Update is called once per frame
    void Update () {
        elapsed += Time.deltaTime;
        if(elapsed >= trialTime)
        {
            BreedNewPopulation();
            elapsed = 0;
        }
    }

    void SaveResults(BehaviourDNA dna)
    {
        scrDNA.genes = dna.genes;
        int id = Random.Range(0, 100000);
        scrDNA.id = id;
        string path = Application.persistentDataPath;
        path += "/Modelisaurio Generation " +generation.ToString() + " Fitness "+ meanTimeAliveLastGen.ToString() +  " "+ id.ToString() +".txt";
        string text = "";
        for(int i = 0; i < dna.genes.Count; i++)
        {
            for(int j = 0; j < dna.genes[i].Count; j++)
            {
                text+= "\n" + dna.genes[i][j].ToString();
            }
        }
        System.IO.File.WriteAllText(path, text);

    }
*/
}
