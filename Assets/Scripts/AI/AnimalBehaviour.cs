﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class AnimalBehaviour : MonoBehaviour {

    public bool wandering, pathfindingWander, rightIfTie;
    public int index, currentCorner;
    public float priority, changeDirectionTime = 10, timeToMove = 2, minDistance = 0.36f, detectionDistance = 1, floorMargin = 0.5f;
    public Vector3 randomDestiny;
    public Transform mouth, centerTail, centerArms;
    public NavMeshPath path;
    public GameObject indicador;
    public Brain brain;
    //
    void Awake()
    {
        path = new NavMeshPath();
        brain = GetComponent<Brain>();
    }

    public bool RandomDestiny ()
    {
        int counter = 0;

        randomDestiny = new Vector3 (Random.Range(-brain.grid.limitX/2, brain.grid.limitX/2), 0, Random.Range(-brain.grid.limitZ/2, brain.grid.limitZ/2));
        while (brain.agent.isOnNavMesh && !brain.agent.CalculatePath(randomDestiny, path) && counter < 100)
        {
            randomDestiny = new Vector3 (Random.Range(-brain.grid.limitX/2, brain.grid.limitX/2), 0, Random.Range(-brain.grid.limitZ/2, brain.grid.limitZ/2));
            counter ++;
        }
        if (counter < 100)
        {
            currentCorner = 0;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Wander()
    {
        if(brain.agent.enabled)
        {
            if(!wandering)
            {
                pathfindingWander = RandomDestiny();
                if(!pathfindingWander)
                {
                    randomDestiny = new Vector3(Random.Range (-100f, 100f), Random.Range (-100f, 100f), Random.Range (-100f, 100f));
                }
                wandering = true;
                Invoke("StopWander", changeDirectionTime);
            }
            else if (pathfindingWander && brain.agent.isOnNavMesh)
            {
                FollowPath();
            }
            else
            {
                Follow(randomDestiny);
            }
        }
        else
        {
            if(!wandering)
            {
                randomDestiny = new Vector3(Random.Range (-100f, 100f), Random.Range (-100f, 100f), Random.Range (-100f, 100f));
                wandering = true;
                Invoke("StopWander", changeDirectionTime);
            }
            else
            {
                Follow (randomDestiny);
            }

        }
    }


	public Vector3 NextDir()
	{
		if(path != null)
		{
            if(path.corners.Length > currentCorner)
            {
    			if(path.corners.Length > 1 && (path.corners[currentCorner] - transform.position).sqrMagnitude < minDistance)
    			{
    				currentCorner ++;
                    if(currentCorner >= path.corners.Length)
                    {
                        return (Vector3.zero);
                    }
    			}
        		return path.corners[currentCorner] - transform.position;

            }
            else
            {
    			return (Vector3.zero);
            }
		}
		else
		{
			return (Vector3.zero);
		}
	}

    public void Follow (Vector3 dir)
    {
        Vector3 capDir = Misc.CapVectorX(dir, brain.builder.size/125);
        brain.actions.Move(capDir);
    }
    public void Follow (Vector3 dir, bool cappedInZ)
    {
        Vector3 capDir = Misc.CapVectorX(dir, brain.builder.size/125);
        brain.actions.Move(capDir);
    }
    public bool FollowPath()
    {
        Vector3 pathdir = NextDir();
        if(pathdir != Vector3.zero)
        {
            Follow(pathdir);
            return true;
        }
        else
        {
            return false;
        }
    }
    public void Reach(Transform targ, Transform part)
    {
        float x = 0;
        Vector3 dir = targ.position - part.position;
        if(IsWithin(targ, part))
        {
            if(rightIfTie)
            {
                x = 100;
            }
            else
            {
                x = -100;
            }
        }
        else
        {
            x = dir.x;
        }
        Follow(new Vector3(x, dir.y, dir.z));
    }

    void StopWander()
    {
        wandering = false;
        int flip = Random.Range(0,2);
        if(flip == 0)
        {
            rightIfTie = true;
        }
        else
        {
            rightIfTie = false;
        }
    }

    public Vector3 OppositePostion(Vector3 position)
    {
        return position *= -10;
    }

    public bool IsWithin(Transform targ, Transform part)
    {
        float distance = Mathf.Abs(transform.parent.position.x  - part.position.x);
        float min = transform.parent.position.x - distance;
        float max = transform.parent.position.x + distance;
        if(targ.position.x < max && targ.position.x > min)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool NoFearVector(Vector3 dir)
    {

            return Misc.IntFromVector(brain.senses.foodDir) != Misc.IntFromVector(Misc.InvertVector(brain.fearVector));

    }

}
