using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class SeekFood : AnimalBehaviour {

    public bool waitingForPath = false;
    Food target;

    void FixedUpdate ()
    {
        if(brain.senses.closestFood != null)
        {
            if(target != brain.senses.closestFood)
            {

                target = brain.senses.closestFood;
                if(brain.grid.FloorDistance (target.transform) < floorMargin)
                {
                    brain.agent.enabled = true;

                    float mouthFactor = 0;
                    if(rightIfTie)
                    {
                        mouthFactor = 1;
                    }
                    else
                    {
                        mouthFactor = -1;
                    }
                    Vector3 mouthVector = new Vector3 ((float)mouthFactor * (float)brain.builder.mouthMargin, 0, 0);
            		if(brain.agent.isOnNavMesh && !brain.agent.CalculatePath(target.transform.position + mouthVector, path))
                    {
                        brain.senses.RemoveClosestFood();
                        brain.senses.FindClosestFood();
                    }
                    else if (brain.agent.isOnNavMesh)
                    {
                        currentCorner = 0;
                    }
                    else
                    {
                        brain.senses.RemoveClosestFood();
                        brain.senses.FindClosestFood();
                    }
                }
                else
                {
                    brain.agent.enabled = false;
                }

            }
            if(brain.animal.headAttack.possibleMeals.Contains(target))
            {
                brain.actions.Idle();
                brain.actions.Eat();
            }
            else if (!(brain.grid.FloorDistance (target.transform) < floorMargin))
            {
                Reach(target.transform, brain.builder.mouth);
            }
            else if(brain.agent.isOnNavMesh && FollowPath())
            {
            }
            else
            {
                Reach(target.transform, brain.builder.mouth);
            }
        }
        else
        {
            Wander();
        }
    }
}
