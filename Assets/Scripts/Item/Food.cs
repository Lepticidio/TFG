﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;

public class Food : Item
{
	public bool vegetable, meat, plankton, gotIt, noMinimap, consumed;
	public float rotRate, rebirthTime, maxNutrition, RotTimes, colorRotRate;
	public BoxCollider collider;
	public Animator animator;
	public GameObject full, bitten, minimapIcon;
	public SpriteMeshInstance rotMesh;
	public AudioSource source;
	public ParticleSystem particles, eatingParticles;
	public AudioClip interacted, eaten;

	void Start()
	{
		collider = GetComponent<BoxCollider>();
		maxNutrition = nutrition;
		RotTimes =  (maxNutrition/2)/rotRate;
		colorRotRate = 0.4f / RotTimes;
		World world = GameObject.Find("Data").GetComponent<World>();

		if(world != null && ((plankton &&  !world.player.planktivore )|| (meat && !world.player.carnivore)
			|| (hardness > world.player.herbivoreEfficacy)))
		{
			noMinimap = true;
			minimapIcon.SetActive(false);
		}

		if(rotRate > 0)
		{
			Invoke("Rot", 1);
		}
		Invoke ("GetAnimator", Random.Range(0f,1f));
	}

	void OnTriggerEnter(Collider col)
	{
		if(animator != null && (col.gameObject.tag == "Player" || col.gameObject.tag == "NPC"))
		{
			animator.SetTrigger("interaction");
			if(col.gameObject.tag == "Player")
			{
				source.clip = interacted;
				source.Play();
			}
		}
	}

	void GetAnimator()
	{
		if(!gotIt)
		{
			if(render.activeSelf)
			{
				animator = render.GetComponent<Animator>();
				gotIt = true;
			}
			else
			{
				Invoke ("GetAnimator", 1);
			}
		}
	}

	public void Eaten()
	{
		Eaten(false);
	}


	public void Eaten(bool player)
	{
		consumed = true;
		if(particles != null)
		{
			particles.Play();
		}
		if(player)
		{
			source.clip = eaten;
			source.Play();
		}
		Invoke("ReallyEaten", 0f);
	}
	public void ReallyEaten()
	{
		if(vegetable || plankton)
		{
			Invoke("ParticleRebirth", rebirthTime -0.25f);
			Invoke("Rebirth", rebirthTime);
			if(full != null)
			{
				full.SetActive(false);
			}
			minimapIcon.SetActive(false);
			if(bitten != null)
			{
				bitten.SetActive(true);
			}
			collider.enabled = false;

		}
		else
		{
			Destroy(gameObject);
		}
	}
	public void ParticleRebirth()
	{
		if(particles != null)
		{
			particles.Play();
		}
	}

	public void Rebirth()
	{
		consumed = false;
		if(full != null)
		{
			full.SetActive(true);
		}
		if(!noMinimap)
		{
			minimapIcon.SetActive(true);
		}
		if(bitten != null)
		{
			bitten.SetActive(false);
		}
		collider.enabled = true;
	}

	void Rot()
	{
		nutrition -= rotRate;
		if(nutrition < maxNutrition /2)
		{
			rotMesh.color = new Color(rotMesh.color.r - colorRotRate, rotMesh.color.g + colorRotRate, rotMesh.color.b + colorRotRate/2);
		}
		if(nutrition < 0)
		{
			Destroy(gameObject);
		}
		Invoke("Rot", 1);
	}
}
