﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData : MonoBehaviour
{
	public List<Item> items = new List<Item>();

	/*
    void Awake(){



        Item Meat = new Item ("Meat", "Food");
        Meat.nutrition = 50;
        AddItem(Meat);
		Animal.meat = Meat.gameObject;

        Item Plankton = new Item ("Plankton", "Food");
        Plankton.aquatic = true;
        Plankton.nutrition = 50;
        Plankton.millionYears = 0;
        Plankton.minTemperature = 0;
        Plankton.maxTemperature = 50;
        Plankton.minPerfectTemperature = 0;
        Plankton.maxPerfectTemperature = 35;
        Plankton.abundance = 0.12f;
        Plankton.floating = true;
        AddItem(Plankton);

        Item Seaweed = new Item ("Seaweed", "Plant");
        Seaweed.aquatic = true;
        Seaweed.nutrition = 50;
        Seaweed.millionYears = 0;
        Seaweed.minTemperature = 0;
        Seaweed.maxTemperature = 50;
        Seaweed.minPerfectTemperature = 20;
        Seaweed.maxPerfectTemperature = 35;
        Seaweed.abundance = 0.12f;
        Seaweed.hardness = 1;
        AddItem(Seaweed);

        Item Clubmoss = new Item ("Clubmoss", "Plant");
		Clubmoss.terrestrial = true;
        Clubmoss.nutrition = 50;
        Clubmoss.millionYears = 50;
        Clubmoss.minHumidity = 200;
        Clubmoss.maxHumidity = 400;
        Clubmoss.minPerfectHumidity = 200;
        Clubmoss.maxPerfectTemperature = 400;
        Clubmoss.minTemperature = 0;
        Clubmoss.maxTemperature = 35;
        Clubmoss.minPerfectTemperature = 0;
        Clubmoss.maxPerfectTemperature = 35;
        Clubmoss.abundance = 0.5f;
        Clubmoss.hardness = 1;
        AddItem(Clubmoss);
/*
        Item PineTree = new Item ("PineTree", "Plant");
		PineTree.terrestrial = true;
        PineTree.nutrition = 50;
        PineTree.millionYears = 250;
        PineTree.minHumidity = 200;
        PineTree.maxHumidity = 400;
        PineTree.minPerfectHumidity = 200;
        PineTree.maxPerfectTemperature = 400;
        PineTree.minTemperature = 0;
        PineTree.maxTemperature = 35;
        PineTree.minPerfectTemperature = 0;
        PineTree.maxPerfectTemperature = 35;
      //  PineTree.abundance = 0.03f;
        PineTree.abundance = 0.2f;
        PineTree.hardness = 99999;
        AddItem(PineTree);

        Item Fern = new Item ("Fern", "Plant");
		Fern.terrestrial = true;
        Fern.nutrition = 50;
        Fern.millionYears = 100;
        Fern.minHumidity = 100;
        Fern.maxHumidity = 400;
        Fern.minPerfectHumidity = 200;
        Fern.maxPerfectTemperature = 400;
        Fern.minTemperature = 0;
        Fern.maxTemperature = 35;
        Fern.minPerfectTemperature = 0;
        Fern.maxPerfectTemperature = 35;
        Fern.abundance = 0.5f;
        Fern.hardness = 2;
        AddItem(Fern);

        Item Cycad = new Item ("Cycad", "Plant");
		Cycad.terrestrial = true;
        Cycad.nutrition = 50;
        Cycad.millionYears = 200;
        Cycad.minHumidity = 100;
        Cycad.maxHumidity = 400;
        Cycad.minPerfectHumidity = 100;
        Cycad.maxPerfectTemperature = 400;
        Cycad.minTemperature = 25;
        Cycad.maxTemperature = 35;
        Cycad.minPerfectTemperature = 25;
        Cycad.maxPerfectTemperature = 35;
        Cycad.abundance = 0.1f;
        Cycad.hardness = 3;
        AddItem(Cycad);

        Item Grass = new Item ("Grass", "Plant");
		Grass.terrestrial = true;
        Grass.nutrition = 50;
        Grass.millionYears = 450;
        Grass.minHumidity = 0;
        Grass.maxHumidity = 200;
        Grass.minPerfectHumidity = 100;
        Grass.maxPerfectTemperature = 200;
        Grass.minTemperature = -10;
        Grass.maxTemperature = 50;
        Grass.minPerfectTemperature = 0;
        Grass.maxPerfectTemperature = 35;
        Grass.abundance = 0.5f;
        Grass.hardness = 4;
        AddItem(Grass);


    }

	public void AddItem(Item item)
	{
		items.Add(item);
		itemsGo.Add(item.gameObject);
		if(item.type == "Food")
		{
			item.gameObject.GetComponent<Food>().item = item;
		}
		else if (item.type == "Plant")
		{
			item.gameObject.GetComponent<Plant>().item = item;
		}
		else if (item.type == "Decoration")
		{
			item.gameObject.GetComponent<Decoration>().item = item;
		}
	}

	*/

}
