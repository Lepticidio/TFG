﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;

public class PlantColor : MonoBehaviour {

    Color color;
    SpriteRenderer sr;
    SpriteMeshInstance sm;
    World world;

    void Awake()
    {
        world = GameObject.Find("Controller").GetComponent<World>();

        sr = gameObject.GetComponent<SpriteRenderer>();
        if(sr==null)
        {
            sm = gameObject.GetComponent<SpriteMeshInstance>();
            color = sm.color;
        }else
        {
            color = sr.color;
        }
    }
    void Start()
    {
        UpdateColor();
    }

    void UpdateColor(){
        if(world != null)
        {
            color = ColorConversion.HSLtoRGB(ColorConversion.HSLFromRGB(color.r,color.g, color.b)[0] - (120-world.plantHue),
            ColorConversion.HSLFromRGB(color.r,color.g, color.b)[1],ColorConversion.HSLFromRGB(color.r,color.g, color.b)[2] );
            if(sr==null)
            {
                sm.color = color;
            }
            else
            {
                sr.color = color;
            }

        }
    }

}
