﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
	public string[] name = new string[2];
	public bool floating, unmoving = true, verticalFlip, horizontalFlip, finished, homogenous, notRotated, sizeChange = true,
		sizeChanged, tree, grassy;
	public int width = 1, height = 1, hardness;
	public float nutrition, millionYears, minHumidity, maxHumidity, minPerfectHumidity, maxPerfectHumidity, minTemperature,
        maxTemperature, minPerfectTemperature, maxPerfectTemperature, minAltitude, maxAltitude, minDepth, maxDepth, abundance,
		heteroffsetX, heteroffsetY, minSizeVariation = 0.5f, maxSizeVariation = 1.5f, sizeVariation = 1;
	public string type;
	public Transform center;
	public GameObject prefab, render;

	void Awake()
	{
		RandomFlip();
		if (center == null)
		{
			center = transform;
		}
		Transform renderTrans = transform.Find("Render");
		if(renderTrans != null)
		{
			render = renderTrans.gameObject;
		}

		//render.SetActive(false);
		if(sizeChange && !sizeChanged)
		{
			ChangeSize();
		}

	}

	void OnBecameVisible()
	{
		render.SetActive(true);
	}
	void OnBecameInvisible()
	{
		render.SetActive(false);
	}

	void RandomFlip()
	{
		if(!finished)
		{
			if(verticalFlip)
			{
				int flip = Random.Range(0,2);
				if (flip==1)
				{
					transform.localScale = new Vector3 (transform.localScale.x, -transform.localScale.y, transform.localScale.z);
				}
			}
			if(horizontalFlip)
			{
				int flip = Random.Range(0,2);
				if (flip==1)
				{
					transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
				}
			}
			finished = true;
		}
	}
	public void ChangeSize()
	{
		sizeVariation = Random.Range(minSizeVariation, maxSizeVariation);
		Obstacle obstacle = GetComponent<Obstacle>();
		if(obstacle != null)
		{
			obstacle.height = (int)Mathf.Ceil( (float)obstacle.height * sizeVariation);
			obstacle.width = (int)Mathf.Ceil( (float)obstacle.width *  sizeVariation);
			obstacle.altitude = (int)Mathf.Ceil( (float)obstacle.altitude *  sizeVariation);
			obstacle.BlockGrid();
		}
		nutrition *= sizeVariation;
		transform.localScale *= sizeVariation;
		sizeChanged = true;

	}


}
