﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;

public class Rock : Decoration
{
	public bool dochange = true;
	bool changed, lastChange;
	public int id;
	GameObject activeChild;

	void Start()
	{
		Invoke("CheckColor", Random.Range(0, 0.1f));
	}

	void CheckColor()
	{
		if(!lastChange)
		{
			if(dochange && !changed && render.activeSelf)
			{
				activeChild = transform.GetChild(RockGenerator.possibleInts[id]).gameObject;
				activeChild.SetActive(true);
				changed = true;
			}
			else if (changed && !lastChange)
			{
				SpriteMeshInstance mesh = activeChild.GetComponentInChildren<SpriteMeshInstance>();
				if (mesh != null)
				{
					mesh.color = RockGenerator.color;
					lastChange = true;
				}
			}
			Invoke("CheckColor", 0.1f);
		}
	}
}
