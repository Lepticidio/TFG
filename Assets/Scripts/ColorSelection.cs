﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSelection : MonoBehaviour {


	public Slider H, L, S;
	public GameObject hh, hl, hs;
	public GameObject[] Parts;
	public List <SpriteRenderer> Sprites;
	public Color color;

	// Use this for initialization
 	void Start () {
		H.maxValue = 359;
		GetParts ();
	}

	// Update is called once per frame
	void Update () {
		hh.GetComponent<Image> ().color = ColorConversion.HSLtoRGB (H.value, 0.5f, 1);
		hl.GetComponent<Image> ().color = ColorConversion.HSLtoRGB (0, L.value, 0);
		hs.GetComponent<Image> ().color = ColorConversion.HSLtoRGB (H.value, 0.5f, S.value);
		foreach (SpriteRenderer sr in Sprites){
			sr.color = ColorConversion.HSLtoRGB (H.value,L.value,S.value);
		}

	}
	void GetParts(){
		Parts = GameObject.FindGameObjectsWithTag("Color");
		foreach (GameObject go in Parts){
			Sprites.Add (go.GetComponent<SpriteRenderer>());
		}

	}
}
