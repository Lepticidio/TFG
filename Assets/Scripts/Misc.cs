﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Misc : MonoBehaviour
{
    public static bool IsVisibleToCamera(Transform transform)
    {
        Vector3 visTest = Camera.main.WorldToViewportPoint(transform.position);
        return (visTest.x >= 0 && visTest.y >= 0) && (visTest.x <= 1 && visTest.y <= 1) && visTest.z >= 0;
    }

    static public T FindClosest<T>(ref List<T> list, Transform trans, ref Vector3 dir, ref float distance) where T: MonoBehaviour
    {
        if (trans != null)
        {

            Misc.Clean<T>(ref list);
            T result = null;
            float sqrDistance  = float.PositiveInfinity;
            if(list.Count>0)
            {
                result = list[0];
                dir = result.transform.position-trans.position;
                sqrDistance = (dir).sqrMagnitude;
                for(int i = 0; i < list.Count; i++)
                {

                    Vector3 possibleDir = list[i].transform.position-trans.position;
                    float possibleDistance = (possibleDir).sqrMagnitude;
                    if(sqrDistance> possibleDistance)
                    {
                        sqrDistance = possibleDistance;
                        dir = possibleDir;
                        result = list[i];
                    }
                }
            }

            distance = sqrDistance;
            return result;

        }
        else
        {
            return null;
        }
    }
    static public void Clean<T>(ref List<T> list) where T: MonoBehaviour
    {
        for (int i = list.Count -1; i > -1; i--)
        {
            if(list[i] == null)
            {
                list.RemoveAt(i);
            }
        }
    }

    static public Vector3 InvertVector(Vector3 dir)
    {
        return new Vector3 (-dir.x, -dir.y, -dir.z);
    }

    static public Vector3 RotateBack(Vector3 dir)
    {
        return new Vector3 (-dir.x, dir.y, -dir.z);
    }

    static public Vector3 RotateLeft(Vector3 dir)
    {
        return new Vector3(-dir.z, dir.y, dir.x);
    }


    static public Vector3 RotateRight(Vector3 dir)
    {
        return new Vector3(dir.z, dir.y, -dir.x);
    }

    static public Vector3 RotateBackLeft(Vector3 dir)
    {
        return new Vector3 (-dir.x - dir.z, dir.y, -dir.z + dir.x);
    }

    static public Vector3 RotateBackRight(Vector3 dir)
    {
        return new Vector3 (-dir.x + dir.z, dir.y, -dir.z - dir.x);
    }

    static public Vector3 VectorFromInt(int n)
    {
        if (n == 0)
        {
            return new Vector3 (0, 1, 0);
        }
        else if (n == 1)
        {
            return new Vector3 (1, 1, 0);
        }
        else if (n == 2)
        {
            return new Vector3 (1, 0, 0);
        }
        else if (n == 3)
        {
            return new Vector3 (1, -1, 0);
        }
        else if (n == 4)
        {
            return new Vector3 (0, -1, 0);
        }
        else if (n == 5)
        {
            return new Vector3 (-1, -1, 0);
        }
        else if (n == 6)
        {
            return new Vector3 (-1, 0, 0);
        }
        else if (n == 7)
        {
            return new Vector3 (-1, 1, 0);
        }
        else
        {
            return new Vector3 (0, 0, 0);
        }
    }

    static public int IntFromVector(Vector3 vector)
    {
        if (vector.x == 0 && vector.z > 0)
        {
            return 0;
        }
        else if (vector.x > 0 && vector.z > 0)
        {
            return 1;
        }
        else if (vector.x > 0 && vector.z == 0)
        {
            return 2;
        }
        else if (vector.x > 0 && vector.z < 0)
        {
            return 3;
        }
        else if (vector.x == 0 && vector.z < 0)
        {
            return 4;
        }
        else if (vector.x < 0 && vector.z < 0)
        {
            return 5;
        }
        else if (vector.x < 0 && vector.z == 0)
        {
            return 6;
        }
        else if (vector.x < 0 && vector.z > 0)
        {
            return 7;
        }
        else
        {
            return 8;
        }
    }

    static public Vector3 CapVectorX (Vector3 vector, float margin)
    {
        float x = Mathf.Abs(vector.x);
        if(Mathf.Abs(vector.x) < margin)
        {
            return new Vector3 (0, vector.y, vector.z);
        }
        else
        {
            return vector;
        }
    }

    static public int IntFromVector(Vector3 vector, float margin)
    {
        if ( Mathf.Abs(vector.x) < margin && vector.z > 0)
        {
            return 0;
        }
        else if (vector.x > margin && vector.z > 0)
        {
            return 1;
        }
        else if (vector.x > margin && vector.z == 0)
        {
            return 2;
        }
        else if (vector.x > margin && vector.z < 0)
        {
            return 3;
        }
        else if (Mathf.Abs(vector.x) < margin && vector.z < 0)
        {
            return 4;
        }
        else if (vector.x < -margin && vector.z < 0)
        {
            return 5;
        }
        else if (vector.x < -margin && vector.z == 0)
        {
            return 6;
        }
        else if (vector.x < -margin && vector.z > 0)
        {
            return 7;
        }
        else
        {
            return 8;
        }
    }

    static public void LoadScene(string level)
    {
		SceneManager.LoadScene (level);
    }
    static public Color RandomColor()
    {
        return new Color (Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1f);
    }

    static public Color PseudoRandomColor()
    {
        return new Color (Random.Range(0.1f, 1f), Random.Range(0.1f, 1f), Random.Range(0.1f, 1f), 1f);
    }

    static public int IntFromVector(Vector3 vector, float margin, bool cappedInZ)
    {
        if ( Mathf.Abs(vector.x) < margin && vector.z > margin)
        {
            return 0;
        }
        else if (vector.x > margin && vector.z > margin)
        {
            return 1;
        }
        else if (vector.x > margin && Mathf.Abs(vector.z) < margin)
        {
            return 2;
        }
        else if (vector.x > margin && vector.z < - margin)
        {
            return 3;
        }
        else if (Mathf.Abs(vector.x) < margin && vector.z < - margin)
        {
            return 4;
        }
        else if (vector.x < -margin && vector.z < - margin)
        {
            return 5;
        }
        else if (vector.x < -margin && Mathf.Abs(vector.z) < margin)
        {
            return 6;
        }
        else if (vector.x < -margin && vector.z > margin)
        {
            return 7;
        }
        else
        {
            return Random.Range(0, 8);
        }
    }
    static public float[] SortByAbsoluteValue(float[] input)
    {

        int n = input.Length;
        float[] output = new float[n];
        int start = 0;
        int last = n - 1;

        while (last >= start)
        {
            n--;
            if (Mathf.Abs(input[start]) < Mathf.Abs(input[last]))
            {
                output[n] = input[start++];
            }
            else
            {

                output[n] = input[last--];
            }
        }
        return output;
    }
    public static string FirstCharToUpper(string input)
    {
        if (input == null)
            return null;

        if (input.Length > 1)
            return char.ToUpper(input[0]) + input.Substring(1);

        return input.ToUpper();
    }
}
