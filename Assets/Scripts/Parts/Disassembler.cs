﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disassembler : MonoBehaviour
{

	AnimalBuilder builder;

	public List<Transform> anchors = new List<Transform>();
	public List<Vector3> positions = new List<Vector3>();
	int index;

	public void Disassemble()
	{
		Separate(builder.headPO);
		Separate(builder.bodyPO);
		Separate(builder.tailPO);
		Separate(builder.arm1);
		Separate(builder.arm2);
		Separate(builder.leg1);
		Separate(builder.leg2);
	}

	public void Separate(PartObject part)
	{
		part.transform.SetParent(anchors[index]);
		index ++;
	}

	void Update()
	{

	}
}
