﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Body : Part {

  public Body (string animalP){
		animal = animalP;
		name[0] = animal;
		name[1] = animal;
		life = 15;
        type = "body";
		gameObject = Resources.Load<GameObject>("Prefabs/Parts/"+type+animal);
		if(!gameObject){
			animal = "None";
		}
		if(secondaryType==""){
			secondaryGameObject = Resources.Load<GameObject>("Prefabs/Parts/"+secondaryType+animal);
		}
  }


}
