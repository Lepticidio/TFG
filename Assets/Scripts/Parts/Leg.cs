﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leg : Part {

  public Leg (string animalP){
		animal = animalP;
		name[0] = animal;
		name[1] = animal;
		life = 5;
		type = "leg";
        secondaryType = "arm";
		gameObject = Resources.Load<GameObject>("Prefabs/Parts/"+type+animal);
		if(secondaryType!="")
    {
			secondaryGameObject = Resources.Load<GameObject>("Prefabs/Parts/"+secondaryType+animal);
		}
  }
}
