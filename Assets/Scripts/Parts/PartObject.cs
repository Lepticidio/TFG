﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;

public class PartObject : MonoBehaviour {

	public bool coordinated;
	public Transform headTracker, tailTracker, arm1Tracker, leg1Tracker, arm2Tracker, leg2Tracker ;
	public Bone2D front, rear, bone;
	public Part part;
	public AttackPart attackPart;
    public AudioSource source;
    public AudioClip conflict, dying;

	public void Play(bool grave)
	{
		if(grave)
		{
			source.clip = dying;
		}
		else
		{
			source.clip = conflict;
		}
		source.Play();
	}

}
