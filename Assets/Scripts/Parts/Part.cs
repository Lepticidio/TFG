﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Part
{

    public bool carnivore, planktivore;
    public int herbivore;
	public float defense, terrestrialSpeed, aquaticSpeed, energyCost, life, terrestrialSpeedFactor = 1, aquaticSpeedFactor = 1, millionYears;
	public string animal, type, secondaryType;
	public string[] name = new string[2];
	public string[] nameFrag = new string[2];
	public GameObject gameObject, secondaryGameObject;
	public Attack attack, secondaryAttack;
	public List <Part> softEvolutions = new List<Part>();
	public List <Part> evolutions = new List<Part>();

	public Part (string animalP)
	{
		animal = animalP;
		name[0] = animal;
		name[1] = animal;
		gameObject = Resources.Load<GameObject>("Prefabs/Parts/"+type+animal);
		if(secondaryType=="")
		{
			secondaryGameObject = Resources.Load<GameObject>("Prefabs/Parts/"+secondaryType+animal);
		}
	}
	public void NameFrag(string nF)
	{
		nameFrag[0] = nF;
		nameFrag[1] = nF;
	}
	public Part ()
	{
		animal = ":-)";
	}

	public string Compare(Part other)
	{
		int numberStats = 10;
		float[] changes = new float[numberStats];
		if(other.planktivore != planktivore)
		{
			if (other.planktivore)
			{
				changes[1] = 0.5f;
			}
			else
			{
				changes[1] = -0.5f;
			}
		}
		if(other.carnivore != carnivore)
		{
			if (other.carnivore)
			{
				changes[2] = 15;
			}
			else
			{
				changes[2] = -15;
			}
		}
		changes[3] = (float)(other.herbivore - herbivore)*6;
		changes[4] = other.aquaticSpeed - aquaticSpeed + other.aquaticSpeedFactor*10 - aquaticSpeedFactor*10;
		changes[5] = other.terrestrialSpeed - terrestrialSpeed + other.terrestrialSpeedFactor*10 - terrestrialSpeedFactor*10;
		changes[6] = other.defense - defense;
		changes[7] = other.MaxAttack() - MaxAttack();
		changes[8] = other.life - life;
		changes[9] = energyCost - other.energyCost;
        float maxValue = changes.Max();
        int maxIndex = changes.ToList().IndexOf(maxValue);
        string result = StatFromInt(maxIndex);
        if(result == "herbivore")
        {
            if(other.herbivore == 1)
            {
                result = "herbivore1";
            }
            else if (other.herbivore == 2)
            {
                result = "herbivore2";
            }
            else if (other.herbivore == 3)
            {
                result = "herbivore3";
            }
            else if (other.herbivore == 4)
            {
                result = "herbivore4";
            }
            else
            {
                result = "none";
            }
        }
		return result;

	}
	string StatFromInt (int stat)
	{
		if(stat == 0)
		{
			return "down";
		}
		else if (stat == 1)
		{
			return "planktivore";
		}
		else if (stat == 2)
		{
			return "carnivore";
		}
		else if (stat == 3)
		{
			return "herbivore";
		}
		else if (stat == 4)
		{
			return "aquaticSpeed";
		}
		else if (stat == 5)
		{
			return "terrestrialSpeed";
		}
		else if (stat == 6)
		{
			return "defense";
		}
		else if (stat == 7)
		{
			return "attack";
		}
		else if (stat == 8)
		{
			return "health";
		}
        else if (stat == 9)
        {
            return "stomach";
        }
        else
        {
            return "none";
        }

	}

	float MaxAttack ()
	{
		float result = 0;
		if (attack != null)
		{
			if(secondaryAttack != null)
			{
				result = Mathf.Max(attack.damage, secondaryAttack.damage);
			}
			else
			{
				result = attack.damage;
			}
		}
		else if (secondaryAttack != null)
		{
			result = secondaryAttack.damage;
		}
		return result;
	}
}
