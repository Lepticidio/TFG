﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack {

	public float damage;
	public Sprite sprite;
	public string type;
	public string[] name = new string[2];

	public Attack(float dam, string nameEnglish, string nameSpanish)
	{
		type = nameEnglish;
		name[0] = nameEnglish;
		name[1] = nameSpanish;
		damage = dam;
		sprite = Resources.Load<Sprite>("Sprites/Interface/Stats/"+type);
	}
}
