﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PartPositionSaver : MonoBehaviour {

	public AnimalBuilder builder;
	public PartData data;
	public bool changed;


	public void Create(){
		builder.UpdateAnimal();
	}

	public void DestroyAnimal(){
		builder.DestroyAnimalEdit();
	}

	public void RandomAnimal(){
		builder.DestroyAnimalEdit();
		builder.headInt= Random.Range(0, data.heads.Count);
		builder.bodyInt= Random.Range(0, data.bodies.Count);
		builder.tailInt= Random.Range(0, data.tails.Count);
		builder.legsInt= Random.Range(0, data.legs.Count);
		builder.UpdateAnimal();

	}

}
