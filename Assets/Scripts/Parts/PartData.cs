﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartData : MonoBehaviour
{
	public List<Head> heads = new List <Head>();
	public List<Body> bodies = new List <Body>();
	public List<Tail> tails = new List <Tail>();
	public List<Leg> legs = new List <Leg>();


	void Awake ()
	{
		Clear();
		Load();
		DontDestroyOnLoad(transform.gameObject);
	}


	public void Load ()
	{
		Head headHaikouichthys = CreateHead("Haikouichthys");
		headHaikouichthys.NameFrag("Hai");
		headHaikouichthys.planktivore = true;
		Body bodyHaikouichthys = CreateBody("Haikouichthys");
		bodyHaikouichthys.NameFrag("kou");
		bodyHaikouichthys.aquaticSpeed = 2;
		Leg legHaikouichthys = CreateLeg("Haikouichthys");
		legHaikouichthys.NameFrag("ich");
		legHaikouichthys.life = 0;
		Tail tailHaikouichthys = CreateTail("Haikouichthys");
		tailHaikouichthys.NameFrag("thys");
		tailHaikouichthys.aquaticSpeed = 10;
		tailHaikouichthys.terrestrialSpeedFactor = 0.5f;

		Head headHolonema = CreateHead("Holonema");
		headHolonema.NameFrag("Ho");
		headHolonema.herbivore = 2;
		headHolonema.attack = new Attack(5, "bite", "mordedura");
		Body bodyBothriolepis = CreateBody("Bothriolepis");
		bodyBothriolepis.NameFrag("thrio");
		bodyBothriolepis.defense = 10;
		Leg legBothriolepis = CreateLeg("Bothriolepis");
		legBothriolepis.NameFrag("le");
		legBothriolepis.aquaticSpeed = 1.5f;
		legBothriolepis.defense = 5;
		Tail tailApe = CreateTail("Ape");
		tailApe.NameFrag("pe");
		tailApe.life = 0;

		Head headDunkleosteus = CreateHead("Dunkleosteus");
		headDunkleosteus.NameFrag("Dun");
		headDunkleosteus.carnivore = true;
		headDunkleosteus.attack = new Attack(25, "bite", "mordedura");
		Body bodyDunkleosteus = CreateBody("Dunkleosteus");
		bodyDunkleosteus.NameFrag("kleos");
		bodyDunkleosteus.aquaticSpeedFactor = 1.25f;
		Leg legDunkleosteus = CreateLeg("Dunkleosteus");
		legDunkleosteus.NameFrag("te");
		legDunkleosteus.aquaticSpeed = 3f;
		Tail tailDunkleosteus = CreateTail("Dunkleosteus");
		tailDunkleosteus.NameFrag("us");
		tailDunkleosteus.aquaticSpeed = 13f;
		tailDunkleosteus.terrestrialSpeedFactor = 0.5f;

		Head headHylonomus = CreateHead("Hylonomus");
		headHylonomus.NameFrag("Hy");
		headHylonomus.carnivore = true;
		headHylonomus.herbivore = 1;
		headHylonomus.attack = new Attack(15, "bite", "mordedura");
		Body bodyHylonomus = CreateBody("Hylonomus");
		bodyHylonomus.NameFrag("lo");
		bodyHylonomus.terrestrialSpeedFactor = 1.25f;
		Leg legHylonomus = CreateLeg("Hylonomus");
		legHylonomus.NameFrag("no");
		legHylonomus.terrestrialSpeed = 13f;
		legHylonomus.aquaticSpeed = 8;
		legHylonomus.aquaticSpeedFactor = 0.5f;
		Tail tailHylonomus = CreateTail("Hylonomus");
		tailHylonomus.NameFrag("mus");
		tailHylonomus.aquaticSpeed = 4;
		tailHylonomus.terrestrialSpeedFactor = 1.25f;

		Head headShark = CreateHead("Shark");
		headShark.NameFrag("Me");
		headShark.name[1] = "Tiburón";
		headShark.carnivore = true;
		headShark.attack = new Attack(40, "bite", "mordedura");
		Body bodyShark = CreateBody("Shark");
		bodyShark.NameFrag("ga");
		bodyShark.name[1] = "Tiburón";
		bodyShark.aquaticSpeedFactor = 1.5f;
		Leg legShark = CreateLeg("Shark");
		legShark.NameFrag("lo");
		legShark.name[1] = "Tiburón";
		legShark.aquaticSpeed = 8f;
		Tail tailShark = CreateTail("Shark");
		tailShark.NameFrag("don");
		tailShark.name[1] = "Tiburón";
		tailShark.aquaticSpeed = 18;
		tailShark.terrestrialSpeedFactor = 0.5f;

		Head headCrocodile = CreateHead("Crocodile");
		headCrocodile.name[1] = "Cocodrilo";
		headCrocodile.NameFrag("Cro");
		headCrocodile.nameFrag[1] = "Co";
		headCrocodile.carnivore = true;
		headCrocodile.attack = new Attack(30, "bite", "mordedura");
		headCrocodile.defense = 5;
		Body bodyCrocodile = CreateBody("Crocodile");
		bodyCrocodile.name[1] = "Cocodrilo";
		bodyCrocodile.NameFrag("co");
		bodyCrocodile.nameFrag[1] = "co";
		bodyCrocodile.defense = 15;
		Leg legCrocodile = CreateLeg("Crocodile");
		legCrocodile.name[1] = "Cocodrilo";
		legCrocodile.NameFrag("di");
		legCrocodile.nameFrag[1] = "dri";
		legCrocodile.terrestrialSpeed = 12;
		legCrocodile.aquaticSpeed = 2;
		legCrocodile.defense = 5;
		Tail tailCrocodile = CreateTail("Crocodile");
		tailCrocodile.name[1] = "Cocodrilo";
		tailCrocodile.NameFrag("le");
		tailCrocodile.nameFrag[1] = "lo";
		tailCrocodile.terrestrialSpeedFactor = 1.25f;
		tailCrocodile.aquaticSpeed = 13;

		Head headStegosaurus = CreateHead("Stegosaurus");
		headStegosaurus.NameFrag("Ste");
		headStegosaurus.herbivore = 3;
		headStegosaurus.attack = new Attack(5, "bite", "mordedura");
		Body bodyStegosaurus = CreateBody("Stegosaurus");
		bodyStegosaurus.NameFrag("go");
		bodyStegosaurus.life = 25;
		bodyStegosaurus.aquaticSpeedFactor = 0.8f;
		Leg legStegosaurus = CreateLeg("Stegosaurus");
		legStegosaurus.NameFrag("sau");
		legStegosaurus.terrestrialSpeed = 13;
		legStegosaurus.aquaticSpeed = 8;
		legStegosaurus.aquaticSpeedFactor = 0.5f;
		legStegosaurus.life = 10;
		Tail tailStegosaurus = CreateTail("Stegosaurus");
		tailStegosaurus.NameFrag("rus");
		tailStegosaurus.attack = new Attack(40, "lash", "coletazo");
		tailStegosaurus.aquaticSpeed = 4f;

		Head headRat = CreateHead("Rat");
		headRat.name[1] = "Rata";
		headRat.NameFrag("Ra");
		headRat.carnivore = true;
		headRat.herbivore = 2;
		headRat.attack = new Attack(20, "bite", "mordedura");
		Body bodyRat = CreateBody("Rat");
		bodyRat.name[1] = "Rata";
		bodyRat.NameFrag("ra");
		bodyRat.terrestrialSpeedFactor = 1.5f;
		Leg legRat = CreateLeg("Rat");
		legRat.name[1] = "Rata";
		legRat.NameFrag("t");
		legRat.terrestrialSpeed = 18;
		legRat.aquaticSpeed = 8;
		legRat.aquaticSpeedFactor = 0.5f;
		Tail tailRat = CreateTail("Rat");
		tailRat.name[1] = "Rata";
		tailRat.NameFrag("t");
		tailRat.nameFrag[1] = "a";
		tailRat.terrestrialSpeedFactor = 1.5f;

		Head headSailfish = CreateHead("Sailfish");
		headSailfish.name[1] = "Pez vela";
		headSailfish.NameFrag("Sa");
		headSailfish.nameFrag[1] = "Pez ";
		headSailfish.carnivore = true;
		headSailfish.attack = new Attack(40, "bite", "mordedura");
		headSailfish.aquaticSpeedFactor = 1.25f;
		Body bodySailfish = CreateBody("Sailfish");
		bodySailfish.name[1] = "Pez vela";
		bodySailfish.NameFrag("il");
		bodySailfish.nameFrag[1] = "ve";
		bodySailfish.aquaticSpeedFactor = 1.7f;
		Leg legSailfish = CreateLeg("Sailfish");
		legSailfish.name[1] = "Pez vela";
		legSailfish.NameFrag("f");
		legSailfish.nameFrag[1] = "l";
		legSailfish.aquaticSpeed = 18f;
		Tail tailSailfish = CreateTail("Sailfish");
		tailSailfish.name[1] = "Pez vela";
		tailSailfish.NameFrag("ish");
		tailSailfish.nameFrag[1] = "a";
		tailSailfish.aquaticSpeed = 28;
		tailSailfish.terrestrialSpeedFactor = 0.5f;

		Head headAnkylosaurus = CreateHead("Ankylosaurus");
		headAnkylosaurus.NameFrag("An");
		headAnkylosaurus.herbivore = 2;
		headAnkylosaurus.attack =new Attack(5, "bite", "mordedura");
		headAnkylosaurus.defense = 10;
		Body bodyAnkylosaurus = CreateBody("Ankylosaurus");
		bodyAnkylosaurus.NameFrag("ky");
		bodyAnkylosaurus.defense = 15;
		Leg legAnkylosaurus = CreateLeg("Ankylosaurus");
		legAnkylosaurus.NameFrag("lo");
		legAnkylosaurus.terrestrialSpeed = 13;
		legAnkylosaurus.aquaticSpeed = 8f;
		legAnkylosaurus.aquaticSpeedFactor = 0.5f;
		legAnkylosaurus.defense = 10;
		Tail tailAnkylosaurus = CreateTail("Ankylosaurus");
		tailAnkylosaurus.NameFrag("saurus");
		tailAnkylosaurus.attack = new Attack(60, "lash", "coletazo");
		tailAnkylosaurus.aquaticSpeed = 4;

		Head headTyrannosaurus = CreateHead("Tyrannosaurus");
		headTyrannosaurus.NameFrag("Ty");
		headTyrannosaurus.carnivore = true;
		headTyrannosaurus.attack =new Attack(60, "bite", "mordedura");
		Body bodyTyrannosaurus = CreateBody("Tyrannosaurus");
		bodyTyrannosaurus.NameFrag("ran");
		bodyTyrannosaurus.life = 20;
		bodyTyrannosaurus.terrestrialSpeedFactor = 1.5f;
		Leg legTyrannosaurus = CreateLeg("Tyrannosaurus");
		legTyrannosaurus.NameFrag("nosa");
		legTyrannosaurus.terrestrialSpeed = 18;
		legTyrannosaurus.aquaticSpeed = 8;
		legTyrannosaurus.aquaticSpeedFactor = 0.5f;
		legTyrannosaurus.life = 10;
		Tail tailTyrannosaurus = CreateTail("Tyrannosaurus");
		tailTyrannosaurus.NameFrag("urus");
		tailTyrannosaurus.terrestrialSpeedFactor = 1.7f;

		Head headGazelle = CreateHead("Gazelle");
		headGazelle.name[1] = "Gacela";
		headGazelle.NameFrag("Ga");
		headGazelle.herbivore = 4;
		headGazelle.attack = new Attack(10, "bite", "mordedura");
		Body bodyGazelle = CreateBody("Gazelle");
		bodyGazelle.name[1] = "Gacela";
		bodyGazelle.NameFrag("ze");
		bodyGazelle.nameFrag[1] = "ce";
		bodyGazelle.terrestrialSpeedFactor = 1.7f;
		Leg legGazelle = CreateLeg("Gazelle");
		legGazelle.name[1] = "Gacela";
		legGazelle.NameFrag("l");
		legGazelle.terrestrialSpeed = 28;
		legGazelle.aquaticSpeedFactor = 0.4f;
		Tail tailGazelle = CreateTail("Gazelle");
		tailGazelle.name[1] = "Gacela";
		tailGazelle.NameFrag("le");
		tailGazelle.nameFrag[1] = "a";
		tailGazelle.terrestrialSpeedFactor = 1.5f;
		tailGazelle.life = 0;

		Head headMonkey = CreateHead("Monkey");
		headMonkey.name[1] = "Mono";
		headMonkey.NameFrag("Mo");
		headMonkey.carnivore = true;
		headMonkey.herbivore = 3;
		headMonkey.attack = new Attack(25, "bite", "mordedura");
		Body bodyMonkey = CreateBody("Monkey");
		bodyMonkey.name[1] = "Mono";
		bodyMonkey.NameFrag("n");
		bodyMonkey.nameFrag[1] = "a";
		bodyMonkey.life = 25;
		Leg legMonkey = CreateLeg("Monkey");
		legMonkey.name[1] = "Mono";
		legMonkey.NameFrag("ke");
		legMonkey.nameFrag[1] = "n";
		legMonkey.terrestrialSpeed = 13;
		legMonkey.aquaticSpeedFactor = 0.5f;
		legMonkey.aquaticSpeed = 2;
		legMonkey.secondaryAttack = new Attack(40, "punch", "puñetazo");
		Tail tailMonkey = CreateTail("Monkey");
		tailMonkey.name[1] = "Mono";
		tailMonkey.NameFrag("y");
		tailMonkey.nameFrag[1] = "o";
		tailMonkey.terrestrialSpeedFactor = 1.5f;
		tailMonkey.life = 10;


		headHaikouichthys.evolutions.Add(headHolonema);
		headHaikouichthys.evolutions.Add(headDunkleosteus);
		headHaikouichthys.evolutions.Add(headHylonomus);

		headHolonema.softEvolutions.Add(headDunkleosteus);
		headHolonema.softEvolutions.Add(headHylonomus);
		headHolonema.evolutions.Add(headShark);
		headHolonema.evolutions.Add(headCrocodile);
		headHolonema.evolutions.Add(headStegosaurus);
		headHolonema.evolutions.Add(headRat);

		headDunkleosteus.softEvolutions.Add(headHolonema);
		headDunkleosteus.softEvolutions.Add(headHylonomus);
		headDunkleosteus.evolutions.Add(headShark);
		headDunkleosteus.evolutions.Add(headCrocodile);
		headDunkleosteus.evolutions.Add(headStegosaurus);
		headDunkleosteus.evolutions.Add(headRat);

		headHylonomus.softEvolutions.Add(headHolonema);
		headHylonomus.softEvolutions.Add(headDunkleosteus);
		headHylonomus.evolutions.Add(headShark);
		headHylonomus.evolutions.Add(headCrocodile);
		headHylonomus.evolutions.Add(headStegosaurus);
		headHylonomus.evolutions.Add(headRat);

		headShark.softEvolutions.Add(headCrocodile);
		headShark.softEvolutions.Add(headStegosaurus);
		headShark.softEvolutions.Add(headRat);
		headShark.evolutions.Add(headSailfish);
		headShark.evolutions.Add(headTyrannosaurus);

		headCrocodile.softEvolutions.Add(headShark);
		headCrocodile.softEvolutions.Add(headStegosaurus);
		headCrocodile.softEvolutions.Add(headRat);
		headCrocodile.evolutions.Add(headAnkylosaurus);
		headCrocodile.evolutions.Add(headTyrannosaurus);

		headStegosaurus.softEvolutions.Add(headShark);
		headStegosaurus.softEvolutions.Add(headCrocodile);
		headStegosaurus.softEvolutions.Add(headRat);
		headStegosaurus.evolutions.Add(headSailfish);
		headStegosaurus.evolutions.Add(headAnkylosaurus);
		headStegosaurus.evolutions.Add(headGazelle);

		headRat.softEvolutions.Add(headShark);
		headRat.softEvolutions.Add(headCrocodile);
		headRat.softEvolutions.Add(headStegosaurus);
		headRat.evolutions.Add(headGazelle);
		headRat.evolutions.Add(headMonkey);

		headSailfish.softEvolutions.Add(headAnkylosaurus);
		headSailfish.softEvolutions.Add(headTyrannosaurus);

		headAnkylosaurus.softEvolutions.Add(headSailfish);
		headAnkylosaurus.softEvolutions.Add(headTyrannosaurus);

		headTyrannosaurus.softEvolutions.Add(headSailfish);
		headTyrannosaurus.softEvolutions.Add(headAnkylosaurus);

		headGazelle.softEvolutions.Add(headAnkylosaurus);
		headGazelle.softEvolutions.Add(headMonkey);

		headMonkey.softEvolutions.Add(headTyrannosaurus);
		headMonkey.softEvolutions.Add(headGazelle);


		bodyHaikouichthys.evolutions.Add(bodyBothriolepis);
		bodyHaikouichthys.evolutions.Add(bodyDunkleosteus);
		bodyHaikouichthys.evolutions.Add(bodyHylonomus);

		bodyBothriolepis.softEvolutions.Add(bodyDunkleosteus);
		bodyBothriolepis.softEvolutions.Add(bodyHylonomus);
		bodyBothriolepis.evolutions.Add(bodyShark);
		bodyBothriolepis.evolutions.Add(bodyCrocodile);
		bodyBothriolepis.evolutions.Add(bodyStegosaurus);
		bodyBothriolepis.evolutions.Add(bodyRat);

		bodyDunkleosteus.softEvolutions.Add(bodyBothriolepis);
		bodyDunkleosteus.softEvolutions.Add(bodyHylonomus);
		bodyDunkleosteus.evolutions.Add(bodyShark);
		bodyDunkleosteus.evolutions.Add(bodyCrocodile);
		bodyDunkleosteus.evolutions.Add(bodyStegosaurus);
		bodyDunkleosteus.evolutions.Add(bodyRat);

		bodyHylonomus.softEvolutions.Add(bodyBothriolepis);
		bodyHylonomus.softEvolutions.Add(bodyDunkleosteus);
		bodyHylonomus.evolutions.Add(bodyShark);
		bodyHylonomus.evolutions.Add(bodyCrocodile);
		bodyHylonomus.evolutions.Add(bodyStegosaurus);
		bodyHylonomus.evolutions.Add(bodyRat);

		bodyShark.softEvolutions.Add(bodyCrocodile);
		bodyShark.softEvolutions.Add(bodyStegosaurus);
		bodyShark.softEvolutions.Add(bodyRat);
		bodyShark.evolutions.Add(bodySailfish);
		bodyShark.evolutions.Add(bodyAnkylosaurus);
		bodyShark.evolutions.Add(bodyTyrannosaurus);
		bodyShark.evolutions.Add(bodyGazelle);
		bodyShark.evolutions.Add(bodyMonkey);

		bodyCrocodile.softEvolutions.Add(bodyShark);
		bodyCrocodile.softEvolutions.Add(bodyStegosaurus);
		bodyCrocodile.softEvolutions.Add(bodyRat);
		bodyCrocodile.evolutions.Add(bodySailfish);
		bodyCrocodile.evolutions.Add(bodyAnkylosaurus);
		bodyCrocodile.evolutions.Add(bodyTyrannosaurus);
		bodyCrocodile.evolutions.Add(bodyGazelle);
		bodyCrocodile.evolutions.Add(bodyMonkey);

		bodyStegosaurus.softEvolutions.Add(bodyShark);
		bodyStegosaurus.softEvolutions.Add(bodyCrocodile);
		bodyStegosaurus.softEvolutions.Add(bodyRat);
		bodyStegosaurus.evolutions.Add(bodySailfish);
		bodyStegosaurus.evolutions.Add(bodyAnkylosaurus);
		bodyStegosaurus.evolutions.Add(bodyTyrannosaurus);
		bodyStegosaurus.evolutions.Add(bodyGazelle);
		bodyStegosaurus.evolutions.Add(bodyMonkey);

		bodyRat.softEvolutions.Add(bodyShark);
		bodyRat.softEvolutions.Add(bodyCrocodile);
		bodyRat.softEvolutions.Add(bodyStegosaurus);
		bodyRat.evolutions.Add(bodySailfish);
		bodyRat.evolutions.Add(bodyAnkylosaurus);
		bodyRat.evolutions.Add(bodyTyrannosaurus);
		bodyRat.evolutions.Add(bodyGazelle);
		bodyRat.evolutions.Add(bodyMonkey);

		bodySailfish.softEvolutions.Add(bodyAnkylosaurus);
		bodySailfish.softEvolutions.Add(bodyTyrannosaurus);
		bodySailfish.softEvolutions.Add(bodyGazelle);
		bodySailfish.softEvolutions.Add(bodyMonkey);

		bodyAnkylosaurus.softEvolutions.Add(bodySailfish);
		bodyAnkylosaurus.softEvolutions.Add(bodyTyrannosaurus);
		bodyAnkylosaurus.softEvolutions.Add(bodyGazelle);
		bodyAnkylosaurus.softEvolutions.Add(bodyMonkey);

		bodyTyrannosaurus.softEvolutions.Add(bodySailfish);
		bodyTyrannosaurus.softEvolutions.Add(bodyAnkylosaurus);
		bodyTyrannosaurus.softEvolutions.Add(bodyGazelle);
		bodyTyrannosaurus.softEvolutions.Add(bodyMonkey);

		bodyGazelle.softEvolutions.Add(bodySailfish);
		bodyGazelle.softEvolutions.Add(bodyAnkylosaurus);
		bodyGazelle.softEvolutions.Add(bodyTyrannosaurus);
		bodyGazelle.softEvolutions.Add(bodyMonkey);

		bodyMonkey.softEvolutions.Add(bodySailfish);
		bodyMonkey.softEvolutions.Add(bodyAnkylosaurus);
		bodyMonkey.softEvolutions.Add(bodyTyrannosaurus);
		bodyMonkey.softEvolutions.Add(bodyGazelle);


		legHaikouichthys.evolutions.Add(legBothriolepis);
		legHaikouichthys.evolutions.Add(legDunkleosteus);

		legBothriolepis.softEvolutions.Add(legDunkleosteus);
		legBothriolepis.softEvolutions.Add(legHylonomus);
		legBothriolepis.evolutions.Add(legShark);

		legDunkleosteus.softEvolutions.Add(legBothriolepis);
		legDunkleosteus.softEvolutions.Add(legHylonomus);
		legDunkleosteus.evolutions.Add(legShark);

		legHylonomus.softEvolutions.Add(legBothriolepis);
		legHylonomus.softEvolutions.Add(legDunkleosteus);
		legHylonomus.evolutions.Add(legShark);
		legHylonomus.evolutions.Add(legCrocodile);
		legHylonomus.evolutions.Add(legStegosaurus);
		legHylonomus.evolutions.Add(legRat);

		legShark.softEvolutions.Add(legCrocodile);
		legShark.evolutions.Add(legSailfish);

		legCrocodile.softEvolutions.Add(legShark);
		legCrocodile.softEvolutions.Add(legStegosaurus);
		legCrocodile.softEvolutions.Add(legRat);
		legCrocodile.evolutions.Add(legSailfish);
		legCrocodile.evolutions.Add(legAnkylosaurus);
		legCrocodile.evolutions.Add(legTyrannosaurus);
		legCrocodile.evolutions.Add(legGazelle);
		legCrocodile.evolutions.Add(legMonkey);

		legStegosaurus.softEvolutions.Add(legCrocodile);
		legStegosaurus.softEvolutions.Add(legRat);
		legStegosaurus.evolutions.Add(legSailfish);
		legStegosaurus.evolutions.Add(legAnkylosaurus);
		legStegosaurus.evolutions.Add(legTyrannosaurus);
		legStegosaurus.evolutions.Add(legGazelle);
		legStegosaurus.evolutions.Add(legMonkey);

		legRat.softEvolutions.Add(legCrocodile);
		legRat.softEvolutions.Add(legStegosaurus);
		legRat.evolutions.Add(legAnkylosaurus);
		legRat.evolutions.Add(legTyrannosaurus);
		legRat.evolutions.Add(legGazelle);
		legRat.evolutions.Add(legMonkey);

		legSailfish.softEvolutions.Add(legCrocodile);

		legAnkylosaurus.softEvolutions.Add(legSailfish);
		legAnkylosaurus.softEvolutions.Add(legTyrannosaurus);
		legAnkylosaurus.softEvolutions.Add(legGazelle);
		legAnkylosaurus.softEvolutions.Add(legMonkey);

		legTyrannosaurus.softEvolutions.Add(legSailfish);
		legTyrannosaurus.softEvolutions.Add(legAnkylosaurus);
		legTyrannosaurus.softEvolutions.Add(legGazelle);
		legTyrannosaurus.softEvolutions.Add(legMonkey);

		legGazelle.softEvolutions.Add(legSailfish);
		legGazelle.softEvolutions.Add(legAnkylosaurus);
		legGazelle.softEvolutions.Add(legTyrannosaurus);
		legGazelle.softEvolutions.Add(legMonkey);

		legMonkey.softEvolutions.Add(legSailfish);
		legMonkey.softEvolutions.Add(legAnkylosaurus);
		legMonkey.softEvolutions.Add(legTyrannosaurus);
		legMonkey.softEvolutions.Add(legGazelle);


		tailHaikouichthys.evolutions.Add(tailDunkleosteus);
		tailHaikouichthys.evolutions.Add(tailHylonomus);

		tailApe.evolutions.Add(tailDunkleosteus);
		tailApe.evolutions.Add(tailHylonomus);

		tailDunkleosteus.softEvolutions.Add(tailHylonomus);
		tailDunkleosteus.evolutions.Add(tailShark);
		tailDunkleosteus.evolutions.Add(tailCrocodile);
		tailDunkleosteus.evolutions.Add(tailStegosaurus);
		tailDunkleosteus.evolutions.Add(tailRat);

		tailHylonomus.softEvolutions.Add(tailDunkleosteus);
		tailHylonomus.evolutions.Add(tailShark);
		tailHylonomus.evolutions.Add(tailCrocodile);
		tailHylonomus.evolutions.Add(tailStegosaurus);
		tailHylonomus.evolutions.Add(tailRat);

		tailShark.softEvolutions.Add(tailCrocodile);
		tailShark.softEvolutions.Add(tailStegosaurus);
		tailShark.evolutions.Add(tailSailfish);
		tailShark.evolutions.Add(tailAnkylosaurus);

		tailCrocodile.softEvolutions.Add(tailShark);
		tailCrocodile.softEvolutions.Add(tailStegosaurus);
		tailCrocodile.softEvolutions.Add(tailRat);
		tailCrocodile.evolutions.Add(tailSailfish);
		tailCrocodile.evolutions.Add(tailAnkylosaurus);
		tailCrocodile.evolutions.Add(tailTyrannosaurus);
		tailCrocodile.evolutions.Add(tailGazelle);
		tailCrocodile.evolutions.Add(tailMonkey);

		tailStegosaurus.softEvolutions.Add(tailShark);
		tailStegosaurus.softEvolutions.Add(tailCrocodile);
		tailStegosaurus.softEvolutions.Add(tailRat);
		tailStegosaurus.evolutions.Add(tailSailfish);
		tailStegosaurus.evolutions.Add(tailAnkylosaurus);
		tailStegosaurus.evolutions.Add(tailTyrannosaurus);
		tailStegosaurus.evolutions.Add(tailGazelle);
		tailStegosaurus.evolutions.Add(tailMonkey);

		tailRat.softEvolutions.Add(tailCrocodile);
		tailRat.softEvolutions.Add(tailStegosaurus);
		tailRat.evolutions.Add(tailAnkylosaurus);
		tailRat.evolutions.Add(tailTyrannosaurus);
		tailRat.evolutions.Add(tailGazelle);
		tailRat.evolutions.Add(tailMonkey);

		tailSailfish.softEvolutions.Add(tailAnkylosaurus);

		tailAnkylosaurus.softEvolutions.Add(tailSailfish);
		tailAnkylosaurus.softEvolutions.Add(tailTyrannosaurus);
		tailAnkylosaurus.softEvolutions.Add(tailGazelle);
		tailAnkylosaurus.softEvolutions.Add(tailMonkey);

		tailTyrannosaurus.softEvolutions.Add(tailAnkylosaurus);
		tailTyrannosaurus.softEvolutions.Add(tailGazelle);
		tailTyrannosaurus.softEvolutions.Add(tailMonkey);

		tailGazelle.softEvolutions.Add(tailAnkylosaurus);
		tailGazelle.softEvolutions.Add(tailTyrannosaurus);
		tailGazelle.softEvolutions.Add(tailMonkey);

		tailMonkey.softEvolutions.Add(tailApe);
		tailMonkey.softEvolutions.Add(tailAnkylosaurus);
		tailMonkey.softEvolutions.Add(tailTyrannosaurus);
		tailMonkey.softEvolutions.Add(tailGazelle);


	}

	public int IndexOfPart(Part part)
	{
		if(part.type == "head")
		{
			return heads.IndexOf(part as Head);
		}
		else if(part.type == "body")
		{
			return bodies.IndexOf(part as Body);
		}
		else if(part.type == "tail")
		{
			return tails.IndexOf(part as Tail);
		}
		else
		{
			return legs.IndexOf(part as Leg);
		}

	}
	public Part PartFromIndex(string type, int index)
	{
		if(type == "head")
		{
			return heads[index];
		}
		else if(type == "body")
		{
			return bodies[index];
		}
		else if(type == "tail")
		{
			return tails[index];
		}
		else
		{
			return legs[index];
		}

	}


	public void Clear()
	{

		heads.Clear();
		bodies.Clear();
		legs.Clear();
		tails.Clear();


	}

	Head CreateHead (string animal)
	{
		Head newPart = new Head (animal);
		heads.Add(newPart);
		return newPart;
	}

	Body CreateBody (string animal)
	{
		Body newPart = new Body (animal);
		bodies.Add(newPart);
		return newPart;
	}

	Tail CreateTail (string animal)
	{
		Tail newPart = new Tail (animal);
		tails.Add(newPart);
		return newPart;
	}

	Leg CreateLeg (string animal)
	{
		Leg newPart = new Leg (animal);
		legs.Add(newPart);
		return newPart;
	}
}
