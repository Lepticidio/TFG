﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPart : MonoBehaviour {

	public bool attacking;
	public List<Animal> possibleVictims = new List <Animal>();
	public List<Food> possibleMeals = new List <Food>();
	public Attack attack;
	public Species species;
	public AudioClip clip;

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "NPC"||col.gameObject.tag == "Player")
		{
			possibleVictims.Add(col.gameObject.GetComponent<Animal>());
		}
		//MIrar también aquí
		else if((col.gameObject.tag == "Meat" && species.carnivore) ||( col.gameObject.tag == "Plankton" && species.planktivore))
		{
			possibleMeals.Add(col.gameObject.GetComponentInParent<Food>());
		}
		//Mirar aquí
		else if(col.gameObject.tag == "Vegetable" && species.herbivore)
		{
			Food veggy = col.gameObject.GetComponent<Food>();
			if(species.herbivoreEfficacy >= veggy.hardness)
			{
				possibleMeals.Add(veggy);
			}
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag == "NPC"||col.gameObject.tag == "Player")
		{
			possibleVictims.Remove(col.gameObject.GetComponent<Animal>());
		}
		else if(col.gameObject.tag == "Meat" || col.gameObject.tag == "Plankton" || col.gameObject.tag == "Vegetable")
		{
			possibleMeals.Remove(col.gameObject.GetComponentInParent<Food>());
		}
	}
	public void Attack()
	{
		Attack(false);
	}
	public void Attack(bool playerAttacking)
	{
		foreach(Animal victim in possibleVictims)
		{
			attacking = true;
			if(playerAttacking)
			{
				victim.attackedByPlayer = true;
			}
			victim.RecieveAttack(attack, clip);
			Invoke("FinishAttack", 1f);
		}
	}
	void Update()
	{
		Misc.Clean<Food>(ref possibleMeals);
		Misc.Clean<Animal>(ref possibleVictims);
	}
	void FinishAttack()
	{
		attacking = false;
	}
}
