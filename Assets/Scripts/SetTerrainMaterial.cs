﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTerrainMaterial : MonoBehaviour {

	bool isDone;
	public Material material;
	// Use this for initialization
	void Update ()
	{
		if(!isDone)
		{
			GetComponent<Terrain>().materialTemplate = material;
			isDone = true;
			Debug.Log("Yeah");
		}
	}
}
