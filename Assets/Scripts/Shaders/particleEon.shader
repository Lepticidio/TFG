// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Eon/particlesEon" {
Properties {
    _TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
    _MainTex ("Particle Texture", 2D) = "white" {}
    _InvFade ("Soft Particles Factor", Range(0.01,3.0)) = 1.0
	_CamOffset ("Camera offset", Vector) = (0, 3, 3)

	[Space(40)]
	_MinUnderwaterTint ("Min Underwater Tint", Range(0, 1)) = 0.35
	_MinUnderwaterFog ("Min Underwater Fog", Float) = 7
	_MaxUnderwaterFog ("Max Underwater Fog", Float) = 25

	[Space(20)]
	_WaterColor ("Water color", Color) = (0.2078431, 0.454902, 0.6980392, 1)
	_WaterTex("Water texture", 2D) = "white" {}
	_Tiling ("Water tiling", Vector) = (0.3, 0.3, 0, 0)
	_TextureVisibility ("Texture visibility", Range(0, 1)) = 0.276

	[Space(20)]
	_DistTex ("Distortion", 2D) = "white" {}
	_DistTiling ("Distortion tiling", Vector) = (0.7, 0.7, 0, 0)

	[Space(20)]
	//_DeepColor ("Water deep color", Color) = (1, 1, 1, 1)
	_WaterHeight ("Water height", Float) = -0.1
	_WaterDeep ("Water deep", Float) = 22.9
	_WaterDepth ("Water depth param", Range(0, 0.1)) = 0.0172
	_WaterMinAlpha ("Water min alpha", Range(0, 1)) = 0.552

	[Space(20)]
	_BorderColor ("Border color", Color) = (1, 1, 1, 0.6196079)
	_BorderWidth ("Border width", Range(0, 1)) = 0.061

	[Space(20)]
	_MoveDirection ("Direction", Vector) = (-0.7, 0, 0.3, 0)
}

Category {
    Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
    Blend SrcAlpha OneMinusSrcAlpha
    ColorMask RGB
    Cull Off Lighting Off ZWrite Off

    SubShader {
        Pass {

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_particles
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            fixed4 _TintColor;
            fixed3 _CamOffset;

			float _MinUnderwaterTint;
			float _MinUnderwaterFog;
			float _MaxUnderwaterFog;

			sampler2D _WaterTex;
			fixed2 _Tiling;
			fixed4 _WaterColor;

			sampler2D _DistTex;
			fixed2 _DistTiling;

			fixed4 _DeepColor;
			fixed _WaterHeight;
			fixed _TextureVisibility;
			fixed _WaterDeep;
			fixed _WaterDepth;
			fixed _WaterMinAlpha;

			fixed4 _BorderColor;
			fixed _BorderWidth;
			fixed _BorderVisibility;

			fixed3 _MoveDirection;

            struct appdata_t {
                float4 vertex : POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				fixed4 worldPos : TEXCOORD1;
				fixed camHeightOverWater : TEXCOORD2;
				fixed waterDepth : TEXCOORD3;
				UNITY_FOG_COORDS(4)
                #ifdef SOFTPARTICLES_ON
                float4 projPos : TEXCOORD5;
                #endif
                UNITY_VERTEX_OUTPUT_STEREO
            };


			float GetCamHeight()
			{
				return _WorldSpaceCameraPos.y - _WaterHeight;
			}

			fixed2 WaterPlaneUV(fixed3 worldPos, fixed camHeightOverWater)
			{
				fixed3 camToWorldRay = worldPos - _WorldSpaceCameraPos;
				fixed3 rayToWaterPlane = (camHeightOverWater / camToWorldRay.y * camToWorldRay);
				return rayToWaterPlane.xz - _WorldSpaceCameraPos.xz;
			}

            float4 _MainTex_ST;

            v2f vert (appdata_t v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                #ifdef SOFTPARTICLES_ON
                o.projPos = ComputeScreenPos (o.vertex);
                COMPUTE_EYEDEPTH(o.projPos.z);
                #endif
                o.color = v.color * _TintColor;


				o.worldPos = mul(UNITY_MATRIX_M, v.vertex);

				fixed3 camToWorldRay = o.worldPos - _WorldSpaceCameraPos;
				o.camHeightOverWater = _WorldSpaceCameraPos.y - _WaterHeight;

				fixed3 rayToWaterPlane = o.camHeightOverWater / (-camToWorldRay.y) * camToWorldRay;
				fixed depth = length(camToWorldRay - rayToWaterPlane);
				o.waterDepth = depth * _WaterDepth * saturate(rayToWaterPlane.y - camToWorldRay.y);;





                o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            UNITY_DECLARE_DEPTH_TEXTURE(_CameraDepthTexture);
            float _InvFade;

            fixed4 frag (v2f i) : SV_Target
            {
                #ifdef SOFTPARTICLES_ON
                float sceneZ = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
                float partZ = i.projPos.z;
                float fade = saturate (_InvFade * (sceneZ-partZ));
                i.color.a *= fade;
                #endif

                fixed4 col = 2.0f * i.color * tex2D(_MainTex, i.texcoord);
                col.a = saturate(col.a); // alpha should not have double-brightness applied to it, but we can't fix that legacy behavior without breaking everyone's effects, so instead clamp the output to get sensible HDR behavior (case 967476)

				float isCamAbove = smoothstep(0, _CamOffset.y + _WaterHeight, _WorldSpaceCameraPos.y - _WaterHeight);

				fixed lengthUnderWater = max(0, _WaterHeight - i.worldPos.y);
				fixed underWater = lerp(0, 1, lengthUnderWater > 0);
				fixed borderAlpha = lerp(underWater * _BorderColor.a, 0, saturate(lengthUnderWater / _BorderWidth));
				fixed waterAlpha = saturate(lengthUnderWater / _WaterDeep + _WaterMinAlpha + i.waterDepth);


				fixed2 water_uv = WaterPlaneUV(i.worldPos, i.camHeightOverWater);
				fixed4 distortion = tex2D(_DistTex, water_uv * _DistTiling) * 2 - 1;
				fixed2 distorted_uv = ((water_uv + distortion.rg) - _Time.y * _MoveDirection.xz) * _Tiling;

				fixed4 waterCol = tex2D(_WaterTex, distorted_uv)*isCamAbove;
				waterCol = lerp(_WaterColor, fixed4(1, 1, 1, 1), waterCol.r * _TextureVisibility);

				float camDist = distance(i.worldPos, _WorldSpaceCameraPos);
				float normalizedDist = min(1, max(_MinUnderwaterTint, step(_MinUnderwaterFog, camDist)*(camDist/_MaxUnderwaterFog)));


				float waterAmount = (isCamAbove*underWater  + (1- isCamAbove)*normalizedDist)* waterAlpha*_WaterColor.a;
				col.rgb = lerp(col.rgb, waterCol.rgb, waterAmount);
				col.rgb = lerp(col.rgb, _BorderColor.rgb, borderAlpha);



                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
}
