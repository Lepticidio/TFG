﻿Shader "Miguel/ShaderMiguelConNegro"
{
    Properties
    {
        _ShadowColor ("Shadow Color", Color) = (0.5,0.0,0.75)
    }

    SubShader
    {
      		GrabPass{
            		Tags
            		{
            			"Queue" = "Transparent"
            		}
          }


          Pass
          {
            Tags {"LightMode"="ForwardBase"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
        		float4 uvgrab : TEXCOORD1;
                fixed4 diff : COLOR0;
                float4 pos : SV_POSITION;
                SHADOW_COORDS(2)
            };
            float3 _ShadowColor;
      			sampler2D _GrabTexture;
      			float4 _GrabTexture_TexelSize;

            v2f vert (appdata v)
            {



                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;
                TRANSFER_SHADOW(o)

        			  //change these ---------
        			  #if UNITY_UV_STARTS_AT_TOP
        			    float scale = -1.0;
        			  #else
        			    float scale = 1.0;
        			  #endif
        			    o.uvgrab.xy = (float2(o.pos.x, o.pos.y*scale) + o.pos.w) * 0.5;
        			  //----------------------

                o.uvgrab.zw = o.pos.zw;
                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i) : SV_Target
            {


                fixed4 col = fixed4(0,0,0,0);
                fixed shadow = SHADOW_ATTENUATION(i);





        				float2 offset = _GrabTexture_TexelSize.xy;
        				i.uvgrab.xy = offset * i.uvgrab.z + i.uvgrab.xy;

        				col += tex2Dproj (_GrabTexture, UNITY_PROJ_COORD(i.uvgrab));


                //shadow will be near 0 (black) where darkest
                //so when it is near 0 add some red
                col.rgb = col.rgb*shadow +  (float3(1,1,1) - shadow) * _ShadowColor;
                return col;
            }
            ENDCG
        }
        Pass
        {
            Tags {"LightMode"="ShadowCaster"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_shadowcaster
            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f {
                V2F_SHADOW_CASTER;
            };

            v2f vert(appdata v)
            {
                v2f o;
                TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
                return o;
            }

            float4 frag(v2f i) : SV_Target
            {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }


    }

}
