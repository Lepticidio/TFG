Shader "Eon/BigFade"
{//
	Properties
	{
		_Color ("Color", Color) = (1, 1, 1, 1)
		_MainTex ("Texture", 2D) = "white" {}

        [Space(20)]
        _CamOffset ("Camera offset", Vector) = (0, 2, 2)
        _MinOpacity ("Min Opacity", float) = 0.9
        _MinPlayerDist ("Min Player Distance", float) = 1
        _MaxPlayerDist ("Max Player Distance", float) = 3

        [Space(40)]
        _MinUnderwaterTint ("Min Underwater Tint", Range(0, 1)) = 0.35
        _MinUnderwaterFog ("Min Underwater Fog", Float) = 7
        _MaxUnderwaterFog ("Max Underwater Fog", Float) = 25

        [Space(20)]
        _WaterColor ("Water color", Color) = (0.2233146, 0.4887639, 0.75, 1)
        _WaterTex("Water texture", 2D) = "white" {}
        _Tiling ("Water tiling", Vector) = (0.3, 0.3, 0, 0)
        _TextureVisibility ("Texture visibility", Range(0, 1)) = 0.276

        [Space(20)]
        _DistTex ("Distortion", 2D) = "white" {}
        _DistTiling ("Distortion tiling", Vector) = (0.7, 0.7, 0, 0)

        [Space(20)]
        //_DeepColor ("Water deep color", Color) = (1, 1, 1, 1)
        _WaterHeight ("Water height", Float) = -0.1
        _WaterDeep ("Water deep", Float) = 22.9
        _WaterDepth ("Water depth param", Range(0, 0.1)) = 0.0172
        _WaterMinAlpha ("Water min alpha", Range(0, 1)) = 0.552

        [Space(20)]
        _BorderColor ("Border color", Color) = (1, 1, 1, 0.6196079)
        _BorderWidth ("Border width", Range(0, 1)) = 0.061

        [Space(20)]
        _MoveDirection ("Direction", Vector) = (-0.7, 0, 0.3, 0)
	}
	SubShader
	{
		Tags {  "RenderType"="Opaque" "BW" = "TrueProbes" "LightMode" = "ForwardBase" }
		LOD 100

        Blend SrcAlpha OneMinusSrcAlpha

		ZWrite On

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

            #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				fixed4 worldPos : TEXCOORD1;
				fixed camHeightOverWater : TEXCOORD2;
				fixed waterDepth : TEXCOORD3;
			};

			fixed4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;

            fixed3 _CamOffset;
            float _MaxPlayerDist;
            float _MinPlayerDist;
            float _MinOpacity;

			float _MinUnderwaterTint;
			float _MinUnderwaterFog;
			float _MaxUnderwaterFog;

			sampler2D _WaterTex;
			fixed2 _Tiling;
			fixed4 _WaterColor;

			sampler2D _DistTex;
			fixed2 _DistTiling;

			fixed4 _DeepColor;
			fixed _WaterHeight;
			fixed _TextureVisibility;
			fixed _WaterDeep;
			fixed _WaterDepth;
			fixed _WaterMinAlpha;

			fixed4 _BorderColor;
			fixed _BorderWidth;
			fixed _BorderVisibility;

			fixed3 _MoveDirection;

            float _CutoutThresh;

			fixed2 WaterPlaneUV(fixed3 worldPos, fixed camHeightOverWater)
			{
				fixed3 camToWorldRay = worldPos - _WorldSpaceCameraPos;
				fixed3 rayToWaterPlane = (camHeightOverWater / camToWorldRay.y * camToWorldRay);
				return rayToWaterPlane.xz - _WorldSpaceCameraPos.xz;
			}

			fixed4 MainColor(v2f i)
			{
				fixed4 mainCol = tex2D(_MainTex, i.uv) * _Color;
                clip(mainCol.a - _CutoutThresh);

				return mainCol;
			}

			v2f vert (appdata v)
			{
				v2f o;
				o.worldPos = mul(UNITY_MATRIX_M, v.vertex);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				fixed3 camToWorldRay = o.worldPos - _WorldSpaceCameraPos;
				o.camHeightOverWater = _WorldSpaceCameraPos.y - _WaterHeight;

				fixed3 rayToWaterPlane = o.camHeightOverWater / (-camToWorldRay.y) * camToWorldRay;
				fixed depth = length(camToWorldRay - rayToWaterPlane);
				o.waterDepth = depth * _WaterDepth * saturate(rayToWaterPlane.y - camToWorldRay.y);

				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{

				fixed4 mainCol = MainColor(i);

				float isCamAbove = smoothstep(0, _CamOffset.y + _WaterHeight, _WorldSpaceCameraPos.y - _WaterHeight);

				fixed lengthUnderWater = max(0, _WaterHeight - i.worldPos.y);
				fixed underWater = lerp(0, 1, lengthUnderWater > 0);
				fixed borderAlpha = lerp(underWater * _BorderColor.a, 0, saturate(lengthUnderWater / _BorderWidth));
				fixed waterAlpha = saturate(lengthUnderWater / _WaterDeep + _WaterMinAlpha + i.waterDepth);

				fixed2 water_uv = WaterPlaneUV(i.worldPos, i.camHeightOverWater);
				fixed4 distortion = tex2D(_DistTex, water_uv * _DistTiling) * 2 - 1;
				fixed2 distorted_uv = ((water_uv + distortion.rg) - _Time.y * _MoveDirection.xz) * _Tiling;

				fixed4 waterCol = tex2D(_WaterTex, distorted_uv)*isCamAbove;
				waterCol = lerp(_WaterColor, fixed4(1, 1, 1, 1), waterCol.r * _TextureVisibility);

				float camDist = distance(i.worldPos, _WorldSpaceCameraPos);
				float normalizedDist = min(1, max(_MinUnderwaterTint, step(_MinUnderwaterFog, camDist)*(camDist/_MaxUnderwaterFog)));


				float waterAmount = (isCamAbove*underWater  + (1- isCamAbove)*normalizedDist)* waterAlpha*_WaterColor.a;
				fixed4 finalCol = lerp(mainCol, waterCol, waterAmount);
				finalCol.rgb = lerp(finalCol.rgb, _BorderColor.rgb, borderAlpha);

				float3 playerPos =   _WorldSpaceCameraPos - _CamOffset;
		    	float infrontPlayer = max(0,  min((i.worldPos.z - playerPos.z + 1), 1));
				infrontPlayer *= infrontPlayer;
		    	float camDistNorm = max(0,  min(camDist, 1));
				camDistNorm *= camDistNorm;
                float playerDist = distance(i.worldPos.xy, playerPos.xy);
                float fadeRange = _MaxPlayerDist - _MinPlayerDist;
                float normalizedDistance = min(1,max(0, (playerDist/fadeRange) - (_MinPlayerDist/fadeRange)));
				finalCol = finalCol*max(_MinOpacity,max(normalizedDistance,(infrontPlayer))*camDistNorm);

				return finalCol;
			}
			ENDCG
		}
	}
//	Fallback "Transparent/Cutout/VertexLit"
}
