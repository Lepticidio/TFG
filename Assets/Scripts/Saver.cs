﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saver : MonoBehaviour
{

	public void Save(World world, int state)
	{

		PlayerPrefs.SetFloat("millionYears", world.millionYears);

		PlayerPrefs.SetInt("numberGenerations", world.generations.Count);
		PlayerPrefs.SetInt("numberSpecies", world.allSpecies.Count);
		for (int i = 0; i < world.allSpecies.Count; i++)
		{
			Save(world.allSpecies[i], "species" + i.ToString());

		}
		for (int i = 0; i < world.generations.Count; i++)
		{
			PlayerPrefs.SetInt("speciesInGeneration" + i.ToString(), world.generations[i].Count);
			for (int j = 0; j < world.generations[i].Count; j++)
			{
				PlayerPrefs.SetInt("indexInGeneration" + i.ToString() + "_" + j.ToString(), world.generations[i][j].index);
			}
		}

		PlayerPrefs.SetInt("playerIndex", world.player.index);
		PlayerPrefs.SetInt("futurePlayerIndex", world.futurePlayer.index);

		for (int i = 0; i < world.biomes.Length; i++)
		{
			Save(world.biomes[i], "biome" + i.ToString());
		}

		PlayerPrefs.SetInt("currentBiome", System.Array.IndexOf(world.biomes, world.currentBiome));
		PlayerPrefs.SetInt("saved", 1);
		PlayerPrefs.SetInt("state", state);
	}

	public string Load (Menu menu)
	{
		World world = menu.world;
		int saved = PlayerPrefs.GetInt("saved");
		int mutationsSelected = PlayerPrefs.GetInt("mutationsSelected");
		if (saved == 1)
		{
			world.millionYears = PlayerPrefs.GetFloat("millionYears");
			int numberSpecies = PlayerPrefs.GetInt("numberSpecies");
			for (int i = 0; i < numberSpecies; i++)
			{
				world.allSpecies.Add(LoadSpecies("species" + i, world));
			}
			int numberGenerations = PlayerPrefs.GetInt("numberGenerations", world.generations.Count);
			for (int i = 0; i < numberGenerations; i++)
			{
				world.generations.Add(new List<Species>());
				int speciesInGeneration = PlayerPrefs.GetInt("speciesInGeneration" + i.ToString());
				for (int j = 0; j < speciesInGeneration; j++)
				{
					world.generations[i].Add(world.allSpecies[PlayerPrefs.GetInt("indexInGeneration" + i.ToString() + "_" + j.ToString())]);
				}
			}

			world.player = world.allSpecies[PlayerPrefs.GetInt("playerIndex")];
			world.futurePlayer = world.allSpecies[PlayerPrefs.GetInt("futurePlayerIndex")];
			
			for (int i = 0; i < world.biomes.Length; i++)
			{
				world.biomes[i] = LoadBiome("biome" + i.ToString(), world);
			}

			world.currentBiome = world.biomes[PlayerPrefs.GetInt("currentBiome")];
			world.StandardConnections();

			int state = PlayerPrefs.GetInt("state");

			if(mutationsSelected == 1)
			{
				for(int i = 0; i < world.mutationTypes.Length; i++)
				{
					world.mutationTypes[i] = PlayerPrefs.GetString("mutationType" + i.ToString());
					world.mutationIndexes[i] = PlayerPrefs.GetInt("mutationIndex" + i.ToString());
				}
				for (int i = 0; i < world.mutationColors.Length; i++)
				{
					world.mutationColors[i] = new Color
					(
						PlayerPrefs.GetFloat("mutationColorR" + i.ToString()),
						PlayerPrefs.GetFloat("mutationColorG" + i.ToString()),
						PlayerPrefs.GetFloat("mutationColorB" + i.ToString())
					);
				}
			}

			if(state == 0)
			{
				return "Level";
			}
			else
			{
				return "Evolution";
			}

		}
		else
		{
			return "null";
		}
	}

	public void Save(Species species, string id)
	{
		for (int i = 0; i < species.name.Length; i++)
		{
			PlayerPrefs.SetString(id + "name" + i.ToString(), species.name[i]);
		}

		PlayerPrefs.SetInt(id + "headInt", species.headInt);
		PlayerPrefs.SetInt(id + "bodyInt", species.bodyInt);
		PlayerPrefs.SetInt(id + "tailInt", species.tailInt);
		PlayerPrefs.SetInt(id + "legsInt", species.legsInt);

		PlayerPrefs.SetFloat(id + "mainR", species.color.r);
		PlayerPrefs.SetFloat(id + "mainG", species.color.g);
		PlayerPrefs.SetFloat(id + "mainB", species.color.b);

		PlayerPrefs.SetFloat(id + "secondaryR", species.secondaryColor.r);
		PlayerPrefs.SetFloat(id + "secondaryG", species.secondaryColor.g);
		PlayerPrefs.SetFloat(id + "secondaryB", species.secondaryColor.b);

		PlayerPrefs.SetFloat(id + "eyeR", species.eyeColor.r);
		PlayerPrefs.SetFloat(id + "eyeG", species.eyeColor.g);
		PlayerPrefs.SetFloat(id + "eyeB", species.eyeColor.b);

		PlayerPrefs.SetInt(id + "index", species.index);

		if(species.antecessor != null)
		{
			PlayerPrefs.SetInt(id + "antecessorIndex", species.antecessor.index);
		}
	}

	public Species LoadSpecies(string id, World world)
	{
		Species species = new Species
		(
			PlayerPrefs.GetInt(id + "headInt"),
			PlayerPrefs.GetInt(id + "bodyInt"),
			PlayerPrefs.GetInt(id + "tailInt"),
			PlayerPrefs.GetInt(id + "legsInt"),

			new Color
			(
				PlayerPrefs.GetFloat(id + "mainR"),
				PlayerPrefs.GetFloat(id + "mainG"),
				PlayerPrefs.GetFloat(id + "mainB")
			),


			new Color
			(
				PlayerPrefs.GetFloat(id + "secondaryR"),
				PlayerPrefs.GetFloat(id + "secondaryG"),
				PlayerPrefs.GetFloat(id + "secondaryB")
			),

			new Color
			(
				PlayerPrefs.GetFloat(id + "eyeR"),
				PlayerPrefs.GetFloat(id + "eyeG"),
				PlayerPrefs.GetFloat(id + "eyeB")
			)
		);
		species.index = PlayerPrefs.GetInt(id + "index");
		species.UpdateStats();
		if(species.index > 0)
		{
			species.antecessor = world.allSpecies
			[
				PlayerPrefs.GetInt(id + "antecessorIndex")
			];
		}
		for (int i = 0; i < species.name.Length; i++)
		{
			PlayerPrefs.GetString(id + "name" + i.ToString(), species.name[i]);
		}
		return species;
	}

	public void Save(Biome biome, string id)
	{
		PlayerPrefs.SetFloat(id + "temperature", biome.temperature);
		PlayerPrefs.SetFloat(id + "humidity", biome.humidity);
		PlayerPrefs.SetFloat(id + "sealevel", biome.sealevel);

		for (int i = 0; i < biome.niches.Count; i++)
		{
			Save(biome.niches[i], id + "niche" + i.ToString());
		}

	}

	public Biome LoadBiome(string id, World world)
	{
		Biome biome = new Biome
		(
			PlayerPrefs.GetFloat(id + "temperature"),
			PlayerPrefs.GetFloat(id + "humidity"),
			PlayerPrefs.GetFloat(id + "sealevel"),
			world
		);
		biome.UpdateBiome();
		for (int i = 0; i < biome.niches.Count; i++)
		{
			biome.niches[i]= LoadNiche(biome.niches[i], id + "niche" + i.ToString(), world);
		}
		return biome;
	}

	public void Save (Niche niche, string id)
	{
		if(niche.species != null)
		{
			PlayerPrefs.SetInt(id + "index", niche.species.index);
			PlayerPrefs.SetInt(id + "hasSpecies", 1);
		}
		else
		{
			PlayerPrefs.SetInt(id + "hasSpecies", 0);
		}
	}

	public Niche LoadNiche (Niche niche, string id, World world)
	{
		if(PlayerPrefs.GetInt(id + "hasSpecies") == 1)
		{
			niche.species = world.allSpecies
			[
				PlayerPrefs.GetInt(id + "index")
			];
		}
		return niche;
	}

	public void ClearSaves()
	{
		PlayerPrefs.SetInt("saved", 0);
		PlayerPrefs.SetInt("mutationsSelected", 0);
		PlayerPrefs.SetInt("state", 0);
	}

	public void SaveMutations(World world)
	{
		for (int i = 0; i < world.mutationTypes.Length; i++)
		{
			PlayerPrefs.SetString("mutationType" + i.ToString(), world.mutationTypes[i]);
			PlayerPrefs.SetInt("mutationIndex" + i.ToString(), world.mutationIndexes[i]);
		}
		for (int i = 0; i < world.mutationColors.Length; i++)
		{
			PlayerPrefs.SetFloat("mutationColorR" + i.ToString(), world.mutationColors[i].r);
			PlayerPrefs.SetFloat("mutationColorG" + i.ToString(), world.mutationColors[i].g);
			PlayerPrefs.SetFloat("mutationColorB" + i.ToString(), world.mutationColors[i].b);
		}
		PlayerPrefs.SetInt("mutationsSelected", 1);
	}
}
